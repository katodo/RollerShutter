#ifndef __COMMON_H__
#define __COMMON_H__
//#if ARDUINO_VERSION <= 106
//-#pragma "test is true"
//-#endif
//--------------------------_DEBUG1 SWITCH-------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//  FILE DI DEFINIZIONI COMUNI VISIBILI A TUTTE  LE LIBRERIE INCLUSE CON LA DIRETTIVA #include
//  DEVE ESSERE INCLUSO CON  #include "common.h"  IN TUTTI GLI HEADERS FILES DELLE LIBRERIE
//----------------------------------------------------------------------------------------------------------------------------------
//libreria col codice del client wifi
//extern "C" {
//    #include "user_interface.h"
//}

#include <Arduino.h>
//#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <MQTT.h> 
//#include <WebSockets.h> 
//#include <WebSocketsClient.h>                     
#include <WebSocketsServer.h>           
#include <ESP8266HTTPUpdateServer.h>

//#include <ArduinoOTA.h>
#include "eepromUtils.h"
#include "timersNonSchedulati.h"
#include "schedservices.h"
#include "abparser.h"
//#include "CSMADriver.h"

//DEFAULT CONFIGURATIONS
//wifi config----------------------------------------------
#define OUTTOPIC		"sonoff17/out"
#define INTOPIC			"sonoff17/in"
#define	LOGPATH 		"sonoff17/log"
#define SSID1			"WebPocket-E280"
#define PSW1			"dorabino.7468!"
#define SSID2			"AndroidAP1"
#define PSW2			"pippo2503"
#define SSIDAP			"admin"
#define PSWAP			""
#define MQUSR			""
#define MQPSW			""
#define WBPSW			"admin"
#define WBUSR			"admin"
#define MQTTSRV			"broker.hivemq.com"
//#define MQTTSRV			"192.168.43.137"
//#define MQTTSRV			"192.168.10.187"
#define MQTTPRT			1883
#define WSPRT			"8000"
#define MQTTPT			"mqtt"
#define ROLLMODE1 		1
#define ROLLMODE2 		1
#define NTP1 			"ntp1.inrim.it"
#define NTP2 			"0.it.pool.ntp.org"
#define DAYSAVE			0
//#define LOGSEL			2
#define LOGSEL			0
#define DEVID			"01"
#define BROADCASTID		"FF"
#define MODBUSID		3
#define MODBUSBAUDRATE	57600
#define DELTAP      	40
//END DEFAULTS
//_DEBUG1 LEVELS---------------------
#define _DEBUG   		1		//ACTIVATE LOCAL AND REMOTE _DEBUG1 MODE
#define _DEBUGR   		0		//ACTIVATE ONLY REMOTE _DEBUG1 MODE
//LARGE FW OTA UPLOAD---------------------
#define LARGEFW 		1
//----------------------------------------
//Definizione modello
#define SONOFF_4CH				0
#define SONOFF_S20 				0
#define ROLLERSHUTTER_V1 		0
#define ROLLERSHUTTER_V2 		1
//-------------------------------------------------------------------------------------------------------------------------------------------------
//-------------NON MODIFICARE LA PARTE SOTTOSTATNTE------------------------------------------------------------------------------------------------
//#define AUTOCAL_HLW8012			0
//#define AUTOCAL_ACS712			1
//#define MCP2317					1

#if (ROLLERSHUTTER_V2)
  #define TEMP 		1
  #define SCR   	1  
  #define INPULLUP  0  			//disable internal pullup
  #define OUTSLED  	2     
  #define OUT1EU  	8      		// OUT1 =  MOTOR1 UP   
  #define OUT1DD  	9    	 	// OUT2 =  MOTOR1 DOWN     
  #define OUT2EU  	10 	    	// OUT3 =  MOTOR2 UP  
  #define OUT2DD  	11      		// OUT4 =  MOTOR2 DOWN
  #define GREEN  	13  
  #define RED  		12
  #define BLUE  	14   
  #define UNUSE1  	4 
  #define UNUSE2 	5
  #define UNUSE3 	6
  #define UNUSE4  	7
  #define UNUSE5 	15 
  //local buttons
  #define BTN1U    	0    		// IN1   =  MOTOR1 UP    
  #define BTN1D    	1    		// IN2   =  MOTOR1 DOWN    
  #define BTN2U    	2    		// IN3   =  MOTOR2 UP    
  #define BTN2D    	3    		// IN4   =  MOTOR2 DOWN
  #define AUTOCAL_HLW8012			1
  #define AUTOCAL_ACS712			0
  #define MCP2317					1
  #define RS485						1
  #define ONEBTN					0
  #define TWOBTN					0
  #define ROLLER					1
  #define BTNDIM     				2     	//FUNCTION SWITCH ARRAY DIMENSION (NUMBER OF FUNCTION SWITCHES FOR BUTTON GROUPS)
  #define FILTERMASK				15
  #elif (ROLLERSHUTTER_V1)
  #define TEMP 						1
  #define SCR    	1  
  #define INPULLUP  0  			//disable internal pullup
  #define OUTSLED  	2     
  #define OUT1EU  	12      	// OUT1 =  MOTOR1 UP   
  #define OUT1DD  	4 //5    	// OUT2 =  MOTOR1 DOWN     
  #define OUT2EU  	5 //4     	// OUT3 =  MOTOR2 UP  
  #define OUT2DD  	15       	// OUT4 =  MOTOR2 DOWN
  //local buttons
  #define BTN1U    	0    		// IN1   =  MOTOR1 UP    
  #define BTN1D    	13    		// IN2   =  MOTOR1 DOWN    
  #define BTN2U    	16    		// IN3   =  MOTOR2 UP    
  #define BTN2D    	14    		// IN4   =  MOTOR2 DOWN
  #define AUTOCAL_ACS712			1
  #define AUTOCAL_HLW8012			0
  #define MCP2317					0
  #define RS485						0
  #define ONEBTN					0
  #define TWOBTN					0
  #define ROLLER					1
  #define BTNDIM     				2     	//FUNCTION SWITCH ARRAY DIMENSION (NUMBER OF FUNCTION SWITCHES FOR BUTTON GROUPS)
#elif (SONOFF_4CH)
  #define TEMP 						1
  #define SCR		1 
  #define INPULLUP  1   		//enable internal pullup
  #define OUTSLED	13     
  #define OUT1EU	12  
  #define OUT1DD	5   
  #define OUT2EU	4     
  #define OUT2DD	15    
  //local buttons
  #define BTN1U		0    
  #define BTN1D		9  
  #define BTN2U		10   
  #define BTN2D		14   
  #define AUTOCAL_ACS712			1
  #define AUTOCAL_HLW8012			0
  #define MCP2317					0
  #define RS485						0
  #define ONEBTN					0
  #define TWOBTN					0
  #define ROLLER					1
  #define BTNDIM     				2     	//FUNCTION SWITCH ARRAY DIMENSION (NUMBER OF FUNCTION SWITCHES FOR BUTTON GROUPS)
  #define FILTERMASK				17921
 #elif (SONOFF_S20)
  #define TEMP 						0
  #define SCR						1 
  #define INPULLUP 					1   		//enable internal pullup
  #define OUTSLED					13     
  #define OUT1EU					12 
  #define OUT1DD					13   
  #define BTN1D 					9 
  #define BTN1U						0  
  #define AUTOCAL_ACS712			0
  #define AUTOCAL_HLW8012			0
  #define MCP2317					0
  #define RS485						0
  #define ONEBTN					1
  #define TWOBTN					0
  #define ROLLER					0
  #define BTNDIM     				1    	//FUNCTION SWITCH ARRAY DIMENSION (NUMBER OF FUNCTION SWITCHES FOR BUTTON GROUPS)
  #define TMRHALT   				0		//INDICE TIMER DI FINE CORSA
  #define ROLLMODE1 				0
  #define ROLLMODE2 				0
  #define FILTERMASK				0x1
 #else
  #error Wrong version defined - cannot continue!
#endif

//#define THRESHOLD1	13
//#define THRESHOLD2	13
//#define RAMPDELAY1	1  		//n*20ms
//#define RAMPDELAY2	1  		//n*20ms

//rollershutter constants
#if (ROLLER)
#define DELTAL		4
#define TENDCHECK	2		//MAX TIME IN SEC BEFORE THE EFFECTIVE START OF THE MOTOR
#define SW1ONS      2		//SWITCH 1 (UP) ON STATE
#define SW2ONS     	3		//SWITCH 2 (DOWN) ON STATE
#define TAP1      	0		//INDICE TAPPARELLA 1
#define TAP2      	1		//INDICE TAPPARELLA 2
#define CHROLLER	0
#define UP			1
#define DOWN		-1
#endif

#define ENABLES    	0		//ENABLES STATE
#define DIRS       	1		//DIR SELECTION STATE
#define THALTMAX   	90000 
#define TMRHALT   	0		//INDICE TIMER DI FINE CORSA
#define TMRHALT2 	1		//INDEX OF
#define ONGAP		20		//divisioni ADC della soglia di ON motore
#define ENDFACT		2		// (%) margine di posizionamento con i sensori in percentuale dell'escursione totale, dopo avviene col timer
#define PUSHINTERV	60 		// in sec
#define ONE_WIRE_BUS 2  	// DS18B20 pin
#define	TCOUNT		5		//MAX FAILED CONNECTION ATTEMPTS BEFORE WIFI CLIENT COMMUTATION
#define RSTTIME		20		//DEFINE HOW MANY SECONDS BUTTON1 MUST BE PRESSED UNTIL A RESET OCCUR 
#define CNTIME		4		//DEFINE HOW MANY SECONDS HAVE TO LAST THALT PARAMETER AT LEAST
#define CHTEMP		1
#define CHPOWER		2
#define ALL			3
//#define CONFTIME	4		//DEFINE HOW MANY SECONDS 
#define APOFFT		120		//DEFINE HOW MANY SECONDS   
#define MAINBTN		3		//BUTTON WITH PROGRAMMING AND RESET FUNCTIONS

//Dimensione dei vettori di switch, timers e counters
#define CHRN	 	2		//OVERALL NUMBER OF CHRONOMETERS (FUNCTION COUNTERS + SPECIAL COUNTERS) 
#define TIMERN  	6		//OVERALL NUMBER OF TIMERS (FUNCTION TIMERS + SPECIAL TIMERS)      
#define SWITCHND 	4		//OVERALL NUMBER OF SWITCHES (FUNCTION SWITCHES + SPECIAL SWITCHES) 
#define JSONN   	4		//JSON ARRAY DIMENSION 
//INIZIO COSTANTI LOGICA DI COMANDO---------------------------------------------------
#define NBTN     	2     	//NUMBER OF BUTTONS
#define	BTNDEL1   	600    	//BTN DELAY (ms)
#define	BTNDEL2  	600    	//BTN DELAY (ms)
#define RSTATUSDIM 	4		//STATUS ARRAY DIMENSION (NUMBER OF STATES FOR BUTTON GROUPS)
#define TIMERDIM  	2		//FUNCTION TIMERS ARRAY DIMENSION (NUMBER OF FUNCTION TIMERS FOR BUTTON GROUPS)
#define BTN1IN      0       //UP BUTTON INDEX
#define BTN2IN     	1		//DOWN BUTTON INDEX
//#define BTNDIM     	2     	//FUNCTION SWITCH ARRAY DIMENSION (NUMBER OF FUNCTION SWITCHES FOR BUTTON GROUPS)
//#define OUTDIM    	2     	//OUT PORT ARRAY DIMENSION (NUMBER OF OUT PORTS FOR BUTTON GROUPS)
//#define JSONLEN   	8		//NUMBER OF FIELDS OF THE JSON MESSAGE
#define STATBTNNDX  0  	    //STATUS BUTTON INDEX
//special array elements (not used for normal functions in all button groups, are always after normal elements)
//#define CONNSTATSW  4		//INDEX OF CONNECTION SWITCH (DETECTS WIFI STATUS RISE FRONTS)
#define RESETTIMER  4		//INDEX OF RESET TIMER (DETECTS IF A RESET COMMAND OCCUR)
#define APOFFTIMER	5		//INDEX OF AP TIMER (DETECTS IF A AP ACTIVATION COMMAND OCCUR)
#define CNTSERV1	0		//INDEX OF SERVICE COUNTER GROUP 1 (DETECTS AND COUNT MAIN BUTTON CLICKS)
#define CNTSERV2	1		//INDEX OF SERVICE COUNTER GROUP 2 (DETECTS AND COUNT MAIN BUTTON CLICKS)
#define CNTSERV3	2		//INDEX OF SERVICE COUNTER GROUP 1 (DETECTS AND COUNT MAIN BUTTON CLICKS)
#define CNTSERV4	3		//INDEX OF SERVICE COUNTER GROUP 2 (DETECTS AND COUNT MAIN BUTTON CLICKS)
#define CNTIME1		4		//INDEX OF
#define CNTIME2		5		//INDEX OF
#define CNTIME3		6		//INDEX OF
#define CNTIME4		7		//INDEX OF
#define SMPLCNT1	8		//INDEX OF
#define SMPLCNT2	9		//INDEX OF
#define SMPLCNT3	10		//INDEX OF
#define SMPLCNT4	11		//INDEX OF
#define CONDCNT1	12		//INDEX OF
#define CONDCNT2	13		//INDEX OF
#define CONDCNT3	14		//INDEX OF
#define CONDCNT4	15		//INDEX OF
#define ACTPWRCNT	16		//INDEX OF
#define ACVOLTCNT	17		//INDEX OF
#define TEMPCNT		18		//INDEX Of
//#define TIMECNT		12	//INDEX OF
#define NCNT	 	19		//OVERALL NUMBER OF COUNTERS (FUNCTION COUNTERS + SPECIAL COUNTERS) 
#define BTNUP		0		//INDEX OF CALIBRATION CRONO (DETECTS UP TIME AND DOWN TIME)
#define BTNDOWN		1		//INDEX OF CALIBRATION CRONO (DETECTS UP TIME AND DOWN TIME)
#define WIFISTA		0
#define WIFIAP		1
//Sensor gates
#define GTTEMP		0
#define GTMEANPWR1	1
#define GTMEANPWR2	2
#define GTPEAKPWR1	3
#define GTPEAKPWR2	4
#define GTIPWR		5
#define GTIVAC		6
#define GATEND 		7	//OVERALL NUMBER OF GATES
//Async R/W buffers
#define ASYNCDIM	5	//OVERALL NUMBER OF R/W BUFFERS
#define TEMPRND			0.1
#define MEANPWR1RND		1
#define MEANPWR2RND		1	
#define PEAKPWR1RND		1
#define PEAKPWR2RND		1
#define IPWRRND			2  //W
#define IVACRND			1  //V
#define DATEBUFLEN		48
//#define DBGBUFLEN		200
#define RSLTBUFLEN		300
//SENSOR POLLING INTERVAL (SEC)
#define ACTPWRINT		1
#define ACVOLTINT		1
#define TEMPINT			3
//-----------------------------------------------------------------------------------
//MODBUS registers
//-----------------------------------------------------------------------------------
//Comandi --------------------------------------------------------------------------- 0+7
#define WRBTN			0		//comando 4 pulsanti (4 bit L)
#define WRUP1			1		//comando pulsante UP1
#define WRDW1			2		//comando pulsante DOWN1
#define WRUP2			3		//comando pulsante UP2
#define WRDW2			4		//comando pulsante DOWN2
#define WRPR1			5		//comando calcolo percentuale posizione tapparella 1
#define WRPR2			6		//comando calcolo percentuale posizione tapparella 2
//Stati ----------------------------------------------------------------------------- 7+10
#define RDBTN			7		//stato 4 pulsanti (4 bit L)
#define RDGPA			8		//stato registri IOExpander A
#define RDGPB			9		//stato registri IOExpander B
#define RDUP1			10		//stato pulsante UP1
#define RDDW1			11		//stato pulsante DOWN1
#define RDUP2			12		//stato pulsante UP2
#define RDDW2			13		//stato pulsante DOWN2
#define RDPR1			14		//valore percentuale posizione tapparella 1
#define RDPR2			15		//valore percentuale posizione tapparella 2
#define RDVLT			16		//valore Volt AC
//Stati double----------------------------------------------------------------------- 17+7 (double)
#define RDTR1H			17		//valore tempo di partenza tapparella 1 H
#define RDTR1L			18		//valore tempo di partenza tapparella 1 L
#define RDTR2H			19		//valore tempo di partenza tapparella 2 H
#define RDTR2L			20		//valore tempo di partenza tapparella 2 L
#define RDSP1H			21		//valore tempo finale tapparella 1 H
#define RDSP1L			22		//valore tempo finale tapparella 1 L
#define RDSP2H			23		//valore tempo finale tapparella 2 H
#define RDSP2L			24		//valore tempo finale tapparella 2 L
#define RDTMPH			25		//valore temperatura H
#define RDTMPL			26		//valore temperatura L
#define RDWATH			27		//valore potenza attiva istantanea H
#define RDWATL			28		//valore potenza attiva istantanea L
#define RDWREH			29		//valore potenza reattiva istantanea H
#define RDWREL			30		//valore potenza reattiva istantanea L
#define REGDIM			31
//--------------------------EEPROM offsets-------------------------------------------
//First two uint8_t reserved for EEPROM check
//1 byte offets (char)
#define DONOTUSE				2
#define MBUSIDOFST				3
#define LOGSLCTOFST				4
#define ACVOLTOFST				5
#define SWROLL1OFST				6
#define SWROLL2OFST				7
#define NTPSDTOFST				8
#define NTPZONEOFST				9
//2 byte offets (int)
#define EEPROMLENOFST			10
#define NTPADJUSTOFST			12
//4 byte offets (float, unsigned long)
#define THALT1OFST             	14
#define THALT2OFST				18
#define THALT3OFST				22
#define THALT4OFST				26
#define PWRMULTOFST				30
#define STDEL1OFST				34
#define STDEL2OFST				38
#define VALWEIGHTOFST			42
#define	TLENGTHOFST				46
#define	BARRELRADOFST			50
#define	THICKNESSOFST			54
#define SLATSRATIOFST			58
#define NTPSYNCINTOFST			62
#define CALPWROFST				66
#define CURRMULTOFST			70
#define VACMULTOFST				74
#define SWSPLDPWR1OFST3			78
#define SWSPLDPWR1OFST4			82
#define MBUSVELOFST				86
//32 byte offsets (fixed medium String)
#define	MQTTIDOFST				90
#define	OUTTOPICOFST			122
#define	INTOPICOFST				144
#define	MQTTUP1OFST				186
#define	MQTTDOWN1OFST			218
#define	MQTTUP2OFST				250
#define	MQTTDOWN2OFST			282
#define	MQTTPORTOFST			314
#define	WSPORTOFST				346
#define	MQTTPROTOFST			378
#define	RESERVEDSTR3			410
#define	WIFICLIENTSSIDOFST1		442
#define	WIFICLIENTPSWOFST1		474
#define	WIFICLIENTSSIDOFST2		506
#define	WIFICLIENTPSWOFST2		538
#define	WIFIAPSSIDOFST			570
#define	WIFIAPPPSWOFST			602
#define	WEBUSROFST				634
#define	WEBPSWOFST				666
#define	MQTTUSROFST				698
#define	MQTTPSWOFST				730
#define	MQTTLOGOFST				762
#define RESERVEDOFST			794
//64 byte offsets (fixed long String)
#define MQTTADDROFST			826
#define NTP1ADDROFST			900
#define NTP2ADDROFST			964
//end fixed lenght params
#define FIXEDPARAMSLEN			1028
//x byte offsets (variable String)
//--------------------------Fine EEPROM offsets-------------------------------------------

//--------------------------Inizio MQTT array indexes-----------------------------------
//Indici array MQTT[MQTTDIM] dei NOMI dei campi json --> array inr[MQTTDIM] dei VALORI numerici di input (valori numerici di ingresso, 
//flag di segnalazione arrivo in INPUT: richieste di valori di parametri, richieste di esecuzione di comandi.
//valori di risposta in OUTPUT: feedbacks broadcast di arrivo di un nuovo comando, parametro o richiesta, valori di risposta all'esecuzione di un comando, 
//valori di risposta alla richiesta di lettura di un parametro (si noti che tutti questi, non dovendo essere processati dal parser MQTT, 
//potrebbero alternativamente essere hardcoded, � opportuno che stiano qu� se il loro nome COINCIDE con quello di un corrispondente flag di richiesta).
//----------------------------
#define MQTTUP1				0
#define MQTTDOWN1			1
#define MQTTUP2				2
#define MQTTDOWN2			3
//richiesta parametri
#define MQTTMAC				4
#define MQTTIP				5
#define MQTTTIME			6
#define MQTTTEMP			7
#define MQTTMEANPWR			8
#define MQTTPEAKPWR			9
#define MQTTALL				10
#define MQTTDATE			11
#define INSTPWR				12
#define INSTACV				13
#define DOPWRCAL			14
//end user modificable flags
#define MQTTDIM				15
#define INRDIM				4
#define USRMODIFICABLEFLAGS 15
//--------------------------Inizio MQTT config array indexes-----------------------------------------------------
//Indici array confJson[CONFDIM] dei NOMI dei campi json dei valori di configurazione --> array confcmd[CONFDIM] 
//dei VALORI stringa di configurazione corrispondenti a flags attivi
//---------------------------------------------------------------------------------------------------------------
//Parametri di IN/OUT (no IN o OUT separatamente) da esporre via MQTT (hanno corrispettivo in array dei flag E VANNO PRIMA SEMPRE)
//Sono aggiornati anche via MQTT (oltre che da eeprom e da form)
//---------------------------------------------------------------------------------------------------------------
//parametri di lunghezza variabile (vanno prima sempre)
#define ONCOND1				0
#define ONCOND2				1
#define ONCOND3				2
#define ONCOND4				3
#define ONCOND5				4
#define ACTIONEVAL			5
//end variable length params--
#define VARCONFDIM			6
//parametri di lunghezza fissa (vanno subito dopo sempre)
#define UTCVAL				0 + VARCONFDIM
#define UTCSYNC				1 + VARCONFDIM
#define UTCADJ				2 + VARCONFDIM
#define UTCSDT				3 + VARCONFDIM
#define UTCZONE				4 + VARCONFDIM
#define WEBUSR				5 + VARCONFDIM
#define WEBPSW				6 + VARCONFDIM
#define CALPWR				7 + VARCONFDIM
#define	MBUSVEL				8 + VARCONFDIM
//end extern params ----------
#define EXTPARAM			9 + VARCONFDIM
//-------------------------------------------------------------------------------------------------------
//Parametri di stato da non eporre (non hanno corrispettivo in array dei flag, vanno subito dopo sempre)
//Non sono aggiornati via MQTT (solo attraverso eeprom e attraverso form)
//-------------------------------------------------------------------------------------------------------
#define APPSSID				0 + EXTPARAM
#define APPPSW				1 + EXTPARAM
#define CLNTSSID1			2 + EXTPARAM
#define CLNTPSW1			3 + EXTPARAM
#define CLNTSSID2			4 + EXTPARAM
#define CLNTPSW2			5 + EXTPARAM
#define MQTTADDR			6 + EXTPARAM
#define MQTTPORT			7 + EXTPARAM
#define WSPORT				8 + EXTPARAM
#define MQTTPROTO			9 + EXTPARAM
#define MQTTOUTTOPIC		10 + EXTPARAM
#define MQTTINTOPIC			11 + EXTPARAM
#define MQTTUSR				12 + EXTPARAM
#define MQTTPSW				13 + EXTPARAM
#define THALT1				14 + EXTPARAM
#define THALT2				15 + EXTPARAM
#define THALT3				16 + EXTPARAM
#define THALT4				17 + EXTPARAM
#define STDEL1				18 + EXTPARAM
#define STDEL2				19 + EXTPARAM
#define VALWEIGHT			20 + EXTPARAM
#define	TLENGTH				21 + EXTPARAM
#define	BARRELRAD			22 + EXTPARAM
#define	THICKNESS			23 + EXTPARAM
#define	SLATSRATIO			24 + EXTPARAM
#define SWROLL1				25 + EXTPARAM
#define SWROLL2				26 + EXTPARAM
#define LOCALIP				27 + EXTPARAM
#define NTPADDR1			28 + EXTPARAM
#define NTPADDR2			29 + EXTPARAM
#define LOGSLCT				30 + EXTPARAM 
#define	MQTTLOG				31 + EXTPARAM
#define	ACVOLT				32 + EXTPARAM
#define VACMULT				33 + EXTPARAM
#define PWRMULT				34 + EXTPARAM
#define CURRMULT			35 + EXTPARAM
#define MBUSID				36 + EXTPARAM
//end intern params ----------
#define MQTTID				37 + EXTPARAM
//parametri di stato (da non esporre)
#define SWACTION1			38 + EXTPARAM
#define SWACTION2			39 + EXTPARAM
#define SWACTION3			40 + EXTPARAM
#define SWACTION4			41 + EXTPARAM
//end parametri di stato -----
#define CONFDIM				42 + EXTPARAM
#define EXTCONFDIM			USRMODIFICABLEFLAGS + EXTPARAM
#define PARAMSDIM 			USRMODIFICABLEFLAGS + CONFDIM
//--------------------------Fine array indexes-----------------------------------
#if (AUTOCAL_HLW8012 || AUTOCAL_ACS712) 
#define	AUTOCAL			1
#else
#define	AUTOCAL			0
#endif

#define EMAA  			0.125
#define EMAB  			0.25
#define TSIGMA			20
#define TEMPCOUNT		1
#define NOMEDIATEMP		3

#define RUNDELAY  		3
#define NSIGMA 			1.4
#define EMA  			0.7
#define TBASE 			25	
#define MAINPROCSTEP	2
#define ONESEC_STEP		40
#define	LED_STEP		10
#define STOP_STEP		3
#define SEL_PIN			12
#define CF1_PIN			13
#define CF_PIN			14
#if (AUTOCAL_HLW8012) 
#define RUNDELAY  		3
#define NSIGMA 			1.5
#define EMA  			0.7
#define TBASE 			25	
#define MAINPROCSTEP	2
#define ONESEC_STEP		40
#define	LED_STEP		10
#define STOP_STEP		3
#define SEL_PIN			12
#define CF1_PIN			13
#define CF_PIN			14
// Set SEL_PIN to HIGH to sample current
// This is the case for Itead's Sonoff POW, where a
// the SEL_PIN drives a transistor that pulls down
// the SEL pin in the HLW8012 when closed
#define CURRENT_MODE                    HIGH
// These are the nominal values for the resistors in the circuit
#define CURRENT_RESISTOR                0.001
#define VOLTAGE_RESISTOR_UPSTREAM       ( 4 * 470000 ) // Real: 2280k
#define VOLTAGE_RESISTOR_DOWNSTREAM     ( 1000 ) // Real 1.009k
#endif

#if (AUTOCAL_ACS712) 
#define RUNDELAY  		1
#define TBASE 			2
#define EMA  			0.8
#define MAINPROCSTEP 	25
#define ONESEC_STEP		500
#define STOP_STEP		20
#define ZEROSMPL		400		//per non interrompere il caricamento delle pagine durante
								//il campionamento dello zero
#define NPWRSMPL		4
#define NZEROSMPL		4
#define NSIGMA 			2.5
#endif

#define WIFISTEP 		4

#if (LARGEFW)
	#if (AUTOCAL_HLW8012) 
		#include "HLW8012.h"
	#endif
	//#include <WiFiUdp.h>
	//#include <ESP8266mDNS.h>
	#include <RemoteDebug.h>                  // https://github.com/JoaoLopesF/RemoteDebug
	#include <OneWire.h>                      //  https://www.pjrc.com/teensy/td_libs_OneWire.html
#if (TEMP)
	#include <DallasTemperature.h>            //  https://github.com/milesburton/Arduino-Temperature-Control-Library
#endif	
	//#include "AsyncPing.h"
	#include <Pinger.h>
	#include "ntp.h"
	extern RemoteDebug telnet;
	void setup_mDNS();
#else
	#define _DEBUGR			0	
	#define _DEBUG   	    0		
#endif

#if (MCP2317) 
	#include <Wire.h>
	#include "Adafruit_MCP23017_MY.h"
#endif
#if (RS485)
#include "ModbusRtu.h"
#endif

//global strings
const char* const twodot = "\":\"";
const char* const comma = "\",\"";
//String openbrk2 = "{";
const char* const opensqr = "\":[\"";
const char* const closesqr = "\"]}";
const char* const closesqr2 = "\"],\"";
const char* const closesqr3 = "\"],";
const char* const closesqr4 = "\"]";
const char* const closebrk = "\"}";
const char* const enda = "\",";
const char* const end = "\"";
const char* const endbrk = "}";
const char* const  openbrk = "{\"";
static char s[300];
static char* t;
static char* frst;


//#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))
//-----------------------_DEBUG1 MACRO------------------------------------------------------------
//#define F(string_literal) (reinterpret_cast<const const __FlashStringHelper *>(PSTR(string_literal)))
//#define PGMT( pgm_ptr ) ( reinterpret_cast< const const __FlashStringHelper * >( pgm_ptr ) )

//legge gli ingressi dei tasti gi� puliti dai rimbalzi
/*
#if (MCP2317) 
    #define leggiTastiLocali()  in[BTN1IN] = !mcp.digitalRead(BTN1U);	\		
		in[BTN2IN] = !mcp.digitalRead(BTN1D);	\
		in[BTN1IN+BTNDIM] = !mcp.digitalRead(BTN2U); 	\
		in[BTN2IN+BTNDIM] = !mcp.digitalRead(BTN2D)	
#else										
	#define leggiTastiLocali()  in[BTN1IN] =!digitalRead(BTN1U);	\
		in[BTN2IN] = !digitalRead(BTN1D);	\
		in[BTN1IN+BTNDIM] = !digitalRead(BTN2U); 	\
		in[BTN2IN+BTNDIM] = !digitalRead(BTN2D)				
#endif

*/
//global strings
/*
#define twodot		"\":\""
#define comma   	"\",\""
#define openbrk 	"{\""
//#define openbrk2 = "{";
#define opensqr 	"\":[\""
#define closesqr 	"\"]}"
#define closesqr2 	"\"],\""
#define closesqr3 	"\"],"
#define closesqr4 	"\"]"
#define closebrk 	"\"}"
#define enda 		"\","
#define end 		"\""
#define endbrk 		"}"
*/
//#define logMsg2 = "{\"topic\":\"%s\",\"funct\":\"%s\",\"line\":\"%s\",\"msg\":\"%s\"}";
	
#define p(x) 	x + USRMODIFICABLEFLAGS
#define pp(x) 	(x < USRMODIFICABLEFLAGS)?x:x + USRMODIFICABLEFLAGS
#define ESP8266_REG(addr) *((volatile uint32_t *)(0x60000000+(addr)))
#define GPI    ESP8266_REG(0x318) //GPIO_IN RO (Read Input Level)
#define GPIP(p) ((GPI & (1 << ((p) & 0xF))) != 0)

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(uint8_t)  \
  (uint8_t & 0x80 ? '1' : '0'), \
  (uint8_t & 0x40 ? '1' : '0'), \
  (uint8_t & 0x20 ? '1' : '0'), \
  (uint8_t & 0x10 ? '1' : '0'), \
  (uint8_t & 0x08 ? '1' : '0'), \
  (uint8_t & 0x04 ? '1' : '0'), \
  (uint8_t & 0x02 ? '1' : '0'), \
  (uint8_t & 0x01 ? '1' : '0') 

#if (_DEBUG)
 #define DEBUG1_PRINT(x)	dbg1->print(x)
 // #define DEBUG1_PRINT(x)	Serial.print(x)
 #define DEBUG1_PRINTLN(x)	dbg1->println(x)
 //#define DEBUG1_PRINTLN(x)	Serial.println(x)
 #define DEBUG2_PRINT(x)	dbg2->print(x)
 #define DEBUG2_PRINTLN(x)	dbg2->println(x)
 //#define DEBUG2_PRINT(x)	Serial.print(x)
 //#define DEBUG2_PRINTLN(x)	Serial.println(x)

 //#define telnet_print(x) 	if (telnet.isActive(telnet.ANY)) 	telnet.print(x)
 #if (RS485)
	#define DEBUG_PRINT(x)   	Serial1.print (x)
    #define DEBUG_PRINTLN(x)  	Serial1.println (x)
 #else
	#define DEBUG_PRINT(x)   	Serial.print (x)
	#define DEBUG_PRINTLN(x)  	Serial.println (x)
 #endif
 //#define DEBUG_PRINTDEC(x)     Serial.print (x, DEC);  telnet.print(x)
#elif (_DEBUGR)
  #define DEBUG_PRINT(x)   telnet.print(x)
  #define DEBUG_PRINTLN(x) telnet.println(x)
#else 
 #define DEBUG_PRINT(x)
 //#define DEBUG_PRINTDEC(x)
 #define DEBUG_PRINTLN(x) 
#endif	
#define SET_BIT(p,n) ((p) |= (1 << (n-1)))
#define CLR_BIT(p,n) ((p) &= (~(1 << (n-1))))
#define TGL_BIT(p,n) ((p) ^ ((1 << (n-1))))
#define GET_BIT(p,n) ((p) (& (~(1 << (n-1)))) != 0) 
#define BIT_WRITE(c,p,m) (c ? SET_BIT(p,m) : CLR_BIT(p,m))
extern char gbuf[DATEBUFLEN];

//Event classes---------------------------------------------------------------------------------------------
class BaseEvnt{
	public:
		unsigned long pid;
		bool acc = false;
		bool active = false;
		uint8_t count = 0;
		BaseEvnt(){};
		BaseEvnt(unsigned id){pid = id;};
		virtual void doaction(uint8_t) = 0;
};

class MQTTBTN_Evnt: public BaseEvnt{
	public:
		MQTTBTN_Evnt(unsigned x = 0):BaseEvnt(x){};
		void doaction(uint8_t);
		//void loadPid(uint8_t);
};

class MQTTMAC_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTIP_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTMQTTID_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTTIME_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTTEMP_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTMEANPWR_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTPEAKPWR_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
#if (AUTOCAL_HLW8012) 
class DOPWRCAL_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class INSTPWR_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class INSTACV_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class CALPWR_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class PWRMULT_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class CURRMULT_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class VACMULT_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class ACVOLT_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
#endif
//-----------------------------------------------------------------------
class UTCVAL_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};

class NTPADDR2_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class NTPADDR1_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTADDR_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTCONNCHANGED_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTINTOPIC_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class WIFICHANGED_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class UTCSYNC_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class UTCADJ_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class UTCSDT_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class UTCZONE_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class ACTIONEVAL_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class ONCOND1_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class ONCOND2_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class ONCOND3_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class ONCOND4_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class ONCOND5_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class LOGSLCT_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class STDELX_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
#if (ROLLER)
class SLATSRATIO_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class BARRELRAD_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class THICKNESS_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class TLENGTH_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class VALWEIGHT_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
#endif
class SWROLL1_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class SWROLL2_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class THALTX_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MQTTID_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
#if(RS485) 
class MBUSVEL_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class MBUSID_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
#endif	
/*
class SWSPLDPWR1_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class SWSPLDPWR2_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class SWSPLDPWR3_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class SWSPLDPWR4_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
*/

/*
class WEBUSR_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
class WEBPSW_Evnt: public BaseEvnt{
	public:
		void doaction(uint8_t);
};
*/

class Par{
	public:
		char * jsoname;
		char * formname = "empty";
		unsigned eprom;
		char formfield;
		char partype;
		BaseEvnt *e = NULL;
		bool doalws;
		uint8_t val = 0;  //Attention! shadowed property, is referred by early binding on base class! (you must use static cast for read access)
		
		Par(const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false);
		
		String getStrFormName();				//base class version
		String getStrJsonName();				//base class version
		char getFormField();
		char getType();
		void doaction(uint8_t = 0);			//base class version
		virtual void writeParam(String) = 0;	//late binding abstract
		virtual void loadFromStr(String) = 0;	//late binding abstract
		virtual void saveOnEprom() = 0;			//late binding abstract
		virtual String getStrVal() = 0;			//late binding abstract
		virtual void loadFromEprom() = 0;		//late binding abstract
		virtual void load(uint8_t); 			//late binding
		virtual void load(int);					//late binding
		virtual void load(unsigned long);		//late binding
		virtual void load(unsigned);		//late binding
		virtual void load(float);				//late binding
		virtual void load(double);				//late binding
		virtual void load(char*);				//late binding
		virtual void load(String);				//late binding
};

class ParUint8 : public Par{
	 
	public:
		uint8_t val;
		ParUint8(uint8_t v, const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false):Par(x,y,z,u,t,w,k){val = v;};
		
		String getStrVal();
		void writeParam(String);
		void loadFromStr(String);
		void loadFromEprom();
		void saveOnEprom();
		void load(uint8_t);
};

class ParInt : public Par{
	public:
		int val;
		ParInt(int v, const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false):Par(x,y,z,u,t,w,k){val = v;};
		
		String getStrVal();
		void writeParam(String);
		void loadFromStr(String);
		void loadFromEprom();
		void saveOnEprom();
		void load(int);
};

class ParLong : public Par{
	public:
		unsigned long val;
		ParLong(unsigned long v, const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false):Par(x,y,z,u,t,w,k){val = v;};
		
		String getStrVal();
		void writeParam(String);
		void loadFromStr(String);
		void loadFromEprom();
		void saveOnEprom();
		void load(unsigned long);
		void load(unsigned int);
};

class ParFloat : public Par{
	public:
		float val;
		ParFloat(float v, const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false):Par(x,y,z,u,t,w,k){val = v;}
		
		String getStrVal();
		void writeParam(String);
		void loadFromStr(String);
		void loadFromEprom();
		void saveOnEprom();
		void load(float);
		void load(double);
};

class ParStr32 : public Par{
	public:
		char val[32];
		ParStr32(const char* v, const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false):Par(x,y,z,u,t,w,k){this->load((char*)v);};
		
		String getStrVal();
		void writeParam(String);
		void loadFromStr(String);
		void loadFromEprom();
		void saveOnEprom();
		void load(char *);
};

class ParStr64 : public Par{
	public:
		char val[64];
		ParStr64(const char* v, const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false):Par(x,y,z,u,t,w,k){this->load((char*)v);};
		
		String getStrVal();
		void writeParam(String);
		void loadFromStr(String);
		void loadFromEprom();
		void saveOnEprom();
		void load(char *);
};

class ParVarStr : public Par{
	public:
		String val;
		ParVarStr(const char* v, const char* x = "empty", const char* y = "" , unsigned z = 2, char u = 'n', char t = 'n', BaseEvnt * w = NULL, bool k = false):Par(x,y,z,u,t,w,k){this->load(v);};
		
		//using Par::load;
		String getStrVal();
		void writeParam(String);
		void loadFromStr(String);
		void saveOnEprom();
		void loadFromEprom();
		void load(String);
};

class MQTTC{
	private:
		MQTT *mqt = NULL;
		char result[RSLTBUFLEN];
		byte pos = DATEBUFLEN + 1;
		char* lp; 
		char* topic;
		char* func;
	public:
		//MQTTC(MQTT *x){mqt = x; memset(result, ' ', DATEBUFLEN); result[0] = '\0';};
		MQTTC(MQTT *x){
			mqt = x;
			lp = result;	
			strcpy(result,openbrk); 
			topic = "";
			func = "";
		};
	
		//void mqttSend(const char *c, bool b = false, char* topic = NULL, char* func = NULL);
		void printTags(const char* tpc, const char* fnc = "");
		void mqttSend(const char *c, bool b = false);
		void setMQTTClient(MQTT *x){mqt = x;};
};
//End of event classes------------------------------------------------------------------------------------------------------------
class BaseLog{
	protected:
		bool ontlnt = false;
		bool onsrl = false;
		MQTTC *mqtc = NULL;
	public:
		bool isTelnet();
		bool isSerial();
		byte getLevel(){return level;};
		uint8_t level;
		BaseLog(uint8_t lev){level = lev;};
		virtual void printTags(const char* topic,const char* func = NULL);
		virtual void print(const char*);
		virtual void println(const char*);
		virtual void print(const __FlashStringHelper *);
		virtual void println(const __FlashStringHelper *);
		virtual void print(String msg);
		virtual void println(String msg);
		virtual void print(char msg);
		virtual void println(char msg);
		virtual void print(uint8_t msg);
		virtual void println(uint8_t msg);
		virtual void print(int msg);
		virtual void println(int msg);
		virtual void print(long msg);
		virtual void println(long msg);
		virtual void print(unsigned int msg);
		virtual void println(unsigned int msg);
		virtual void print(unsigned long msg);
		virtual void println(unsigned long msg);
		virtual void print(float msg);
		virtual void println(float msg);
		virtual void print(double msg);
		virtual void println(double msg);
		//virtual void print(double msg);
		//virtual void println(double msg);
		virtual ~BaseLog();
		virtual void destroy();
		/*virtual ~SerialLog();
		virtual ~TelnetLog();
		virtual ~MQTTLog();
		virtual ~SerialTelnetLog();
		virtual ~SerialMQTTLog();
		virtual ~TelnetMQTTLog();
		virtual ~SerialTelnetMQTTLog();*/
};

class SerialLog: public BaseLog{
	public:
		SerialLog(uint8_t x):BaseLog(x){ontlnt = false; onsrl = true; ontlnt = false; mqtc = NULL;Serial1.begin(115200);};
		void printTags(const char* topic,const char* func = NULL);
		void print(const char*);
		void println(const char*);
		void print(const __FlashStringHelper *);
		void println(const __FlashStringHelper *);
		void print(String msg);
		void println(String msg);
		void print(char);
		void println(char);
		void print(uint8_t msg);
		void println(uint8_t msg);
		void print(int msg);
		void println(int msg);
		void print(long msg);
		void println(long msg);
		void print(unsigned long msg);
		void println(unsigned long msg);
		void print(unsigned int msg);
		void println(unsigned int msg);
		void print(float msg);
		void println(float msg);
		void print(double msg);
		void println(double msg);
		void destroy();
		~SerialLog();
};
class TelnetLog: public BaseLog{
	protected:
		RemoteDebug *tel;
	public:
		TelnetLog(uint8_t x, RemoteDebug* y):BaseLog(x){tel = y; onsrl = false; ontlnt = true; mqtc = NULL;};
		void printTags(const char* topic,const char* func = NULL);
		void print(const char*);
		void println(const char*);
		void print(const __FlashStringHelper *);
		void println(const __FlashStringHelper *);
		void print(String msg);
		void println(String msg);
		void print(char);
		void println(char);
		void print(uint8_t msg);
		void println(uint8_t msg);
		void print(int msg);
		void println(int msg);
		void print(long msg);
		void println(long msg);
		void print(unsigned long msg);
		void println(unsigned long msg);
		void print(float msg);
		void println(float msg);
		void print(unsigned int msg);
		void println(unsigned int msg);
		void print(double msg);
		void println(double msg);
		void destroy();
		~TelnetLog();
};
class MQTTLog: public BaseLog{
	public:
		MQTTLog(uint8_t x, MQTTC *y):BaseLog(x){mqtc = y; onsrl = false; ontlnt = false;};
		void printTags(const char* topic,const char* func = NULL);
		void print(const char*);
		void println(const char*);
		void print(const __FlashStringHelper *);
		void println(const __FlashStringHelper *);
		void print(String msg);
		void println(String msg);
		void print(char);
		void println(char);
		void print(uint8_t msg);
		void println(uint8_t msg);
		void print(int msg);
		void println(int msg);
		void print(long msg);
		void println(long msg);
		void print(unsigned long msg);
		void println(unsigned long msg);
		void print(float msg);
		void println(float msg);
		void print(unsigned int msg);
		void println(unsigned int msg);
		void print(double msg);
		void println(double msg);
		void destroy();
		~MQTTLog();
};
class SerialTelnetLog: public BaseLog{
	protected:
		RemoteDebug *tel;
	public:
		SerialTelnetLog(uint8_t x, RemoteDebug* y):BaseLog(x){tel = y; onsrl = true; ontlnt = true; mqtc = NULL;Serial1.begin(115200);};
		void printTags(const char* topic,const char* func = NULL);
		void print(const char*);
		void println(const char*);
		void print(const __FlashStringHelper *);
		void println(const __FlashStringHelper *);
		void print(String msg);
		void println(String msg);
		void print(char);
		void println(char);
		void print(uint8_t msg);
		void println(uint8_t msg);
		void print(int msg);
		void println(int msg);
		void print(long msg);
		void println(long msg);
		void print(unsigned long msg);
		void println(unsigned long msg);
		void print(float msg);
		void println(float msg);
		void print(unsigned int msg);
		void println(unsigned int msg);
		void print(double msg);
		void println(double msg);
		void destroy();
		~SerialTelnetLog();
};
class SerialMQTTLog: public BaseLog{
	public:
		SerialMQTTLog(uint8_t x, MQTTC *y):BaseLog(x){mqtc = y; onsrl = true; ontlnt = false;Serial1.begin(115200);};
		void printTags(const char* topic,const char* func = NULL);
		void print(const char*);
		void println(const char*);
		void print(const __FlashStringHelper *);
		void println(const __FlashStringHelper *);
		void print(String msg);
		void println(String msg);
		void print(char);
		void println(char);
		void print(uint8_t msg);
		void println(uint8_t msg);
		void print(int msg);
		void println(int msg);
		void print(long msg);
		void println(long msg);
		void print(unsigned long msg);
		void println(unsigned long msg);
		void print(float msg);
		void println(float msg);
		void print(unsigned int msg);
		void println(unsigned int msg);
		void print(double msg);
		void println(double msg);
		void destroy();
		~SerialMQTTLog();
};
class TelnetMQTTLog: public BaseLog{
	protected:
		RemoteDebug *tel;
	public:
		TelnetMQTTLog(uint8_t x, RemoteDebug* y, MQTTC *z):BaseLog(x){tel = y; mqtc = z; onsrl = false; ontlnt = true;};
		void printTags(const char* topic,const char* func = NULL);
		void print(const char*);
		void println(const char*);
		void print(const __FlashStringHelper *);
		void println(const __FlashStringHelper *);
		void print(String msg);
		void println(String msg);
		void print(char);
		void println(char);
		void print(uint8_t msg);
		void println(uint8_t msg);
		void print(int msg);
		void println(int msg);
		void print(long msg);
		void println(long msg);
		void print(unsigned long msg);
		void println(unsigned long msg);
		void print(float msg);
		void println(float msg);
		void print(unsigned int msg);
		void println(unsigned int msg);
		void print(double msg);
		void println(double msg);
		void destroy();
		~TelnetMQTTLog();
};
class SerialTelnetMQTTLog: public BaseLog{
	protected:
		RemoteDebug *tel;
	public:
		SerialTelnetMQTTLog(uint8_t x, RemoteDebug* y, MQTTC *z):BaseLog(x){tel = y; mqtc = z; onsrl = true; ontlnt = true;Serial1.begin(115200);};
		void printTags(const char* topic,const char* func = NULL);
		void print(const char*);
		void println(const char*);
		void print(const __FlashStringHelper *);
		void println(const __FlashStringHelper *);
		void print(String msg);
		void println(String msg);
		void print(char);
		void println(char);
		void print(uint8_t msg);
		void println(uint8_t msg);
		void print(int msg);
		void println(int msg);
		void print(long msg);
		void println(long msg);
		void print(unsigned long msg);
		void println(unsigned long msg);
		void print(float msg);
		void println(float msg);
		void print(unsigned int msg);
		void println(unsigned int msg);
		void print(double msg);
		void println(double msg);
		void destroy();
		~SerialTelnetMQTTLog();
};

extern BaseLog* dbg1;
extern BaseLog* dbg2;
/*
class BaseLocalEvnt{
	protected:
		bool ontlnt = false;
		MQTTC *mqtc = NULL;
	public:
		bool isTelnet();
		byte getLevel(){return level;};
		uint8_t level;
		BaseLog(uint8_t lev){level = lev;};
		virtual void print(const char*);
		virtual void println(const char*);
};
*/

//PRIMA DEFINISCO LE COSTANTI, POI INCLUDO I FILES HEADERS (.h) CHE LE USANO
#include "tapparellaLogic.h"
#include "serialize.h"
#include "logicaTasti.h"

#if (AUTOCAL)  
#include "statistics.h"
#endif

void printIn();
void setup_AP(bool);
void setup_wifi();
void mqttReconnect();
void mqttCallback(String&, String&);
void readStatesAndPub(byte nn, bool all = false, bool shortm = false);
void readAvgPowerAndPub();
void readPeakPowerAndPub();
void readTempAndPub();
void readMacAndPub();
void readIpAndPub();
void readTimeAndPub();
void readMQTTIdAndPub();
void readParamAndPub(uint8_t, char*, bool ip = false);
void readModeAndPub(uint8_t);
void readActModeAndPub(uint8_t);
void readPwrCalAndPub();
void publishStr(bool ip = false);
void publishStr2();
float getAmpRMS(float);
double getTemperature();
//void leggiTasti();
void scriviOutDaStato(byte);
void saveOnEEPROM(int);
void loadConfig();
void rebootSystem();
void onStationConnected(const WiFiEventSoftAPModeStationConnected&);
void onStationDisconnected(const WiFiEventSoftAPModeStationDisconnected&);
void testFlash();
void initIiming(bool);
void printConfig();
void processCmdRemoteDebug();
void webSocketEvent(uint8_t, WStype_t, uint8_t *, size_t);
void setSWMode(uint8_t, uint8_t);
void saveConf(unsigned);
void loadConf(unsigned);
void saveSingleConf(unsigned);
void saveSingleJson(unsigned);
void printFixedParam(unsigned);
bool saveParamFromForm(unsigned);
float writeFloatConf(unsigned, float);
float saveFloatConf(unsigned);
long saveLongConf(unsigned);
int saveIntConf(unsigned);
uint8_t saveByteConf(unsigned);
void updtConf(unsigned, String);
unsigned getConfofstFromParamofst(unsigned);
void printOut();
void rstldcnt(uint8_t);
#if (AUTOCAL_HLW8012) 
void calibrate_pwr(double pwr);
void readIpwrAndPub();
void readIacvoltAndPub();
#elif (AUTOCAL_ACS712) 
void zeroDetect();
#endif
void stopPageLoad();
void startPageLoad();
void setPinger();
void testSwitch(byte);
void testRoller(byte);
String getLastFeedBack();
void printSwitch(byte);
/*
//http server callback function prototypes
void handleRoot(ESP8266WebServer (&), String const (&)[PARAMSDIM]);        // function prototypes for HTTP handlers
void handleLogin(ESP8266WebServer (&),  String const (&)[PARAMSDIM], String const (&)[MQTTDIM]);
void handleNotFound(ESP8266WebServer (&));
void handleModify(ESP8266WebServer (&), String (&)[PARAMSDIM], String (&)[MQTTDIM]);
void handleWifiConf(ESP8266WebServer (&),  String const (&)[PARAMSDIM], String const (&)[MQTTDIM]);
void handleSystemConf(ESP8266WebServer (&),  String const (&)[PARAMSDIM], String const (&)[MQTTDIM]);
void handleMQTTConf(ESP8266WebServer (&),  String const (&)[PARAMSDIM], String const (&)[MQTTDIM]);
void handleLogicConf(ESP8266WebServer (&),  String const (&)[PARAMSDIM], String const (&)[MQTTDIM]);
void handleCmd(ESP8266WebServer (&),  String const (&)[PARAMSDIM], String const (&)[MQTTDIM]);
*/
//void handleCmdJson(ESP8266WebServer (&), String&);
bool is_authentified(ESP8266WebServer&);
// function prototypes for HTTP handlers
void initCommon(ESP8266WebServer *,  Par**);

void handleFavicon();
void handleLogConf();
void handleMqttCmd();
void handleRoot();              
void handleLogin();
void handleNotFound();
void handleModify();
void handleSystemConf();
void handleWifiConf();
void handleMQTTConf();
void handleCmd();
void handleBtnCmd();
void handleLogicConf();
void handleModify();
void handleEventConf();
void writeOnOffConditions();
void writeOnOffAction(uint8_t, uint8_t);
void writeSWMode(uint8_t, uint8_t);
void writeHaltDelay(unsigned int, uint8_t);
void setTimers();
//void readMqttConfAndSet(int);
#endif
/*
{ "Sonoff 4CH",      // Sonoff 4CH (ESP8285)

     GPIO_KEY1,        // GPIO00 Button 1

     GPIO_USER,        // GPIO01 Serial RXD and Optional sensor

     GPIO_USER,        // GPIO02 Optional sensor (riservato alla selezione del boot mode, pu� essere in o out ma al boot deve essere sempre alto)

     GPIO_USER,        // GPIO03 Serial TXD and Optional sensor

     GPIO_REL3,        // GPIO04 Sonoff 4CH Red Led and Relay 3 (0 = Off, 1 = On)

     GPIO_REL2,        // GPIO05 Sonoff 4CH Red Led and Relay 2 (0 = Off, 1 = On)

     0, 0, 0,          // Flash connection

     GPIO_KEY2,        // GPIO09 Button 2

     GPIO_KEY3,        // GPIO10 Button 3

     0,                // Flash connection

     GPIO_REL1,        // GPIO12 Red Led and Relay 1 (0 = Off, 1 = On)

     GPIO_LED1_INV,    // GPIO13 Blue Led (0 = On, 1 = Off)

     GPIO_KEY4,        // GPIO14 Button 4

     GPIO_REL4,        // GPIO15 Red Led and Relay 4 (0 = Off, 1 = On)

     0, 0

  },
  
 */
