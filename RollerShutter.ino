#include "common.h"
//End MQTT config-------------------------------------------
//inizio variabili e costanti dello schedulatore (antirimbalzo)
#define nsteps          12000        // numero di fasi massimo di un periodo generico

//String s = ""; //global message buffer
byte npinger;
byte firstTemp = NOMEDIATEMP;
char IP[] = "(IP unset)";          // buffer
//end global string
//stats variables
#if (ROLLER)
unsigned short isrundelay[2] = {RUNDELAY, RUNDELAY};
#endif
double ex[2] = {0,0};
double calAvg[2] = {0,0};
double weight[2] = {0,0};
short chk[2]={0,0};
bool isrun[2]={false,false};
volatile bool dosmpl = false;

float overallSwPower = 0;
float overallSwPower2 = 0;
//float overallSwPower2 = 0;
bool ledState[2];
int samples[10];
uint8_t indx = 0;
uint8_t stat;
bool mov = false;
uint8_t lastPing[4] = {0, 0, 0, 0};
unsigned pingCnt[4] = {0, 0, 0, 0};
bool toPing[4] = {false, false, false, false};
uint8_t nping[4] = {3, 3, 3, 3};
uint8_t pingPer[4] = {10, 10, 10, 10};
uint8_t pingPer2[4] = {10, 10, 10, 10};
bool pingRes[4] = {false, false, false, false};
//char * destips[4] = {"", "", "", ""};
float vcosfi = 220*0.8;
double ACSVolt;
unsigned int mVperAmp = 185;   // 185 for 5A, 100 for 20A and 66 for 30A Module
double ACSVoltage = 0;
//double peak = 0;
double VRMS = 0;
double VMCU = 0;
double AmpsRMS = 0;
int zero, l=0, h=1024;
unsigned wificnt = 0;
boolean pageLoad = false;
unsigned long smplcnt, smplcnt2; //sampleCount;
unsigned short zeroCnt = 0;
unsigned long mqttcnt = 0;
unsigned short mqttofst = 0;
unsigned pushCnt = 0;
//double ex=0;
unsigned long n=1;
unsigned long pn=0;
volatile int minx = 1024;
volatile int maxx = 0;
volatile int x;
float dd = 0;
byte ipcount = 0;
byte tempCount = TEMPCOUNT;
volatile double st = 0;
volatile double stv = 85;
volatile double st2 = 0;
volatile double stv2 = 0;
volatile float m, m2;

bool zsampled = false;
uint8_t cont=0;
//end of stats variables
unsigned long prec=0;
unsigned long current = 0;
wl_status_t wfs;
uint8_t wifiState = WIFISTA;
volatile bool wifiConn, keepConn, startWait, endWait, lastWifiConn, ipSet;
unsigned long _connectTimeout  = 10*1000;
//int haltPrm[2] = {THALT1,THALT2};
//int haltOfs[2] = {THALT1OFST,THALT2OFST};
uint8_t blocked[2]={0,0};
//unsigned long edelay[2]={0,0};
uint8_t wsnconn = 0;
IPAddress myip(192, 168, 43, 1);
IPAddress mygateway(192, 168, 43, 1);
IPAddress mysubnet(255, 255, 255, 0);
bool roll[2] = {true, true};
#if (AUTOCAL)
//uint8_t halfStop = STOP_STEP * TBASE / 2;
uint8_t halfStop =0;
#endif
#if (MCP2317) 
Adafruit_MCP23017_MY mcp;	
#endif

#if(LARGEFW)
RemoteDebug telnet;
OneWire oneWire(ONE_WIRE_BUS);
#if (TEMP)
DallasTemperature DS18B20(&oneWire);
#endif
#endif


WiFiEventHandler stationConnectedHandler;
WiFiEventHandler stationDisconnectedHandler;

char gbuf2[50];
//WiFiEventHandler gotIpEventHandler, disconnectedEventHandler, connectedEventHandler;

//User config
//--------------------------------------------------------------------------------------
//Valore iniziale: il suo contenuto viene poi caricato da EEPROM
unsigned int thalt1=5000;
unsigned int thalt3=5000;
unsigned int thalt2=5000;
unsigned int thalt4=5000;
//End JSON config------------------------------------------
//JSON config----------------------------------------------
//{"OUTSLED":"0","up1":"1","down1":"0","up2":"50","down2":"0", pr1:"12", pr2:"76"}
int ncifre=4;
//vettori di ingresso, uscite e stato
uint8_t in[NBTN*BTNDIM], out[NBTN*BTNDIM], outPorts[NBTN*BTNDIM], inPorts[NBTN*BTNDIM];
float outPwr[NBTN*BTNDIM];
byte doPwrSpl[NBTN*BTNDIM];
short doZeroSampl = -1;
short doPwrSampl = -1;
bool inflag = false;
Par *pars[PARAMSDIM];
unsigned long *inl = (unsigned long *)in;

ESP8266WebServer server(80);    	// Create a webserver object that listens for HTTP request on port 80
WebSocketsServer webSocket(81);	    // create a websocket server on port 81
ESP8266HTTPUpdateServer httpUpdater;
extern "C"
{
  #include <lwip/icmp.h> // needed for icmp packet definitions
}

// Set global to avoid object removing after setup() routine
Pinger pinger[4];

// fine variabili e costanti dello schedulatore
// Update these with values suitabule for your network.
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
//                                      End User config
//--------------------------------------------------------------------------------------
//Port config----------------------------------------------
//LED and relays switches
bool boot=true;
uint8_t swcount=0;
uint8_t wifindx=0;
//End port config------------------------------------------
volatile unsigned int step;
//bool extCmd=false;
//numerazione porte
//uint8_t outLogic[NBTN*RSTATUSDIM];
//inizio variabili globali antirimbalzo----------------------------------------------
//fine variabili globali antirimbalzo----------------------------------------------
//dichiara una variabile oggetto della classe WiFiClient (instanzia il client WiFi)
//WiFiClient espClient;
//dichiara una variabile oggetto della classe PubSubClient (instanzia il client MQTT)
// data array for modbus network sharing
#if(RS485)
byte modbusState;
uint16_t au16data[REGDIM] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; 
//byte usecase[REGDIM] = {1, 2, 3, 4};
/**
 *  Modbus object declaration
 *  u8id : node id = 0 for master, = 1..247 for slave
 *  u8serno : serial port (use 0 for Serial)
 *  u8txenpin : 0 for RS-232 and USB-FTDI 
 *               or any pin number > 1 for RS-485
 */
Modbus slave(MODBUSID,0); // this is slave @1 and RS-232 or USB-FTDI 
#endif
// create MQTT
MQTT *mqttClient = NULL;
MQTTC *mqttc = new MQTTC(mqttClient);

bool configLoaded=true;
bool mqttConnected=false;
bool mqttAddrChanged=true;
bool dscnct=true;
//String pr[3] = {"{\"pr1\":\"", "{\"pr2\":\"", "\"}"};

void printIn(){
	DEBUG1_PRINT(F("in array: "));
	DEBUG1_PRINT(in[0]);
	DEBUG1_PRINT(F(", "));
	DEBUG1_PRINT(in[1]);
	DEBUG1_PRINT(F(", "));
	DEBUG1_PRINT(in[2]);
	DEBUG1_PRINT(F(", "));
	DEBUG1_PRINTLN(in[3]);
}

inline float sampletemp(byte c, byte t = 0){
	double temp = -127;
#if(LARGEFW && TEMP)	
#if (RS485)
	/*if(dbg1->isSerial() || dbg2->isSerial()){
		//flush TX serial buffer
		Serial1.flush();
		//flush RX serial buffer
		while(Serial1.available())
			Serial1.read();
		//Serial1.end();
	}*/
	if(!dbg1->isSerial() && !dbg2->isSerial()){
		DS18B20.requestTemperatures(); 
		unsigned short cnt = 0;
		do{
			temp = DS18B20.getTempCByIndex(0);
			cnt++;	
			delay(t);
		}while((temp == 85.0 || temp == (-127.0)) && cnt < c);
	}
	/*if(dbg1->isSerial() || dbg2->isSerial()){
		Serial1.begin(115200);
		//flush TX serial buffer
		Serial1.flush();
        //flush RX serial buffer
		while(Serial1.available())
			Serial1.read();
	}*/
#else
	DS18B20.requestTemperatures();
	unsigned short cnt = 0;
	do{
		temp = DS18B20.getTempCByIndex(0);
		cnt++;	
		delay(t);
	}while((temp == 85.0 || temp == (-127.0)) && cnt < c);
#endif
#endif
return temp;
}
/*
inline float sampletemp(byte c, byte t = 0){
	double temp = -127;
#if(LARGEFW && TEMP)	
#if (RS485)
	if(dbg1->isSerial() || dbg2->isSerial()){
		//flush TX serial buffer
		Serial1.flush();
		//flush RX serial buffer
		while(Serial1.available())
			Serial1.read();
		Serial1.end();
	}
#endif

	DS18B20.requestTemperatures(); 
	unsigned short cnt = 0;
	do{
		temp = DS18B20.getTempCByIndex(0);
		cnt++;	
		delay(t);
		DEBUG1_PRINT(F("Temp : "));DEBUG1_PRINTLN(temp);
	}while((temp == 85.0 || temp == (-127.0)) && cnt < c);
	DEBUG1_PRINT(F("Temp right : "));DEBUG1_PRINTLN(temp);
#if (RS485)
	if(dbg1->isSerial() || dbg2->isSerial()){
		Serial1.begin(115200);
		//flush TX serial buffer
		Serial1.flush();
        //flush RX serial buffer
		while(Serial1.available())
			Serial1.read();
	}
#endif
#endif
return temp;
}
*/
//-----------------------------------------Begin of prototypes---------------------------------------------------------
#if (AUTOCAL_HLW8012 && LARGEFW) 
HLW8012 hlw8012;

// When using interrupts we have to call the library entry point
		// whenever an interrupt is triggered
inline void ICACHE_RAM_ATTR hlw8012_cf1_interrupt() {
	hlw8012.cf1_interrupt();
}

inline void ICACHE_RAM_ATTR hlw8012_cf_interrupt() {
	hlw8012.cf_interrupt();
}

// Library expects an interrupt on both edges
void setInterrupts() {
	attachInterrupt(CF1_PIN, hlw8012_cf1_interrupt, CHANGE);
	attachInterrupt(CF_PIN, hlw8012_cf_interrupt, CHANGE);
}

void calibrate_pwr(double pwr = 0) {
	// Let some time to register values
	//unsigned long timeout = millis();
	//while ((millis() - timeout) < 10000) {
	//	delay(1);
	//}
	DEBUG1_PRINT(F("CALPWR : ")); DEBUG1_PRINTLN(static_cast<ParFloat*>(pars[p(CALPWR)])->val);
	hlw8012.resetMultipliers();
	// Calibrate using a 60W bulb (pure resistive) on a 230V line
	if(pwr > 0){
		hlw8012.expectedActivePower(static_cast<ParFloat*>(pars[p(CALPWR)])->val, pwr);
	}else{
		hlw8012.expectedActivePower(static_cast<ParFloat*>(pars[p(CALPWR)])->val);
	}
	hlw8012.expectedVoltage(static_cast<ParUint8*>(pars[p(ACVOLT)])->val);
	hlw8012.expectedCurrent((float) static_cast<ParFloat*>(pars[p(CALPWR)])->val / static_cast<ParUint8*>(pars[p(ACVOLT)])->val);
	
	DEBUG1_PRINT(F("[HLW] Expected power : ")); DEBUG1_PRINTLN(static_cast<ParFloat*>(pars[p(CALPWR)])->val);
	DEBUG1_PRINT(F("[HLW] Expected voltage : ")); DEBUG1_PRINTLN(static_cast<ParUint8*>(pars[p(ACVOLT)])->val);
	DEBUG1_PRINT(F("[HLW] Expected current : ")); DEBUG1_PRINTLN((float) static_cast<ParFloat*>(pars[p(CALPWR)])->val / static_cast<ParUint8*>(pars[p(ACVOLT)])->val);
	//Save parameter in the system array
	static_cast<ParFloat*>(pars[p(PWRMULT)])->load(hlw8012.getPowerMultiplier());
	static_cast<ParFloat*>(pars[p(CURRMULT)])->load(hlw8012.getCurrentMultiplier());
	static_cast<ParFloat*>(pars[p(VACMULT)])->load(hlw8012.getVoltageMultiplier());
	
	// Show corrected factors
	DEBUG1_PRINT(F("[HLW] New current multiplier : ")); DEBUG1_PRINTLN(hlw8012.getCurrentMultiplier());
	DEBUG1_PRINT(F("[HLW] New voltage multiplier : ")); DEBUG1_PRINTLN(hlw8012.getVoltageMultiplier());
	DEBUG1_PRINT(F("[HLW] New power multiplier   : ")); DEBUG1_PRINTLN(hlw8012.getPowerMultiplier());
}

void HLW8012_init(){
		// Initialize HLW8012
		// void begin(unsigned char cf_pin, unsigned char cf1_pin, unsigned char sel_pin, unsigned char currentWhen = HIGH, bool use_interrupts = false, unsigned long pulse_timeout = PULSE_TIMEOUT);
		// * cf_pin, cf1_pin and sel_pin are GPIOs to the HLW8012 IC
		// * currentWhen is the value in sel_pin to select current sampling
		// * set use_interrupts to true to use interrupts to monitor pulse widths
		// * leave pulse_timeout to the default value, recommended when using interrupts
		hlw8012.begin(CF_PIN, CF1_PIN, SEL_PIN, CURRENT_MODE, true);

		// These values are used to calculate current, voltage and power factors as per datasheet formula
		// These are the nominal values for the Sonoff POW resistors:
		// * The CURRENT_RESISTOR is the 1milliOhm copper-manganese resistor in series with the main line
		// * The VOLTAGE_RESISTOR_UPSTREAM are the 5 470kOhm resistors in the voltage divider that feeds the V2P pin in the HLW8012
		// * The VOLTAGE_RESISTOR_DOWNSTREAM is the 1kOhm resistor in the voltage divider that feeds the V2P pin in the HLW8012
		hlw8012.setResistors(CURRENT_RESISTOR, VOLTAGE_RESISTOR_UPSTREAM, VOLTAGE_RESISTOR_DOWNSTREAM);

		// Show default (as per datasheet) multipliers
		DEBUG1_PRINT(F("[HLW] New current multiplier : ")); DEBUG1_PRINTLN(hlw8012.getCurrentMultiplier());
		DEBUG1_PRINT(F("[HLW] New voltage multiplier : ")); DEBUG1_PRINTLN(hlw8012.getVoltageMultiplier());
		DEBUG1_PRINT(F("[HLW] New power multiplier   : ")); DEBUG1_PRINTLN(hlw8012.getPowerMultiplier());

		setInterrupts();
}
#endif

//#if (MCP2317) 
inline void getbits(uint8_t num, uint8_t &r, uint8_t &g, uint8_t &b) 
{ 
    r = (num >> 2) & 1;
	g = (num >> 1) & 1;
	b = num & 1;
}
	
inline void setColor(uint8_t num) 
{   
	uint8_t r,g,b;

    
#if (MCP2317) 
	getbits(num, r, g, b);
	DEBUG1_PRINT(F("r: ")); DEBUG1_PRINTLN(r);
	DEBUG1_PRINT(F("g: ")); DEBUG1_PRINTLN(g);
	DEBUG1_PRINT(F("b: ")); DEBUG1_PRINTLN(b);
	mcp.digitalWrite(GREEN,!g);	
	mcp.digitalWrite(RED,!r);		
	mcp.digitalWrite(BLUE,!b);
#if (RS485)
	au16data[RDGPB] = mcp.readGPIO(1);
#endif
#endif
	
	//rgb
	//001 = 1
	//010 = 2
	//100 = 4
} 

/*
void testparam(int i){
	char * param;
	
	if(pars[i] != NULL){
		param = pars[i]->formname;
		DEBUG_PRINT(F("param("));
		DEBUG_PRINT(i);
		DEBUG_PRINT(F("): "));
		DEBUG_PRINTLN(param);
	}
}

void printparams(){
	DEBUG_PRINT(F("printparams "));
	DEBUG_PRINTLN(PARAMSDIM);
	for(int i=0; i<PARAMSDIM; i++){
		testparam(i);
	}
}
*/
unsigned getConfofstFromParamofst(unsigned pofst){
	if(pars[pofst] != NULL){
		if(pofst < USRMODIFICABLEFLAGS)
			return pofst;
		else
			return pofst - USRMODIFICABLEFLAGS;
	}
}

//-----------------------------------------End of prototypes---------------------------------------------------------
inline void initOfst(){
	//------------------------------------------
	//pars[i][0] - parameter eeprom offset
	//------------------------------------------
	//pars[i][1] - type of eeprom saved data
	//------------------------------------------
	//pars[i][2] - type of form field
	//------------------------------------------
	//i:input, textarea
	//s:select
	//n:no form
	//------------------------------------------
	//p:parameter
	//j:jsonname
	//----------------------------------------------
	//pars[PARAM_NAME] = new ParParam_type(initial_value_saved_on_eeprom_reset, "param_name", json_name", EEPROM_PARAM_OFST, 'j!p!n (type_of_load_from_eeprom)','i|s|n (type_of_load_from_form), new PARAM_NAME_MANAGER_Evnt(PARAM_NAME));
	for(int i=0; i<PARAMSDIM; i++){
		pars[i] = NULL;
	}
	/*1*/pars[MQTTUP1] = new ParUint8(0, "up1", "up1", MQTTUP1OFST, 'j','i', new MQTTBTN_Evnt(MQTTUP1));
#if !(ONEBTN)
	/*2*/pars[MQTTDOWN1] = new ParUint8(0, "down1", "down1", MQTTDOWN1OFST, 'j','i', new MQTTBTN_Evnt(MQTTDOWN1));
#if !(TWOBTN)	
	/*3*/pars[MQTTUP2] = new ParUint8(0, "up2", "up2", MQTTUP2OFST, 'j','i', new MQTTBTN_Evnt(MQTTUP2));
	/*4*/pars[MQTTDOWN2] = new ParUint8(0, "down2", "down2", MQTTDOWN2OFST, 'j','i', new MQTTBTN_Evnt(MQTTDOWN2));
#endif
#endif
	/*4*/pars[MQTTALL] = new ParUint8(0, "all","all", 2,'n','n');
	/*4*/pars[MQTTMAC] = new ParUint8(0, "mac", "mac", 2,'n','n', new MQTTMAC_Evnt());
	/*4*/pars[MQTTIP] = new ParUint8(0, "ip", "ip", 2,'n','n', new MQTTIP_Evnt());
	/*4*/pars[MQTTTEMP] = new ParUint8(0, "temp", "temp", 2, 'n','n', new MQTTTEMP_Evnt());
	/*4*/pars[MQTTMEANPWR] = new ParUint8(0, "avgpwr", "avgpwr", 2,'n','n', new MQTTMEANPWR_Evnt());
	/*4*/pars[MQTTPEAKPWR] = new ParUint8(0, "peakpwr", "peakpwr", 2,'n','n', new MQTTPEAKPWR_Evnt());
	/*4*/pars[MQTTTIME] = new ParUint8(0, "time", "time", 2, 'n','n', new MQTTTIME_Evnt());
	/*4*/pars[MQTTDATE] = new ParUint8(0, "date", "date", 2, 'n','n');
#if (AUTOCAL_HLW8012) 
	/*4*/pars[DOPWRCAL] = new ParUint8(0, "dopwrcal", "dopwrcal", 2, 'n','n', new DOPWRCAL_Evnt());
	/*4*/pars[INSTACV] = new ParUint8(0, "iacvolt", "iacvolt", 2,'n','n', new INSTACV_Evnt());
	/*4*/pars[INSTPWR] = new ParUint8(0, "ipwr", "ipwr", 2,'n','n', new INSTPWR_Evnt());
	//------------------------------------------------------------------------------------------------------------------------------------
	/*42*/pars[p(ACVOLT)] = new ParUint8(230, "acvolt", "acvolt", ACVOLTOFST, 'p','i', new ACVOLT_Evnt());
	/*42*/pars[p(CALPWR)] = new ParFloat(60, "calpwr", "calpwr", CALPWROFST, 'p', 'i', new CALPWR_Evnt());//Must be after ACVOLT!
#else
	/*4*/pars[DOPWRCAL] = new ParUint8(0, "dopwrcal", "dopwrcal", 2, 'n','n');
	/*4*/pars[INSTACV] = new ParUint8(0, "iacvolt", "iacvolt", 2,'n','n');
	/*4*/pars[INSTPWR] = new ParUint8(0, "ipwr", "ipwr", 2,'n','n');
	/*42*/pars[p(ACVOLT)] = new ParUint8(230, "acvolt", "acvolt", ACVOLTOFST, 'n','n');
	/*42*/pars[p(CALPWR)] = new ParFloat(60, "calpwr", "calpwr", CALPWROFST, 'n', 'n');
#endif
#if (RS485) 
	/*43*/pars[p(MBUSVEL)] = new ParLong(MODBUSBAUDRATE, "mbusvel", "mbusvel", MBUSVELOFST, 'p', 'n', new MBUSVEL_Evnt());
	/*44*/pars[p(MBUSID)] = new ParUint8(MODBUSID, "mbusid", "mbusid", MBUSIDOFST, 'p', 'i', new MBUSID_Evnt());
#endif	
	/*5*/pars[p(LOCALIP)] = new ParStr32("ip", "localip","ip");
	/*7*/pars[p(UTCSDT)] = new ParUint8(DAYSAVE, "utcsdt", "utcsdt", NTPSDTOFST, 'p', 'n', new UTCSDT_Evnt());
	/*8*/pars[p(UTCZONE)] = new ParInt(1, "utczone", "utczone", NTPZONEOFST, 'p', 'i', new UTCZONE_Evnt());
	/*9*/pars[p(THALT1)] = new ParLong(thalt1,"thalt1", "thalt1", THALT1OFST, 'p','i', new THALTX_Evnt());
	/*10*/pars[p(THALT2)] = new ParLong(thalt2, "thalt2", "thalt2", THALT2OFST, 'p','i', new THALTX_Evnt());
	/*11*/pars[p(THALT3)] = new ParLong(thalt3,"thalt3", "thalt3", THALT3OFST, 'p','i', new THALTX_Evnt());
	/*12*/pars[p(THALT4)] = new ParLong(thalt4, "thalt4", "thalt4", THALT4OFST, 'p','i', new THALTX_Evnt());
#if (AUTOCAL) 
	/*13*/pars[p(STDEL1)] = new ParLong(TENDCHECK*1000, "stdel1", "stdel1", STDEL1OFST, 'p','i', new STDELX_Evnt());
	/*14*/pars[p(STDEL2)] = new ParLong(TENDCHECK*1000, "stdel2", "stdel2", STDEL2OFST, 'p','i', new STDELX_Evnt());
#else	
	/*13*/pars[p(STDEL1)] = new ParLong(400, "stdel1", "stdel1", STDEL1OFST, 'p','i', new STDELX_Evnt());
	/*14*/pars[p(STDEL2)] = new ParLong(400, "stdel2", "stdel2", STDEL2OFST, 'p','i', new STDELX_Evnt());
#endif	
	/*5*/pars[p(SWROLL1)] = new ParUint8(ROLLMODE1, "swroll1", "swroll1", SWROLL1OFST, 'p', 'i', new SWROLL1_Evnt());
	/*6*/pars[p(SWROLL2)] = new ParUint8(ROLLMODE2, "swroll2", "swroll2", SWROLL2OFST, 'p', 'i', new SWROLL2_Evnt());
#if (ROLLER)
	/*15*/pars[p(VALWEIGHT)] = new ParFloat(0.5, "valweight", "valweight", VALWEIGHTOFST, 'p','i', new VALWEIGHT_Evnt());
	/*16*/pars[p(TLENGTH)] = new ParFloat(53, "tlength","tlength", TLENGTHOFST, 'p','i', new TLENGTH_Evnt());
	/*17*/pars[p(BARRELRAD)] = new ParFloat(3.37, "barrelrad", "barrelrad", BARRELRADOFST, 'p','i', new BARRELRAD_Evnt());
	/*18*/pars[p(THICKNESS)] = new ParFloat(1.5, "thickness", "thickness", THICKNESSOFST, 'p','i', new THICKNESS_Evnt());
	/*20*/pars[p(SLATSRATIO)] = new ParFloat(0.8, "slatsratio", "slatsratio", SLATSRATIOFST, 'p','i', new SLATSRATIO_Evnt());
#endif	
    /*19*/pars[p(UTCADJ)] = new ParInt(0, "utcadj", "utcadj", NTPADJUSTOFST, 'p','i', new UTCADJ_Evnt());
	/*21*/pars[p(UTCSYNC)] = new ParInt(50, "utcsync", "utcsync", NTPSYNCINTOFST, 'p','i', new UTCSYNC_Evnt());
	DEBUG2_PRINT(F("Pippo: "));
	/*22*/pars[p(MQTTID)] = new ParStr32(String(WiFi.macAddress()).c_str(), "mqttid", "devid", MQTTIDOFST, 'p','i', new MQTTID_Evnt());
	DEBUG2_PRINT(F("Pippo2: "));
	/*23*/pars[p(MQTTOUTTOPIC)] = new ParStr32(OUTTOPIC, "mqttouttopic", "mqttouttopic", OUTTOPICOFST, 'p','i');
	/*23*/pars[p(MQTTLOG)] = new ParStr32(LOGPATH, "mqttlog", "mqttlog", MQTTLOGOFST, 'p','i');
	///*23bis*/pars[p(MQTTLOG2)] = new ParStr32(LOGPATH2, "mqttlog2", "mqttlog2", MQTTLOGOFST2, 'p','i');
	/*24*/pars[p(MQTTINTOPIC)] = new ParStr32(INTOPIC, "mqttintopic", "mqttintopic", INTOPICOFST, 'p','i', new MQTTINTOPIC_Evnt());
	/*25*/pars[p(CLNTSSID1)] = new ParStr32(SSID1, "clntssid1", "clntssid1", WIFICLIENTSSIDOFST1, 'p','i', new WIFICHANGED_Evnt());
	/*26*/pars[p(CLNTPSW1)] = new ParStr32(PSW1, "clntpsw1", "clntpsw1", WIFICLIENTPSWOFST1, 'p','i', new WIFICHANGED_Evnt());
	/*27*/pars[p(CLNTSSID2)] = new ParStr32(SSID2, "clntssid2", "clntpsw1", WIFICLIENTSSIDOFST2, 'p','i', new WIFICHANGED_Evnt());
	/*28*/pars[p(CLNTPSW2)] = new ParStr32(PSW2, "clntpsw2", "clntpsw2", WIFICLIENTPSWOFST2, 'p','i', new WIFICHANGED_Evnt());
	/*29*/pars[p(APPSSID)] = new ParStr32(SSIDAP, "appssid", "appssid", WIFIAPSSIDOFST, 'p','i');
	/*30*/pars[p(APPPSW)] = new ParStr32(PSWAP, "apppsw", "apppsw", WIFIAPPPSWOFST, 'p','i');
	/*31*/pars[p(WEBUSR)] = new ParStr32(WBUSR, "webusr", "webusr", WEBUSROFST, 'p','i');
	/*32*/pars[p(WEBPSW)] = new ParStr32(WBPSW, "webpsw", "webpsw", WEBPSWOFST, 'p','i');
	/*33*/pars[p(MQTTUSR)] = new ParStr32(MQUSR, "mqttusr", "mqttusr", MQTTUSROFST, 'p','i');//uno dei due!
	/*34*/pars[p(MQTTPSW)] = new ParStr32(MQPSW, "mqttpsw", "mqttpsw", MQTTPSWOFST, 'p','i', new MQTTCONNCHANGED_Evnt());
	/*35*/pars[p(MQTTADDR)] = new ParStr64(MQTTSRV, "mqttaddr", "mqttaddr", MQTTADDROFST, 'p','i', new MQTTADDR_Evnt(),true);//va prima!
	/*36*/pars[p(MQTTPORT)] = new ParLong(MQTTPRT, "mqttport", "mqttport", MQTTPORTOFST, 'p','i', pars[p(MQTTADDR)]->e,true);
	/*37*/pars[p(WSPORT)] = new ParStr32(WSPRT, "wsport", "wsport", WSPORTOFST, 'p','i');
	/*38*/pars[p(MQTTPROTO)] = new ParStr32(MQTTPT, "mqttproto", "mqttproto", MQTTPROTOFST, 'p','i');
	/*39*/pars[p(NTPADDR1)] = new ParStr64(NTP1, "ntpaddr1", "ntpaddr1", NTP1ADDROFST, 'p','i', new NTPADDR1_Evnt());
	/*40*/pars[p(NTPADDR2)] = new ParStr64(NTP2, "ntpaddr2", "ntpaddr2", NTP2ADDROFST, 'p','i', new NTPADDR2_Evnt());
#if (AUTOCAL_HLW8012) 
	/*41*/pars[p(VACMULT)] = new ParFloat(hlw8012.getVoltageMultiplier(), "vacmult", "vacmult", VACMULTOFST, 'p','n', new VACMULT_Evnt());
	/*41*/pars[p(PWRMULT)] = new ParFloat(hlw8012.getPowerMultiplier(), "pwrmult", "pwrmult", PWRMULTOFST, 'p','i', new PWRMULT_Evnt());
	/*41*/pars[p(CURRMULT)] = new ParFloat(hlw8012.getCurrentMultiplier(), "currmult", "currmult", CURRMULTOFST, 'p','n', new CURRMULT_Evnt());
#else	
	/*41*/pars[p(PWRMULT)] = new ParFloat(1, "pwrmult", "pwrmult", PWRMULTOFST, 'n','n');
	/*41*/pars[p(VACMULT)] = new ParFloat(1, "vacmult", "vacmult", VACMULTOFST, 'n','n');
	/*41*/pars[p(CURRMULT)] = new ParFloat(1, "currmult", "currmult", CURRMULTOFST, 'n','n');
#endif
	/*43*/pars[p(ONCOND1)] = new ParVarStr("-1", "oncond1", "oncond1", 2, 'p','n', new ONCOND1_Evnt());
	/*44*/pars[p(ONCOND2)] = new ParVarStr("-1", "oncond2","oncond2", 2, 'p','n', new ONCOND2_Evnt());
	/*45*/pars[p(ONCOND3)] = new ParVarStr("-1", "oncond3","oncond3", 2, 'p','n', new ONCOND3_Evnt());
	/*46*/pars[p(ONCOND4)] = new ParVarStr("-1", "oncond4","oncond4", 2, 'p','n', new ONCOND4_Evnt());
	///*47*/pars[p(ONCOND5)] = new ParVarStr("(td1=4000)|(ma1=0)|(ma4=2)|(tsmpl4=4)|(oe1=1)", "oncond5","oncond5", 0, 'p','n', new ONCOND5_Evnt());
	/*47*/pars[p(ONCOND5)] = new ParVarStr("-1", "oncond5","oncond5", 0, 'p','n', new ONCOND5_Evnt());
	/*48*/pars[p(ACTIONEVAL)] = new ParVarStr("-1","onaction","onaction", 2, 'p','n', new ACTIONEVAL_Evnt());
	///*5*/pars[p(WIFICHANGED)] = new ParUint8(0, "WIFICHANGED","", new WIFICHANGED_Evnt());
	///*5*/pars[p(CONFLOADED)] = new ParUint8(0, "CONFLOADED","");
	///*5*/pars[p(MQTTADDRMODFIED)] = new ParUint8(0, "MQTTADDRMODFIED","");
	///*5*/pars[p(TOPICCHANGED)] = new ParUint8(0, "TOPICCHANGED","", new TOPICCHANGED_Evnt());
	///*5*/pars[p(MQTTCONNCHANGED)] = new ParUint8(0, "MQTTCONNCHANGED","");
	///*5*/pars[p(TIMINGCHANGED)] = new ParUint8(0, "TIMINGCHANGED","");
	/*5*/pars[p(SWACTION1)] = new ParUint8(0, "SWACTION1","");
	/*5*/pars[p(SWACTION2)] = new ParUint8(0, "SWACTION2","");
	/*5*/pars[p(SWACTION3)] = new ParUint8(0, "SWACTION3","");
	/*5*/pars[p(SWACTION4)] = new ParUint8(0, "SWACTION4","");
	/*5*/pars[p(UTCVAL)] = new ParLong(0, "UTCVAL","utcval");
	/*5*/pars[p(LOGSLCT)] = new ParUint8(LOGSEL, "logslct","logslct", LOGSLCTOFST, 'n', 'n', new LOGSLCT_Evnt());
	///*5*/pars[p(DEVICEID)] = new ParStr32(DEVID, "devid","deviceid", DEVICEIDOFST, 'p', 'i', new DEVICEID_Evnt());
	///*5*/pars[p(SWSPLDPWR1)] = new ParUint8(0, "SWSPLDPWR1","", SWSPLDPWR1OFST1, 'n', 'n', new SWSPLDPWR1_Evnt());
	///*5*/pars[p(SWSPLDPWR2)] = new ParUint8(0, "SWSPLDPWR1","", SWSPLDPWR1OFST2, 'n', 'n', new SWSPLDPWR2_Evnt());
	///*5*/pars[p(SWSPLDPWR3)] = new ParUint8(0, "SWSPLDPWR1","", SWSPLDPWR1OFST3, 'n', 'n', new SWSPLDPWR3_Evnt());
	///*5*/pars[p(SWSPLDPWR4)] = new ParUint8(0, "SWSPLDPWR1","", SWSPLDPWR1OFST4, 'n', 'n', new SWSPLDPWR4_Evnt());
	//printparams();
}

inline bool gatedfn(float val, uint8_t n, float rnd, bool updtOnChange = true){
	//n: numero di porte
	/*DEBUG1_PRINT("val: ");
	DEBUG1_PRINT(val);
	DEBUG1_PRINT(", asyncBuf: ");
	DEBUG1_PRINT(asyncBuf[n]);
	DEBUG1_PRINT(", rnd: ");
	DEBUG1_PRINT(rnd);
	DEBUG1_PRINT(", changed: ");
	DEBUG1_PRINTLN(changed[n]);*/
	changed[n] = (val < asyncBuf[n] - rnd || val > asyncBuf[n] + rnd);
	if(updtOnChange){
		(changed[n]) && (asyncBuf[n] = val);
	}else{
		asyncBuf[n] = val;  
	}
	           
	return changed[n];
}

#if (AUTOCAL_ACS712) 	
float getAmpRMS(float ACSVolt){
	ACSVolt = (double) (ACSVolt * 5.0) / 1024.0;
	VRMS = ACSVolt * 0.707;
	AmpsRMS = (double) (VRMS * 1000) / mVperAmp;
	if((AmpsRMS > -0.015) && (AmpsRMS < 0.008)){ 
		AmpsRMS = 0.0;
	}
	return AmpsRMS*vcosfi;
}
#endif

void setSWMode(uint8_t mode, uint8_t n){
	roll[n] = mode;
	isrun[n] = false;
	DEBUG2_PRINT("setSWMode");
	DEBUG2_PRINTLN(n);
	DEBUG2_PRINT(": ");
	DEBUG2_PRINTLN(mode);
	//setSWModeTap(mode,0);	
	//readModeAndPub(n);
}

uint8_t inline getSWMode(uint8_t n){
	return roll[n];
}

double getTemperature(){
	double temp = -127;
#if(LARGEFW)

	temp = sampletemp(3);

	if(firstTemp > 0){
		firstTemp--;
		st = temp;
		stv = 85.0;
		DEBUG2_PRINT("firstTemp: ");
		DEBUG2_PRINTLN(st);
	}else{
		if((temp >= (double) st - TSIGMA*stv && temp <= (double) st + TSIGMA*stv) || tempCount == 0){
			if(tempCount == 0){
				tempCount = TEMPCOUNT;
				DEBUG2_PRINT("Recupero scartati... ");
				st = st2;
				stv = stv2;
			}
			DEBUG2_PRINT("Sommo corrente... ");
			st = (double) st*(1-EMAA) + temp*EMAA;
			if(temp >= st){
				stv = (double) stv*(1-EMAB) + (temp - st)*EMAB;
			}else{
				stv = (double) stv*(1-EMAB) + (st - temp)*EMAB;
			}
			st2 = st;
			stv2 = stv;
		}else{
			DEBUG2_PRINT("Fuori, scarto corrente...");
			//after TEMPCOUNT times are to be considered valid values!
			tempCount--;
			st2 = (double) st2*(1-EMAA) + temp*EMAA;
			if(temp >= st2){
				stv2 = (double) stv2*(1-EMAB) + (temp - st)*EMAB;
			}else{
				stv2 = (double) stv2*(1-EMAB) + (st - temp)*EMAB;
			}
		}
	}
#endif
	DEBUG2_PRINT(" instTemp: ");
	DEBUG2_PRINT(temp);
	DEBUG2_PRINT(", tmedia: ");
	DEBUG2_PRINT(st);
	DEBUG2_PRINT(", finestra: ");
	DEBUG2_PRINT(TSIGMA*stv);
	DEBUG2_PRINT(", scarto: ");
	if(temp >= st - TSIGMA*stv)
		DEBUG2_PRINTLN(temp - st);
	else
		DEBUG2_PRINTLN(st - temp);
	
	return st;
}

//parser actions callBack (assignements)
//configurazioni provenienti da eventi locali
double actions(char *key, double val)
{	
	uint8_t n=-1;
	if(key[0]=='t'){
		if(roll[0] == false){
			if(strcmp(key,"tsmpl1")==0){
				startCnt(0,(unsigned long)val,SMPLCNT1);
			}else if(strcmp(key,"tsmpl2")==0){
				startCnt(0,(unsigned long)val,SMPLCNT2);
			}
			//haldelay 1 e 2
			if(key[1]=='d'){
				n = key[2] - 49;
				DEBUG2_PRINT("d");
				DEBUG2_PRINTLN(n);
				if(n>=0){
					//writeHaltDelay(val,n);
					updtConf(p(THALT1+n), String(val));
					setHaltDelay(val,n);
					readActModeAndPub(n);
				}
			}
		}
		
		if(roll[1] == false){
			if(strcmp(key,"tsmpl3")==0){
				startCnt(0,(unsigned long)val,SMPLCNT3);
			}else if(strcmp(key,"tsmpl4")==0){
				startCnt(0,(unsigned long)val,SMPLCNT4);
			}
			//haldelay 3 e 4
			if(key[1]=='d'){
				n = key[2] - 49;
				DEBUG2_PRINT("d");
				DEBUG2_PRINTLN(n);
				if(n>=0){
					//writeHaltDelay(val,n);
					updtConf(p(THALT1+n), String(val));
					setHaltDelay(val,n);
					readActModeAndPub(n);
				}
			}
		}
	}else if(key[0]=='m'){
		uint8_t act=0;
		if(key[1]=='a'){
			if(roll[0] == false){
				if(key[2]=='1'){
					n = 0;
				}else if(key[2]=='2'){
					n = 1;
				}
				act=val;
			}
			if(roll[1] == false){
				if(key[2]=='3'){
					n = 2;
				}else if(key[2]=='4'){
					n = 3;
				}
				act=val;
			}
			if(n>=0){
				//writeOnOffAction(act,n);
				setSWAction(act,n);
				readActModeAndPub(n);
			}
		}else if(strcmp(key,"mode1")==0){
			if(val==1){
				setSWMode(1,0);
				//writeSWMode(1,0);
				updtConf(p(SWROLL1), String(1));
			}else if(val==0){
				setSWMode(0,0);
				//writeSWMode(0,0);
				updtConf(p(SWROLL1), String(0));
			}
		}else if(strcmp(key,"mode2")==0){
			if(val==1){
				setSWMode(1,1);
				//writeSWMode(1,1);
				updtConf(p(SWROLL2), String(0));
			}else if(val==0){
				setSWMode(0,1);
				//writeSWMode(0,1);
				updtConf(p(SWROLL2), String(1));
			}
		}
	}else if(key[0]=='u'){
		uint8_t act=0;
		if(key[1]=='p'){
			if(roll[0] == true){
#if (ROLLER)
				DEBUG2_PRINT("up1: ");
				DEBUG2_PRINTLN(val);
				if(key[2] == '1' && val > 0 && !isOnTarget(val, 0) && !isrun[0] && !isCalibr()){
					static_cast<ParUint8*>(pars[MQTTUP1])->load((int)val);				
					static_cast<ParUint8*>(pars[MQTTUP1])->doaction(0);
				}
#endif
			}
			if(roll[1] == true){
#if (ROLLER)
				if(key[2] == '2' && val > 0 && !isOnTarget(val, 1) && !isrun[1] && !isCalibr()){
					static_cast<ParUint8*>(pars[MQTTUP2])->load((int)val);		
					static_cast<ParUint8*>(pars[MQTTUP2])->doaction(0);
				}
#endif
			}
		}
		act=val;
	}else if(key[0]=='a'){
		uint8_t act=0;
		if(roll[0] == false){
			bool pub = false;
			if(strcmp(key,"actlgcd1")==0){
				pub = setDiffActionLogic(val,0);
				readStatesAndPub(0);
			}else if(strcmp(key,"actlgcd2")==0){
				pub = setDiffActionLogic(val,1);
				readStatesAndPub(1);
			}else if(strcmp(key,"actlgc1")==0){
				pub = setActionLogic(val,0);
				readStatesAndPub(0);
			}else if(strcmp(key,"actlgc2")==0){
				pub = setActionLogic(val,1);
				readStatesAndPub(1);
			}
			scriviOutDaStato(0);
		}
		if(roll[1] == false){
			bool pub = false;
			if(strcmp(key,"actlgcd3")==0){
				pub = setDiffActionLogic(val,2);
				readStatesAndPub(2);
			}else if(strcmp(key,"actlgcd4")==0){
				pub = setDiffActionLogic(val,3);
				readStatesAndPub(3);
			}else if(strcmp(key,"actlgc3")==0){
				pub = setActionLogic(val,2);
				readStatesAndPub(2);
			}else if(strcmp(key,"actlgc4")==0){
				pub = setActionLogic(val,3);
				readStatesAndPub(3);
			}
			scriviOutDaStato(1);
		}
		act=val;
	}else if(key[0]=='d'){		
		uint8_t act=0;
		if(key[1]=='w'){
			if(roll[0] == true){
#if (ROLLER)
				DEBUG2_PRINT("dw1: ");
				DEBUG2_PRINTLN(val);
				if(key[2] == '1' && val > 0 && !isOnTarget(val, 0) && !isrun[0] && !isCalibr()){
					DEBUG2_PRINT("dw1: ");
					DEBUG2_PRINTLN(val);
					static_cast<ParUint8*>(pars[MQTTDOWN1])->load((int)val);			
					static_cast<ParUint8*>(pars[MQTTDOWN1])->doaction(0);
				}
#endif
			}
			if(roll[1] == true){
#if (ROLLER)
				if(key[2] == '2' && val > 0 && !isOnTarget(val, 1) && !isrun[1] && !isCalibr()){
					static_cast<ParUint8*>(pars[MQTTDOWN2])->load((int)val);		
					static_cast<ParUint8*>(pars[MQTTDOWN2])->doaction(0);
				}
#endif
			}
		}
		act=val;
	}else if(key[0]=='r'){
		unsigned long cnt = val;
		if(strcmp(key,"r1")==0){
			setCntValue(cnt,CNTIME1);
		}else if(strcmp(key,"r2")==0){
			setCntValue(cnt,CNTIME2);
		}else if(strcmp(key,"r3")==0){
			setCntValue(cnt,CNTIME3);
		}else if(strcmp(key,"r4")==0){
			setCntValue(cnt,CNTIME4);
		}
		////return val;
	}else if(key[0]=='o'){
		bool oe = val;
		DEBUG2_PRINT("oeeeeee: ");
				DEBUG2_PRINTLN(oe);
		if(strcmp(key,"oe1")==0){
			setOE(oe,0);
		}else if(strcmp(key,"oe2")==0){
			setOE(oe,1);
		}else if(strcmp(key,"oe3")==0){
			setOE(oe,2);
		}else if(strcmp(key,"oe4")==0){
			setOE(oe,3);
		}
		//return val;
	}else if(key[0]=='s'){
		if(strcmp(key,"sdt")==0){
			updtConf(p(UTCSDT), String(val));
			setSDT((uint8_t) val);
		}
	}
	
	return true;
}

//parser function calls
double variables(char *key){
	double result;
	
	if(key[0]=='t'){
		if(strcmp(key,"tsec")==0){ //secondi 
			result = second();
		}else if(strcmp(key,"tmin")==0){//minuti 
			result = minute();
		}else if(strcmp(key,"thour")==0){//ora
			result = hour();
		}else if(key[1]=='e' && key[2]==':'){// 2019:07:30/03:57:30 o 2019-10-08T09:28:40
			key += strlen("te:");
			result = makeTime(fromStrToTimeEl(key));
			//DEBUG1_PRINT("te: ");
			//DEBUG1_PRINTLN(result);
		}else if(strcmp(key,"tenow")==0){
			result = (unsigned long) getUNIXTime();
			//DEBUG1_PRINT("tenow: ");
			//DEBUG1_PRINTLN(result);
			
		}else if(strlen(strstr(key,"tdst:M")) == strlen(key)){
			key += strlen("tdst:M");//M4.5.0/02:00:00
			result = makeTime(DSTToTimeEl(key));
		}
	}else if(key[0]=='d'){	
		if(strcmp(key,"dyear")==0){
			result =  year();
		}else if(strcmp(key,"dmonth")==0){
			result =  month();
		}else if(strcmp(key,"dwkday")==0){
			result =  weekday();
		}else if(strcmp(key,"day")==0){
			result = day();
		}
	}else /*if(key[0]=='c'){
		if(strcmp(key,"c1")==0){
			result = getCntValue(CNTIME1);
		}else if(strcmp(key,"c2")==0){
			result = getCntValue(CNTIME2);
		}else if(strcmp(key,"c3")==0){
			result = getCntValue(CNTIME3);
		}else if(strcmp(key,"c4")==0){
			result = getCntValue(CNTIME4);
		}else{
			result = 0;
		}
	}else */if(key[0]=='o'){
		if(strcmp(key,"o1")==0){
			result = out[0];
		}else if(strcmp(key,"o2")==0){
			result = out[1];
		}else if(strcmp(key,"o3")==0){
			result = out[2];
		}else if(strcmp(key,"o4")==0){
			result = out[3];
		}else{
			result = 0;
		}
	}else if(key[0]=='s'){
		byte n = key[1] - 49;
		pingCnt[n] = (pingCnt[n] + 1) % 3600;
		if(!(pingCnt[n] % pingPer2[n])){
			if(key[2]==':'){
				key += strlen("s1:");
				DEBUG2_PRINT("\nkey: ");
				DEBUG2_PRINT(key);
				
				char* k;
				bool esci = false;
				int i;
				for(i=0; key[i] != ':' && key[i] != '\0'; i++);
				if(key[i] == ':'){
					key[i] = '\0';
					k = (key+i+1);
					for(i=0; k[i] != ':' && key[i] != '\0'; i++);
					if(k[i] == ':'){
						k[i] = '\0';
						nping[n] = atoi(k);
						k = (k+i+1);
						for(i=0; k[i] != ':' && key[i] != '\0'; i++);
						if(k[i] == ':')
							k[i] = '\0';
						pingPer[n] = atoi(k);
						if(pingPer[n] < 0)
							pingPer[n] = 0;
						
					}
				}
				
				DEBUG2_PRINT(", n: ");
				DEBUG2_PRINT(n); 
				DEBUG2_PRINT(", key: ");
				DEBUG2_PRINT(key); 
				DEBUG2_PRINT(", nping[n]: ");
				DEBUG2_PRINT(nping[n]); 
				DEBUG2_PRINT(", pingPer[n]: ");
				DEBUG2_PRINT(pingPer[n]); 
				

				//(tokens[1] != NULL) && (nping[n] = atoi(tokens[1]));
				//(tokens[2] != NULL) && (pingPer[n] = atoi(tokens[2]));
				
				DEBUG2_PRINT(", ip: ");
				//DEBUG2_PRINT(key);
				IPAddress ip;
				if (!WiFi.hostByName(key, ip))
					ip.fromString(key);
				DEBUG2_PRINT(ip.toString());
				pinger[n].Ping(ip);
				
				if(lastPing[n] >= nping[n]){
					pingRes[n] = 1;
				}else if(lastPing[n] == 0){
					pingRes[n] = 0;
				}
			}
		}
		result = pingRes[n];
		DEBUG2_PRINT(", result: ");
		DEBUG2_PRINTLN(result);
	}else if(key[0] =='p'){
		if(strcmp(key,"percpos2")==0){
#if (ROLLER)
			result = percfdbck(1);
#endif
		}else if(strcmp(key,"pwrall")==0){
			result = overallSwPower;
		}else
#if (AUTOCAL_ACS712) 
		if(strcmp(key,"ptapavg1")==0){
			result = getAmpRMS(getAVG(0)/2);
		}else if(strcmp(key,"ptapavg2")==0){
			result = getAmpRMS(getAVG(1)/2);
		}else
#endif 		
		if(strcmp(key,"percpos1")==0){
#if (ROLLER)
			result = percfdbck(0);
#endif
			DEBUG2_PRINT(" percpos1: ");
			DEBUG2_PRINTLN(result);
		}
	}else if(key[0]=='i'){
		char *app;
		if(strcmp(key,"isPM")==0){
			result = isPM();
		}else if(strcmp(key,"isAM")==0){
			result = isAM();
		}else if(strcmp(key,"isMQTT")==0){
			result = mqttConnected;
		}
	}else if(strcmp(key,"wifi")==0){
		result = wifiConn;
	}/*else if(strcmp(key,"temp")==0){
		result = getTemperature();
	}*/
	
	return result;
}
/*
void printMcpRealOut(){
	char s[47];
	uint16_t m;

#if (MCP2317)	
	m = mcp.readGPIOAB();
	sprintf(s, "Lettura uscite MCP: - %d%d%d%d ", mcp.digitalRead(OUT1EU), mcp.digitalRead(OUT1DD), mcp.digitalRead(OUT2EU), mcp.digitalRead(OUT2DD));
	DEBUG1_PRINT(s);
	//sprintf(s," readGPIOAB: "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN" ", BYTE_TO_BINARY(m>>8), BYTE_TO_BINARY(m));
	//DEBUG1_PRINT(s);
#else
	m = (uint16_t)GPI;
	sprintf(s,"GPIO: "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN" ", BYTE_TO_BINARY(m>>8), BYTE_TO_BINARY(m));
	DEBUG1_PRINT(s);
#endif
	sprintf(s,"%d%d%d%d\n", out[0], out[1], out[2], out[3]);
	DEBUG1_PRINTLN(s);
}
*/
void scriviOutDaStato(byte n){
	//n: 0, 1 
	//0: switch 1
	//1: switch 2
	DEBUG1_PRINT("Evento scriviOutDaStato n: ");
	DEBUG1_PRINT(n);
	DEBUG1_PRINT(" nn: ");
	DEBUG1_PRINT(n*BTNDIM);
	DEBUG1_PRINT(" out 1: ");
	DEBUG1_PRINT(out[n*BTNDIM]);
	DEBUG1_PRINT(" out 2: ");
	DEBUG1_PRINT(out[1+n*BTNDIM]);
#if (MCP2317) 
	//uint8_t out2[4];
	//out2[0]=out2[1]=out2[2]=out2[3]=HIGH;
	mcp.writeOuts(out,n);//n è ininfluente! Scrive un byte alla volta...
#if (RS485)
	au16data[RDGPB] = mcp.readOutputs();
#endif
	//mcp.digitalWrite(OUT1EU + n*BTNDIM,out[n*BTNDIM]);	
	//mcp.digitalWrite(OUT1DD + n*BTNDIM,out[1 + n*BTNDIM]);	
#else										
	digitalWrite(outPorts[n*BTNDIM],out[n*BTNDIM]);	//scrive un bit alla volta
	digitalWrite(outPorts[1+n*BTNDIM],out[1 + n*BTNDIM]);	//scrive un bit alla volta			
#endif
#if (ROLLER)
	isrun[0] = (out[0] || out[1]) && roll[0] > 0;						
	isrun[1] = (out[2] || out[3]) && roll[1] > 0;		
	mov = isrun[0] || isrun[1];
#endif
}

void setup_AP(bool apmode) {
  //ESP.wdtFeed();
  DEBUG1_PRINTLN(F("Configuring access point..."));
  // You can remove the password parameter if you want the AP to be open. 
  //WiFi.softAP(APSsid.c_str(), APPsw.c_str());
  
  if(apmode){
	  DEBUG1_PRINT(F("Setting soft-AP configuration ... "));
	  DEBUG1_PRINTLN(WiFi.softAPConfig(myip, mygateway, mysubnet) ? F("Ready") : F("Failed!"));
	  //noInterrupts ();
	  delay(100);
	  WiFi.softAP((const char*) static_cast<ParStr32*>(pars[p(APPSSID)])->val);
	  //WiFi.softAP("cacca9");
	  //interrupts();
	  //DEBUG_PRINT(F("Setting soft-AP ... "));
	  //DEBUG2_PRINTLN(WiFi.softAP((confcmd[APPSSID]).c_str()) ? F("Ready") : F("Failed!"));
	  //delay(500); // Without delay I've seen the IP address blank
	  delay(1000);
	  pars[p(LOCALIP)]->load((char*) (WiFi.softAPIP().toString()).c_str());
	  DEBUG1_PRINT(F("Soft-AP IP address = "));
	  DEBUG1_PRINTLN(((ParStr32*)pars[p(LOCALIP)])->val);
	  //wifi_softap_dhcps_stop();
  }else{
	  //noInterrupts ();
	  WiFi.softAP((const char*) static_cast<ParStr32*>(pars[p(APPSSID)])->val); 
	  //WiFi.softAP(ssid, password);
	  //interrupts();
	  //DEBUG_PRINT(F("Setting soft-AP ... "));
	  //DEBUG2_PRINTLN(WiFi.softAP((confcmd[APPSSID]).c_str()) ? F("Ready") : F("Failed!"));
	  delay(1000);
	  pars[p(LOCALIP)]->load(WiFi.softAPIP().toString().c_str());
	  DEBUG1_PRINT(F("Soft-AP IP address = "));
	  DEBUG1_PRINTLN(pars[p(LOCALIP)]->getStrVal());
	  //wifi_softap_dhcps_stop();
  }
}

void scan_wifi() {
int numberOfNetworks = WiFi.scanNetworks();
 
  for(int i =0; i<numberOfNetworks; i++){
 
     DEBUG2_PRINT("\nNetwork name: ");
     DEBUG2_PRINT(WiFi.SSID(i));
     DEBUG2_PRINT("\nSignal strength: ");
     DEBUG2_PRINT(WiFi.RSSI(i));
     DEBUG2_PRINT("\n-----------------------");
 
  }
}

//wifi setup function
void setup_wifi(int wifindx) {
	//ESP.wdtFeed();
	wifindx = wifindx*2;  //client1 e client2 hanno indici contigui nell'array confcmd
	// We start by connecting to a WiFi network
	//DEBUG2_PRINT(F("Connecting to "));
	DEBUG1_PRINTLN(pars[p(CLNTSSID1+wifindx)]->getStrVal());
    DEBUG1_PRINTLN(F(" as wifi client..."));
	if (wifiConn) {
		//importante! Altrimenti tentativi potrebbero collidere con una connessione appena instaurata
		DEBUG1_PRINTLN(F("Already connected. Bailing out."));
	}else{
		//WiFi.setAutoConnect(true);		//inibisce la connessione automatica al boot
		//WiFi.setAutoReconnect(true);	//inibisce la riconnessione automatica dopo una disconnesione accidentale
		//wifi_set_sleep_type(NONE_SLEEP_T);
		//WiFi.persistent(false);			//inibisce la memorizzazione dei parametri della connessione
		//WiFi.mode(WIFI_OFF);    //otherwise the module will not reconnect
		//WiFi.mode(WIFI_STA);	
		//wifiState = WIFISTA;
		//check if we have ssid and pass and force those, if not, try with last saved values
		if (pars[p(CLNTSSID1+wifindx)]->getStrVal() != "") {
			//noInterrupts();
			DEBUG1_PRINT(F("Begin status: "));
			DEBUG1_PRINTLN(WiFi.begin((const char*) static_cast<ParStr32*>(pars[p(CLNTSSID1+wifindx)])->val, (const char*) static_cast<ParStr32*>(pars[p(CLNTPSW1+wifindx)])->val));
			delay(100);
			//interrupts();
		} else {
			if (WiFi.SSID()) {
				DEBUG1_PRINTLN(F("Using last saved values, should be faster"));
				//trying to fix connection in progress hanging
				ETS_UART_INTR_DISABLE();
				wifi_station_disconnect();
				ETS_UART_INTR_ENABLE();
				WiFi.begin();
			} else {
				DEBUG1_PRINTLN(F("No saved credentials"));
			}
		}
		IPAddress ip = WiFi.localIP();
		ip.toString().toCharArray(IP, 16);
		pars[p(LOCALIP)]->load(IP);
		DEBUG1_PRINT(F("AP client IP address = "));
		DEBUG1_PRINTLN(((ParStr32*)pars[p(LOCALIP)])->val);
	}	

	DEBUG1_PRINTLN (F("\n******************* begin ***********"));
#if (RS485)
	WiFi.printDiag(Serial1);
#else
	WiFi.printDiag(Serial);
#endif  
	DEBUG1_PRINTLN (F("\n******************* end ***********"));

	//WiFi.enableAP(true);
  //}
}
/*
#if (LARGEFW)
void setup_mDNS() {
	if (MDNS.begin((const char*) static_cast<ParStr32*>(pars[p(LOCALIP)])->val)) {              // Start the mDNS responder for esp8266.local
		DEBUG1_PRINTLN(F("mDNS responder started"));
	} else {
		DEBUG1_PRINTLN(F("Error setting up MDNS responder!"));
	}
}
#endif
*/
void startWebSocket() { // Start a WebSocket server
  webSocket.begin();                          // start the websocket server
  webSocket.onEvent(webSocketEvent);          // if there's an incomming websocket message, go to function 'webSocketEvent'
  DEBUG1_PRINTLN("WebSocket server started.");
}

void restartWebSocket() { 
  webSocket.close();
  startWebSocket();
}

void mqttReconnect() {
	// Loop until we're mqttReconnecte
	if(mqttClient!=NULL){
		DEBUG1_PRINTLN(F("Distruggo l'oggetto MQTT client."));
		DEBUG1_PRINTLN(F("Mi disconetto dal server MQTT"));
		//if(mqttClient->isConnected()){
		if(mqttConnected){
			mqttClient->disconnect();
		}
		delay(100);
		//ESP.wdtFeed();
		//mqttClient->~MQTT();
		//noInterrupts ();
		while(mqttClient->isConnected())
			delay(50);
		DEBUG1_PRINTLN(F("Sono disconesso dal server MQTT"));
		DEBUG1_PRINTLN(F("Chiamo il distruttore dell'oggetto MQTT"));
		//ESP.wdtFeed();
		//ESP.wdtDisable(); 
		//noInterrupts ();
		mqttClient->~MQTT();
		//interrupts ();
		//ESP.wdtEnable(0); 
		//ESP.wdtFeed();
		//interrupts ();
		delay(50);
		//DEBUG2_PRINTLN(F("Cancello la vechia istanza dell'oggetto MQTT"));
		DEBUG1_PRINTLN(F("Annullo istanza dell'oggetto MQTT"));
		//ESP.wdtFeed();
		mqttClient = NULL;
		//interrupts ();
	}
	DEBUG_PRINTLN(F("Instanzio un nuovo oggetto MQTT client."));
	/////noInterrupts ();
	mqttClient = new MQTT((const char *)(static_cast<ParStr32*>(pars[p(MQTTID)]))->val, (const char *)(static_cast<ParStr64*>(pars[p(MQTTADDR)]))->val, (unsigned int) (static_cast<ParLong*>(pars[p(MQTTPORT)]))->val);
	mqttc->setMQTTClient(mqttClient);
	/////interrupts ();
    DEBUG1_PRINTLN(F("Registro i callback dell'MQTT."));
	DEBUG1_PRINT(F("Attempting MQTT connection to: "));
	DEBUG1_PRINT(pars[p(MQTTADDR)]->getStrVal());
	DEBUG1_PRINT(F(", with ClientID: "));
	DEBUG1_PRINT(pars[p(MQTTID)]->getStrVal());
	DEBUG1_PRINTLN(F(" ..."));

	delay(100);
	if(mqttClient==NULL){
		DEBUG1_PRINTLN(F("ERROR on mqttReconnect! MQTT client is not allocated."));
	}
	else
	{
		mqttClient->onData(mqttCallback);
		mqttClient->onConnected([]() {
			mqttConnected=true;//bho
			DEBUG1_PRINTLN(F("mqtt: onConnected([]() dice mi sono riconnesso."));
			mqttcnt = 0;
			dscnct = true;
//#if (MCP2317) 
			setColor(2); //GREEN ON
//#endif
			//Altrimenti dice che è connesso ma non comunica
			mqttClient->subscribe(static_cast<ParStr32*>(pars[p(MQTTINTOPIC)])->val); 
			delay(50);
			DEBUG1_PRINT(F("mqtt: Subsribed to: "));
			DEBUG1_PRINTLN(static_cast<ParStr32*>(pars[p(MQTTINTOPIC)])->val);
			mqttClient->publish((const char *)(static_cast<ParStr32*>(pars[p(MQTTOUTTOPIC)]))->val, (const char *)(static_cast<ParStr32*>(pars[p(MQTTID)]))->val, 32);
			pars[p(LOGSLCT)]->doaction(0);	
			readParamAndPub(MQTTDATE,printUNIXTimeMin(gbuf2),true);
			//readStatesAndPub();
			sensorStatePoll();
		});
		
		mqttClient->onDisconnected([]() {
			mqttConnected=false;
			mqttcnt = 0;
			dscnct = false;
			//DEBUG2_PRINTLN("MQTT disconnected.");
			DEBUG1_PRINTLN(F("MQTT: onDisconnected([]() dice mi sono disconnesso."));
		});
		
		mqttClient->setUserPwd((const char*)static_cast<ParStr32*>(pars[p(MQTTUSR)])->val, (const char*) static_cast<ParStr32*>(pars[p(MQTTPSW)])->val);
		//////noInterrupts ();
		if((wifiConn == true)&& WiFi.status()==WL_CONNECTED && WiFi.getMode()==WIFI_STA){
			DEBUG1_PRINTLN(F("MQTT: Eseguo la prima connect."));
			mqttClient->connect();
			delay(20);
		}
		
		//////interrupts ();
		//delay(50);
		//mqttClient->subscribe(static_cast<ParStr32*>(pars[p(MQTTINTOPIC)])->val);
		//mqttClient->publish((const char *)(static_cast<ParStr32*>(pars[p(MQTTOUTTOPIC)]))->val, (const char *)static_cast<ParStr32*>(pars[p(MQTTID)])->val, 32);
	}
}

void mqttCallback(String &topic, String &response) {
	//funzione eseguita dal subscriber all'arrivo di una notifica
	//decodifica la stringa JSON e la trasforma nel nuovo vettore degli stati
	//il vettore corrente degli stati verr� sovrascritto
	//applica la logica ricevuta da remoto sulle uscite locali (led)
    
	int v;
#if defined (_DEBUG) || defined (_DEBUGR)	
	DEBUG1_PRINT(F("Message arrived on topic: ["));
	DEBUG1_PRINT(topic);
	DEBUG1_PRINT(F("], "));
	DEBUG1_PRINTLN(response);
#endif	
	//v = parseJsonFieldToInt(response, mqttJson[0], ncifre);
	//digitalWrite(OUTSLED, v); 
   
	parseJsonFieldArrayToStr(response, pars, ncifre+500, EXTCONFDIM,0,'#',"|");
    //inr: memoria tampone per l'evento asincrono scrittura da remoto
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) { // When a WebSocket message is received
  char s1[300];
  
  switch (type) {
	case  WStype_ERROR:
	
	break;
    case WStype_DISCONNECTED:             // if the websocket is disconnected
		sprintf(s1,"\n[%u] Disconnected!", num);
		DEBUG2_PRINT(s1);
		wsnconn--;
    break;
    case WStype_CONNECTED: {              // if a new websocket connection is established
		wsnconn++;
        IPAddress wip = webSocket.remoteIP(num);
		sprintf(s1,"[%u] Connected from %d.%d.%d.%d url: %s\n", num, wip[0], wip[1], wip[2], wip[3], payload);
		DEBUG2_PRINT(s1);
		//readStatesAndPub();
	}
    break;
    case WStype_TEXT:                     // if new text data is received 
		sprintf(s1,"[%u] get Text: %s\r\n", num, payload);
		DEBUG1_PRINT(s1);
		String str = String((char *)payload);
		String str2 = String("");
		mqttCallback(str2, str);
	break;
  }
}


void inline printOut(){
	DEBUG1_PRINT(F("\nOut: "));
	DEBUG1_PRINT(out[0]);
	DEBUG1_PRINT(F(", "));
	DEBUG1_PRINT(out[1]);
	DEBUG1_PRINT(F(", "));
	DEBUG1_PRINT(out[2]);
	DEBUG1_PRINT(F(", "));
	DEBUG1_PRINTLN(out[3]);
}

//legge il valore dello stato dei toggle e li pubblica sul broker come stringa JSON
void readStatesAndPub(byte nn, bool all, bool shortm){
	//0,1,2,3 buttons
	//vals=digitalRead(OUTSLED); //legge lo stato del led di stato
	//crea una stringa JSON con i valori  dello stato corrente dei pulsanti
    t = frst;	
    //strcpy(s,openbrk); 
	//t += strlen(t);
	byte mm = nn / 2; //0, 1
	byte kk = mm * 2; //0, 2
	unsigned pr[2];
    
	printOutlogic();
	printOut();
	if(roll[mm] == true && shortm == false){
#if (ROLLER)
	  pr[mm] = percfdbck(mm);
	  long cronoc = getCronoCount(mm);
	  if(cronoc < 0)
		  cronoc = 0;
	#if(RS485)
	  au16data[RDUP1+kk] = out[kk];
	  au16data[RDDW1+kk] = out[kk+1];
	  BIT_WRITE(au16data[RDUP1+kk],au16data[RDBTN],1+kk);
	  BIT_WRITE(au16data[RDDW1+kk],au16data[RDBTN],2+kk);
	  au16data[RDPR1+mm] = pr[mm];
	  au16data[RDTR1L+kk] = l_2uint_int1((unsigned long)cronoc);
	  au16data[RDTR1H+kk] = l_2uint_int2((unsigned long)cronoc);
	  au16data[RDSP1L+kk] = l_2uint_int1((unsigned long)getTapThalt(mm));
	  au16data[RDSP1H+kk] = l_2uint_int2((unsigned long)getTapThalt(mm));
	#endif
	  //sprintf(frst,"%s%s%u%s",pars[MQTTUP1+kk]->jsoname,twodot,out[mm]==HIGH,comma);
	  strcpy(frst,pars[MQTTUP1+kk]->jsoname);
	  strcpy(t+=strlen(t),twodot);
	  itoa((out[kk]),t+=strlen(t),10);
	  strcpy(t+=strlen(t),comma);
	  //sprintf(t+=strlen(t),"%s%s%u%s",pars[MQTTDOWN1+kk]->jsoname,twodot,out[mm+1]==HIGH,comma);
	  strcpy(t+=strlen(t),pars[MQTTDOWN1+kk]->jsoname);
	  strcpy(t+=strlen(t),twodot);
	  itoa((out[kk+1]),t+=strlen(t),10);
	  strcpy(t+=strlen(t),comma);
	  if(blocked[0]>0){
		  sprintf(t+=strlen(t),"blk%d%s%u%s",mm+1,twodot,blocked[mm],comma);
	  }
	  //sprintf(t+=strlen(t),"%s%d%s%u%s","pr",mm+1,twodot,pr[mm],comma);
	  strcpy(t+=strlen(t),"pr");
	  itoa(mm+1,t+=strlen(t),10);
	  strcpy(t+=strlen(t),twodot);
	  itoa(pr[mm],t+=strlen(t),10);
	  strcpy(t+=strlen(t),comma);
	  sprintf(t+=strlen(t),"tr%d%s%ld%s",mm+1,twodot,(unsigned long)cronoc,comma);
	  sprintf(t+=strlen(t),"sp%d%s%lu",mm+1,twodot,(unsigned long)getTapThalt(mm));
#endif
	}else{
	#if(RS485)
	  au16data[RDUP1+nn] = out[nn]==HIGH;
	  BIT_WRITE(au16data[RDUP1+nn],au16data[RDBTN],nn);
	#endif
	  //sprintf(frst,"%s%s%u",pars[MQTTUP1+nn]->jsoname,twodot,(out[nn]==HIGH));
	  strcpy(frst,pars[MQTTUP1+nn]->jsoname);
	  strcpy(t+=strlen(t),twodot);
	  itoa((out[nn]==HIGH),t+=strlen(t),10);
	}
	if(all){
		//sprintf(t+=strlen(t),"%s",comma);
		strcpy(t+=strlen(t),comma);
		sprintf(t+=strlen(t),"%s%s%.2f%s",pars[MQTTTEMP]->jsoname,twodot,asyncBuf[GTTEMP],comma);
	#if (AUTOCAL_HLW8012)
	    sprintf(t+=strlen(t),"%s%s%.2f%s",pars[INSTPWR]->jsoname,twodot,asyncBuf[GTIPWR],comma);
		//sprintf(t+=strlen(t),"%s%s%u",pars[INSTACV]->jsoname,twodot,asyncBuf[GTIVAC]);
		strcpy(t+=strlen(t),pars[INSTACV]->jsoname);
		strcpy(t+=strlen(t),twodot);
		itoa(asyncBuf[GTIVAC],t+=strlen(t),10);
	#else
		sprintf(t+=strlen(t),"%s%s%.2f%s%.2f%s",pars[MQTTMEANPWR]->jsoname,opensqr,asyncBuf[GTMEANPWR1],comma,asyncBuf[GTMEANPWR2],closesqr2);
		sprintf(t+=strlen(t),"%s%s%.2f%s%.2f%s",pars[MQTTPEAKPWR]->jsoname,opensqr,asyncBuf[GTPEAKPWR1],comma,asyncBuf[GTPEAKPWR2],"\"]");
	#endif
	}else{
		//sprintf(t+=strlen(t),"%s",end);
		strcpy(t+=strlen(t),end);
	}
	//DEBUG1_PRINTLN("readStatesAndPub");
	//DEBUG1_PRINTLN(s);
	publishStr2();
}

String getLastFeedBack(){
	return s;
}

#if (ROLLER)
inline uint8_t percfdbck(uint8_t n){
	//n: 0,1
	//DEBUG2_PRINT(F("Posdelta:"));
	//DEBUG2_PRINTLN(getPosdelta());
	if(getDelayedCmd(n) <= 100){
		return round(calcLen(n) - getPosdelta());  
	}else{
		return round(calcLen(n));  
	}
}
#endif

void readActModeAndPub(uint8_t n){
  DEBUG2_PRINTLN(F("\nreadActionsAndPub")); 
  t=frst;	
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%d%s%s%s","actmode",n+1,twodot,pars[p(SWACTION1+n)]->val,end);
  publishStr();
}

void readModeAndPub(uint8_t n){
  DEBUG2_PRINTLN(F("\nreadModeAndPub")); 
  t=frst;		
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%u%s%u%s","mode",n+1,twodot,pars[p(SWROLL1+n)]->val,end);
  publishStr();
}

void readPwrCalAndPub(){
  DEBUG2_PRINTLN(F("\nreadPwrCalAndPub")); 
  t=frst;	
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%s%.2f%s",pars[p(PWRMULT)]->jsoname,twodot,pars[p(PWRMULT)]->val,end);
  publishStr();
}

void readAvgPowerAndPub(){
  DEBUG2_PRINTLN(F("\nreadPowerAndPub")); 
  t=frst;	
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%s%.2f%s%%.2f%s",pars[MQTTMEANPWR]->jsoname,opensqr,asyncBuf[GTMEANPWR1],comma,asyncBuf[GTMEANPWR2],closesqr4);
  publishStr();
}

void readPeakPowerAndPub(){
  DEBUG2_PRINTLN(F("\nreadPowerAndPub")); 
  t=frst;	
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%s%.2f%s%%.2f%s",pars[MQTTPEAKPWR]->jsoname,opensqr,asyncBuf[GTPEAKPWR1],comma,asyncBuf[GTPEAKPWR2],closesqr4);
  publishStr();
}

void readParamAndPub(uint8_t parid, char* str, bool ip){
  DEBUG2_PRINTLN(F("\nreadParamAndPub")); 
  t=frst;
    
  //strcpy(s,openbrk); 
  //t += strlen(t);
  //sprintf(frst,"%s%s%s%s",pars[parid]->jsoname,twodot,str,end);
  strcpy(frst,pars[parid]->jsoname);
  strcpy(t += strlen(t),twodot);
  strcpy(t += strlen(t),str);
  strcpy(t += strlen(t),end);
  //DEBUG1_PRINTLN(s);
  publishStr(ip);
}

void readTempAndPub(){
  DEBUG2_PRINTLN(F("\nreadTempAndPub")); 
  //DEBUG1_PRINTLN(s); 
  t=frst;
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%s%.2f%s",pars[MQTTTEMP]->jsoname,twodot,asyncBuf[GTTEMP],end);
  //DEBUG1_PRINTLN(s); 
  publishStr2();
}

void readIpwrAndPub(){
  DEBUG2_PRINTLN(F("\nreadIpwrAndPub")); 
  t=frst;	
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%s%.2f%s",pars[INSTPWR]->jsoname,twodot,asyncBuf[GTIPWR],end);
  publishStr2();
}

void readVacAndPub(){
  DEBUG2_PRINTLN(F("\nreadVacAndPub")); 
  t=frst;	
  //strcpy(s,openbrk); 
  //t += strlen(t);
  sprintf(frst,"%s%s%.2f%s",pars[INSTACV]->jsoname,twodot,asyncBuf[GTIVAC],end);
  publishStr2();
}
		
void readMacAndPub(){
	DEBUG2_PRINTLN(F("\nreadMacAndPub"));
  readTempAndPub();
}

void readIpAndPub(){
	DEBUG2_PRINTLN(F("\nreadIpAndPub"));
  readTempAndPub();
}

void readTimeAndPub(){
	DEBUG2_PRINTLN(F("\nreadTimeAndPub"));
  readTempAndPub();
}

void readMQTTIdAndPub(){
	DEBUG2_PRINTLN(F("\nreadMQTTIdAndPub"));
  readTempAndPub();
}

/*void readIacvoltAndPub(){
	DEBUG2_PRINTLN(F("\nreadIacvoltAndPub"));
  readTempAndPub();
}*/

void inline readActionConfAndSet(){
	//imposta le configurazioni dinamiche in base ad eventi esterni
	DEBUG2_PRINTLN(F("readActionConfAndSet."));
	eval( (static_cast<ParVarStr*>(pars[p(ACTIONEVAL)])->getStrVal()).c_str() );
}

void printSwitch(byte mm){
	//n: 0,1 switches
	uint8_t kk = mm*2;
	unsigned pr[2];
	
	//DEBUG1_PRINTLN(s);
	strcpy(t+=strlen(t),pars[MQTTUP1+kk]->jsoname);
	strcpy(t+=strlen(t),twodot);
	itoa(out[kk],t+=strlen(t),10);
	strcpy(t+=strlen(t),comma);
	//sprintf(t+=strlen(t),"%s%s%u%s",pars[MQTTDOWN1+kk]->jsoname,twodot,out[kk+1],comma);
#if !(ONEBTN)    
	strcpy(t+=strlen(t),pars[MQTTDOWN1+kk]->jsoname);
	strcpy(t+=strlen(t),twodot);
	itoa(out[kk+1],t+=strlen(t),10);
	strcpy(t+=strlen(t),comma);	
#endif
#if (ROLLER)
	if(roll[mm] == true){
	  pr[mm] = percfdbck(mm);
	  strcpy(t+=strlen(t),"pr");
	  itoa(mm+1,t+=strlen(t),10);
	  strcpy(t+=strlen(t),twodot);
	  itoa(pr[mm],t+=strlen(t),10);
	  strcpy(t+=strlen(t),comma);
	  sprintf(t+=strlen(t),"tr%d%s%ld%s",mm+1,twodot,(long)getCronoCount(mm),comma);
	  sprintf(t+=strlen(t),"sp%d%s%lu\",\"",mm+1,twodot,(unsigned long)getTapThalt(mm));
	}
#endif
	//DEBUG1_PRINTLN(s);
}

void publishStr(bool ip){
  //pubblica sul broker la stringa JSON
  //informazioni mittente
  //s += ",\"";
  //sprintf(t+=strlen(t),"%s",",\"");
  DEBUG1_PRINTLN(F("publishStr: "));
  strcpy(t+=strlen(t),",\"");
  if(ip){
	//sprintf(t+=strlen(t),"%s%s%s%s",pars[MQTTIP]->jsoname,twodot,static_cast<ParStr32*>(pars[p(LOCALIP)])->val,comma);
	strcpy(t+=strlen(t),pars[MQTTIP]->jsoname);
	strcpy(t+=strlen(t),twodot);
	strcpy(t+=strlen(t),static_cast<ParStr32*>(pars[p(LOCALIP)])->val);
	strcpy(t+=strlen(t),comma);
	printSwitch(0);
#if !(ONEBTN || TWOBTN)
	printSwitch(1);
#endif
  }
 #if (AUTOCAL_HLW8012) 
  sprintf(t+=strlen(t),"%s%s%.2f%s",pars[INSTPWR]->jsoname,twodot,asyncBuf[GTIPWR],comma);
  sprintf(t+=strlen(t),"%s%s%.2f%s",pars[INSTACV]->jsoname,twodot,asyncBuf[GTIVAC],comma);
 #endif
  //sprintf(t+=strlen(t),"%s%s%s%s",pars[MQTTDATE]->jsoname,twodot,printUNIXTimeMin(gbuf2),closebrk);
  sprintf(t+=strlen(t),"%s%s%.2f%s",pars[MQTTTEMP]->jsoname,twodot,asyncBuf[GTTEMP],comma);
  strcpy(t+=strlen(t),pars[MQTTDATE]->jsoname);
  strcpy(t+=strlen(t),twodot);
  strcpy(t+=strlen(t),printUNIXTimeMin(gbuf2));
  strcpy(t+=strlen(t),closebrk);
  //DEBUG1_PRINT(F("publishStr: "));
  DEBUG1_PRINTLN(s);
  webSocket.broadcastTXT(s);
  
  if(mqttClient==NULL){
	  DEBUG1_PRINTLN(F("ERROR on publishStr MQTT client is not allocated."));
  }else{
	  DEBUG1_PRINT(F("mqttConnected: "));
	  DEBUG1_PRINTLN(mqttConnected);
	  if(mqttConnected){
		  //str deve essere convertita in array char altrimenti la libreria introduce un carattere spurio all'inizio del messaggio
		  mqttClient->publish((const char *)static_cast<ParStr32*>(pars[p(MQTTOUTTOPIC)])->val, s, strlen(s));
		  DEBUG1_PRINTLN(F("Published data 1: "));
		  DEBUG2_PRINTLN(s);
	  }else{
		  DEBUG1_PRINTLN(F("ERROR on publishStr MQTT client is not connected."));
	  }
  }
  //if(!webSocket){
	  //DEBUG2_PRINTLN(F("ERROR on readStatesAndPub webSocket server is not allocated."));
  //}
  //else
  //{
  
  //}
}

void publishStr2(){
  //pubblica sul broker la stringa JSON
  //informazioni mittente
  //sprintf(t+=strlen(t),"%s",endbrk);
  strcpy(t+=strlen(t),endbrk);
  DEBUG1_PRINTLN(F("\npublishStr2: "));
  DEBUG1_PRINTLN(s);
  webSocket.broadcastTXT(s);
  
  if(mqttClient==NULL){
	  DEBUG1_PRINTLN(F("ERROR on publishStr MQTT client is not allocated."));
  }else{ 
	  DEBUG1_PRINT(F("mqttConnected: "));
	  DEBUG1_PRINTLN(mqttConnected);
	  if(mqttConnected){
		  //str deve essere convertita in array char altrimenti la libreria introduce un carattere spurio all'inizio del messaggio
		  mqttClient->publish((const char *)static_cast<ParStr32*>(pars[p(MQTTOUTTOPIC)])->val, s, strlen(s));
		  DEBUG1_PRINT(F("Published data 2: "));
		  DEBUG2_PRINTLN(s);
	  }else{
		  DEBUG1_PRINTLN(F("ERROR on publishStr MQTT client is not connected."));
	  }
  }
  //if(!webSocket){
	  //DEBUG2_PRINTLN(F("ERROR on readStatesAndPub webSocket server is not allocated."));
  //}
  //else
  //{
  //}
}

void initIiming(bool first){
  //edelay[0] = static_cast<ParLong*>(pars[p(STDEL1)])->val; 
  //edelay[1] = static_cast<ParLong*>(pars[p(STDEL2)])->val;
  //roll[0] = static_cast<ParUint8*>(pars[p(SWROLL1)])->val;
  //roll[1] = static_cast<ParUint8*>(pars[p(SWROLL2)])->val;
  DEBUG2_PRINT(F("Roll1: "));
  DEBUG2_PRINTLN(roll[0]);
  DEBUG2_PRINT(F("Roll2: "));
  DEBUG2_PRINTLN(roll[1]);
  mov = false;
  initTapparellaLogic(in,out,first);
#if (AUTOCAL)  
  resetAVGStats(0,0);
  resetAVGStats(0,1);
#endif  
}

inline void setupNTP() {
  setNtpServer(0,(const char*) static_cast<ParStr32*>(pars[p(NTPADDR1)])->val);
  setNtpServer(1,(const char*) static_cast<ParStr32*>(pars[p(NTPADDR2)])->val); 
  setSyncInterval((unsigned long) static_cast<ParLong*>(pars[p(UTCSYNC)])->val);
  setSDT((uint8_t) static_cast<ParUint8*>(pars[p(UTCSDT)])->val);
  adjustTime((unsigned long) static_cast<ParInt*>(pars[p(UTCADJ)])->val); 
  setTimeZone((int) static_cast<ParInt*>(pars[p(UTCZONE)])->val);
  sntpInit();  
}

#if (AUTOCAL)
void setValweight(float wht){
	DEBUG2_PRINT(F("setValweight: "));
	DEBUG2_PRINTLN(wht);
	weight[0] = wht;
	weight[1] = 1 - weight[0];
}
#endif

/*
void testGPIO(){
	uint16_t gpioread = (uint16_t)GPI;
	uint16_t inmask = FILTERMASK; 						//01000110 00000001 (17921)
	inmask = gpioread & inmask; 
	sprintf(s,"GPIOIN: "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(inmask>>8), BYTE_TO_BINARY(inmask));
	Serial.println(s);
}
*/

void setup(){
	
#if (RS485)
	Serial.begin(MODBUSBAUDRATE);
	Serial1.begin(115200);
#else
	Serial.begin(115200);
#endif
   	
	outPorts[0] = OUT1EU;
	inPorts[0] = BTN1U;
	outPorts[1] = OUT1DD;
	inPorts[1] = BTN1D;
#if !(TWOBTN || ONEBTN)
	outPorts[2] = OUT2EU;
	outPorts[3] = OUT2DD;
	inPorts[2] = BTN2U;
	inPorts[3] = BTN2D;
#endif		    	
		 
	Serial.println(F("Inizializzo la seriale."));
#if (MCP2317) 
	mcp.begin(); 
	//pinMode INPUT
	for(int i=0; i<BTNDIM*NBTN; i++){
		mcp.pinMode(inPorts[i], INPUT);
	}
	for(int i=0; i<BTNDIM*NBTN; i++){
		doPwrSpl[i]=255;
	}
    
	//mcp.pinMode(BTN1U, INPUT);
	//mcp.pinMode(BTN1D, INPUT);
	//mcp.pinMode(BTN2U, INPUT);
	//mcp.pinMode(BTN2D, INPUT);
	//pinMode OUTPUT
	for(int i=0; i<BTNDIM*NBTN; i++){
		mcp.pinMode(outPorts[i], OUTPUT);
	}
	//mcp.pinMode(OUT1EU,OUTPUT);
	//mcp.pinMode(OUT1DD,OUTPUT);
	//mcp.pinMode(OUT2EU,OUTPUT);
	//mcp.pinMode(OUT2DD,OUTPUT);
	mcp.pinMode(GREEN,OUTPUT);
	mcp.pinMode(RED,OUTPUT);
	mcp.pinMode(BLUE,OUTPUT);
	mcp.pinMode(UNUSE1,INPUT);
	mcp.pinMode(UNUSE2,INPUT);
	mcp.pinMode(UNUSE3,INPUT);
	mcp.pinMode(UNUSE4,INPUT);
	mcp.pinMode(UNUSE5,INPUT);
	for(int i=0; i<BTNDIM*NBTN; i++){
		mcp.digitalWrite(outPorts[i], LOW);
	}
	
	//mcp.digitalWrite(OUT1EU, LOW);
	//mcp.digitalWrite(OUT1DD, LOW);
	//mcp.digitalWrite(OUT2EU, LOW);
	//mcp.digitalWrite(OUT2DD, LOW);
	//mcp.digitalWrite(GREEN, LOW);
	//mcp.digitalWrite(RED, LOW);
	//mcp.digitalWrite(BLUE, LOW);
	//setColor(4); //red on
    for(int i=0; i<BTNDIM*NBTN; i++){
		mcp.pullUp(inPorts[i], LOW);
	}
	
	//mcp.pullUp(BTN1U, LOW);
	//mcp.pullUp(BTN1D, LOW);
	//mcp.pullUp(BTN2U, LOW);
	//mcp.pullUp(BTN2D, LOW);
	
	mcp.pullUp(UNUSE1, HIGH);
	mcp.pullUp(UNUSE2, HIGH);
	mcp.pullUp(UNUSE3, HIGH);
	mcp.pullUp(UNUSE4, HIGH);
	mcp.pullUp(UNUSE5, HIGH);
	//pinMode(OUTSLED,OUTPUT);
	//digitalWrite(OUTSLED, LOW);
#else
	
  #if (INPULLUP)
	for(int i=0; i<BTNDIM*NBTN; i++){
		pinMode(inPorts[i], INPUT_PULLUP);
	}
	
	//pinMode(BTN1U, INPUT_PULLUP);
	//pinMode(BTN1D, INPUT_PULLUP);
	//pinMode(BTN2U, INPUT_PULLUP);
	//pinMode(BTN2D, INPUT_PULLUP);
  #else
	for(int i=0; i<BTNDIM*NBTN; i++){
		pinMode(inPorts[i], INPUT);
	}

	//pinMode(BTN1U, INPUT);
	//pinMode(BTN1D, INPUT);
	//pinMode(BTN2U, INPUT);
	//pinMode(BTN2D, INPUT);
  #endif
    
	pinMode(OUTSLED,OUTPUT);
	digitalWrite(OUTSLED, HIGH);
	//DEBUG_PRINTLN(F("Inizializzo le uscite."));
	for(int i=0; i<BTNDIM*NBTN;i++){
		pinMode(outPorts[i],OUTPUT);
		digitalWrite(outPorts[i], LOW);
	}
#endif
 
  //no DEBUG calls before here
  Serial.println(F("Inizializzo i parametri."));
  dbg1 = new SerialLog(1);
  dbg2 = new SerialLog(2);
  mqttClient = NULL;
  delay(10);
  DEBUG_PRINTLN(F("Inizializzo i parametri."));
  //initOfst();
  dosmpl = false;
  zeroCnt = 0;
  mqttcnt = 0;
  mqttofst = 2;
  delay(7000);
  //testGPIO();
  //inizializza la seriale
  //importante per il _DEBUG del WIFI!
  //WiFi.printDiag(Serial);
  //carica la configurazione dalla EEPROM
  //DEBUG2_PRINTLN(F("Carico configurazione."));
  //for(int i=0;i<CONFDIM;i++)
  
  DEBUG_PRINTLN(F("initCommon."));
  initCommon(&server,pars);
 #if (AUTOCAL_HLW8012) //////
  HLW8012_init();		////////
#endif
  delay(10); 
  initOfst();
  delay(10); 
  //testGPIO();
  DEBUG_PRINTLN(F("loadConfig."));
  loadConfig();
  //testGPIO();
  DEBUG_PRINTLN(F("Config loaded."));
  delay(100); 
  wifiConn = false;
  startWait=false;
  endWait=true;
#if (RS485)
	WiFi.printDiag(Serial1);
#else
	WiFi.printDiag(Serial);
#endif
  //WiFi.enableAP(false);
  //delay(100);
  //WiFi.persistent(true);
  //WiFi.softAPdisconnect(true);
  ESP.eraseConfig();
  WiFi.persistent(false);
  //ESP.eraseConfig();
  //WiFi.softAPdisconnect(true);
  //ESP.eraseConfig();
  //inizializza l'AP wifi
  //setup_AP(true);
  wifiState = WIFISTA;
  //WiFi.mode(WIFI_STA);
  //WiFi.mode(WIFI_OFF); 
 
  DEBUG1_PRINTLN(F("Init diff tasti."));
  for(int i=0; i<BTNDIM*NBTN;i++){
	  initdfn(LOW, i);  //pull DOWN init (in realtà è un pull up, c'è un not in ogni ingresso sui pulsanti)
  }
 
  DEBUG2_PRINTLN(F("Init counters."));
  for(int i=0; i<BTNDIM*NBTN;i++){
	  startCnt(0, 1, CONDCNT1+i); 
  }
 
  startCnt(0, ACTPWRINT, ACTPWRCNT);
  startCnt(0, ACVOLTINT, ACVOLTCNT);
  startCnt(0, TEMPINT, TEMPCNT);
  delay(10); 
  //Timing init
  
  DEBUG1_PRINTLN(F("Init timing."));
  initIiming(true);
  delay(10); 
  setSWMode(static_cast<ParUint8*>(pars[p(SWROLL1)])->val,0);
  setSWMode(static_cast<ParUint8*>(pars[p(SWROLL2)])->val,1);
  delay(10); 

  DEBUG1_PRINTLN(F("Init wifi."));
  setup_wifi(wifindx);
  delay(100);  
 
  DEBUG1_PRINTLN(F("Init NTP."));
  setupNTP();
  delay(10); 
  //setTimerState(wfs, CONNSTATSW);
  //delay(10); 
#if(LARGEFW)

  DEBUG1_PRINTLN(F("Init Telnet."));
  telnet.begin((const char *) static_cast<ParStr32*>(pars[p(LOCALIP)])->val); // Initiaze the telnet server
  telnet.setResetCmdEnabled(true); // Enable the reset command
  telnet.setCallBackProjectCmds(&processCmdRemoteDebug);
  DEBUG1_PRINTLN(F("Activated remote _DEBUG"));
#endif  
  delay(10); 
 
  DEBUG1_PRINTLN(F("Init HTTP server."));
  httpSetup();
  delay(100);
  //testGPIO();
  //must be after http setup almeno 100ms se no si pianta!
  DEBUG1_PRINTLN(F("Init Websocket server."));
  startWebSocket();
  delay(100);
  DEBUG1_PRINTLN(F("Init counters."));
  for(int i=0; i<BTNDIM*NBTN;i++){
	startCnt(0,0,SMPLCNT1+i);
  }
  delay(10); 
  DEBUG1_PRINTLN(F("Init outlogic vector."));
  /*for(int i=0;i<(NBTN*RSTATUSDIM);i++){
	  outLogic[i]=LOW;
  }*/
  delay(10);
  DEBUG1_PRINTLN("Init params vector.");  
  for(int i=0;i<MQTTDIM;i++){
	  if(pars[i] != NULL){
		static_cast<ParUint8*>(pars[i])->load(LOW);
	  }
  }
  delay(10);
  setColor(4);
  
	
  //startCnt(0,60,TIMECNT);
  //read and set dynamic configurations
  //DEBUG1_PRINTLN(F("bho3."));
  //imposta la DIRSezione delle porte dei led, imposta inizialmente i led come spento  
  
  //------------------------------------------OTA SETUP---------------------------------------------------------------------------------------
  //------------------------------------------END OTA SETUP---------------------------------------------------------------------------------------
  //delay(500);
  //DEBUG1_PRINTLN(F("bho4."));
  
  // Register event handlers.
  // Callback functions will be called as long as these handler objects exist.
  // Call "onStationConnected" each time a station connects
  DEBUG1_PRINTLN("Init wifi handlers.");
  stationConnectedHandler = WiFi.onSoftAPModeStationConnected(&onStationConnected);
  // Call "onStationDisconnected" each time a station disconnects
  stationDisconnectedHandler = WiFi.onSoftAPModeStationDisconnected(&onStationDisconnected);
  delay(10);

#if defined (_DEBUG) || defined (_DEBUGR)  
  testFlash();
  delay(10);
#endif
#if (AUTOCAL_ACS712)  
  zeroDetect();
  delay(10);
#endif
  cont=0;
  DEBUG1_PRINTLN(F("Wifi A conn delay."));
  while (WiFi.status() != WL_CONNECTED && cont<3000/500) {
     delay(500);
	 cont++;
     DEBUG1_PRINT(".");
  }
  delay(3000);
  if(WiFi.status() != WL_CONNECTED){
	DEBUG1_PRINTLN(F("Change to SSID B."));
	wifindx++;
	setup_wifi(wifindx);
	cont = 0;
	DEBUG1_PRINTLN(F("Wifi B conn delay."));
	while (WiFi.status() != WL_CONNECTED && cont<30000/500) {
		delay(500);
		cont++;
		DEBUG1_PRINT(".");
	}
  }
  
  delay(1000);
  DEBUG1_PRINT(":");
  DEBUG1_PRINT(500*cont);
  if(cont==300000/500){
	DEBUG1_PRINT("\nStation not connected!");  
  }else{
	DEBUG1_PRINT("\nStation connected, IP: ");
	DEBUG1_PRINTLN((WiFi.localIP()).toString());
  }
  
  swcount = 0;
  DEBUG1_PRINTLN(F(" OK"));
  DEBUG1_PRINTLN(F("Last reset reason: "));
  DEBUG1_PRINTLN(ESP.getResetReason());
  
  if(WiFi.status() == WL_CONNECTED){
	  IPAddress ip = WiFi.localIP();
	  ip.toString().toCharArray(IP, 16);
	  pars[p(LOCALIP)]->load(IP);
	  DEBUG1_PRINTLN(F("AP client IP address = "));
	  DEBUG1_PRINTLN(((ParStr32*)pars[p(LOCALIP)])->val);
	  //MQTT INIT
	  mqttReconnect();
	  wifiConn = true;	
  }else{
	  wifiConn = false;
  }
  boot = false;
  
  DEBUG1_PRINTLN(F("sampleCurrTime()"));
  sampleCurrTime();
  npinger = 0;
  for(int i=0; i<4;i++){
	pinger[i].SetPacketsId(i);
	setPinger();
	npinger++;
  }
  DEBUG1_PRINTLN(F("Init actions."));
  readActionConfAndSet();  
  //Turn off WiFi
  //WiFi.mode(WIFI_OFF);    //This also w
  //initTemp();
}

void setPinger(){
	//n: 0,1,2,3 number of pingers
	pinger[npinger].OnReceive([](const PingerResponse& response)
	  {
		if (response.ReceivedResponse)
		{
		    //Serial.printf(
			//"Reply from %s: bytes=%d time=%lums TTL=%d\n",
			//response.DestIPAddress.toString().c_str(),
			//response.EchoMessageSize - sizeof(struct icmp_echo_hdr),
			//response.ResponseTime,
			//response.TimeToLive);
			DEBUG1_PRINT(F("Reply from " ));
			DEBUG1_PRINT(response.DestIPAddress.toString());
			DEBUG1_PRINT(F(": bytes=" ));
			DEBUG1_PRINT(response.EchoMessageSize - sizeof(struct icmp_echo_hdr));
			DEBUG1_PRINT(F(": time=" ));
			DEBUG1_PRINT(response.ResponseTime);
			DEBUG1_PRINT(F(":  TTL=" ));
			DEBUG1_PRINTLN(response.TimeToLive);
			
			if(lastPing[npinger] < nping[npinger]){
				lastPing[npinger]++;
				pingPer2[npinger] = 1;
			}else{
				pingPer2[npinger] = pingPer[npinger];
			}
			DEBUG2_PRINT(F(" ok-pingPer2[0]: "));
			DEBUG2_PRINT(pingPer2[npinger]);
			DEBUG2_PRINT(F(" ok-lastPing[0]: "));
			DEBUG2_PRINT(lastPing[npinger]);
			DEBUG2_PRINT(F(" ok-nping[0]: "));
			DEBUG2_PRINTLN(nping[npinger]);
		}
		else
		{
		  if(lastPing[npinger] > 0){
				lastPing[npinger]--;
				pingPer2[npinger] = 1;
			}else{
				pingPer2[npinger] = pingPer[npinger];
			}
			DEBUG2_PRINT(F(" ko-pingPer2[0]: "));
			DEBUG2_PRINTLN(pingPer2[npinger]);
			DEBUG2_PRINT(F(" ko-lastPing[0]: "));
			DEBUG2_PRINTLN(lastPing[npinger]);
			DEBUG2_PRINT(F(" ko-nping[0]: "));
			DEBUG2_PRINTLN(nping[npinger]);
		  //toPing[id] = true;
		  DEBUG1_PRINT("Request ping 0 timed out.\n");
		}
		DEBUG2_PRINT(F(" n: "));
		DEBUG2_PRINTLN(npinger);
		// Return true to continue the ping sequence.
		// If current event returns false, the ping sequence is interrupted.
		return true;
	  });
}

void httpSetup(){
  DEBUG2_PRINTLN(F("Registro handleRootExt."));
  server.on("/", HTTP_GET, handleRoot);        // Call the 'handleRoot' function when a client requests URI "/"
  DEBUG2_PRINTLN(F("Registro handleLoginExt."));
  server.on("/login", HTTP_POST, handleLogin); // Call the 'handleLogin' function when a POST request is made to URI "/login"
  DEBUG2_PRINTLN(F("Registro handleModifyExt."));
  server.on("/modify", HTTP_POST, handleModify);
  server.on("/systconf", HTTP_POST, handleSystemConf);
  server.on("/wificonf", HTTP_POST, handleWifiConf);
  server.on("/mqttconf", HTTP_POST, handleMQTTConf);
#if (ROLLER)
  server.on("/logicconf", HTTP_POST, handleLogicConf);
#endif
  server.on("/logconf", HTTP_POST, handleLogConf);
  server.on("/eventconf", HTTP_POST, handleEventConf);
  server.on("/cmd", HTTP_GET, handleCmd);
  server.on("/mqttcmd", HTTP_GET, handleMqttCmd);
  server.on("/favicon.ico", HTTP_GET, handleFavicon);
  server.on("/btncmd", HTTP_POST, handleBtnCmd);
  //server.on("/cmdjson", handleCmdJsonExt);
  //DEBUG2_PRINTLN(F("Registro handleNotFoundExt."));
  server.onNotFound(handleNotFound);           // When a client requests an unknown URI (i.e. something other than "/"), call function "handleNotFound"
  //avvia il server HTTP*/
  DEBUG2_PRINTLN("Inizio avvio il server web.");
  //here the list of headers to be recorded
  const char * headerkeys[] = {"User-Agent","Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys)/sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize );
  //OTA web page handler linkage
  httpUpdater.setup(&server);
  //start HTTP server
  server.begin();
  DEBUG2_PRINTLN("HTTP server started");
#if(LARGEFW)
  DEBUG2_PRINTLN("Avvio il responder mDNS.");
  delay(100);
  //setup_mDNS();
  //MDNS.addService(F("http"), F("tcp"), 80); 
  DEBUG2_PRINT("HTTPUpdateServer ready! Open http://");
  DEBUG2_PRINT(pars[p(MQTTID)]->getStrVal()); 
  DEBUG2_PRINTLN(".local/update in your browser");
#endif
}

#if (AUTOCAL_ACS712) 
void zeroDetect(){
	for(int i = 0, m = 0; i < 2000; i++) {
		m = (float) m + analogRead(A0);
		delay(2);
	}
	m /= 2000;
	smplcnt = 0;
	minx = 1024;
	maxx = 0;
	smplcnt2 = 0;
}
#endif

void startPageLoad(){
	pageLoad = true;
}

void stopPageLoad(){
	pageLoad = false;
}

#if (RS485) 
void leggiTastiModbus(uint8_t regA){
	char s[18];
	uint8_t inmask = regA & FILTERMASK;	//00001111 (15)
	uint8_t inmask2;
	if(inmask != 0){ //pulldown!
	//if(inmask != 240){ //pulldown!
		inmask2 = FILTERMASK;
		DEBUG2_PRINT(F("Inmask: "));
		DEBUG2_PRINTLN(inmask2);
		sprintf(s,"MASK  : "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(inmask2));
		DEBUG2_PRINT(s);
		sprintf(s,"GPIOIN: "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(regA));
		DEBUG2_PRINT(s);
		
#if (!ONEBTN)
		for(int i=0; i<BTNDIM*NBTN; i++){
			if(bitRead(regA, i)){
				static_cast<ParUint8*>(pars[MQTTUP1+i])->load((uint8_t)255); 
				static_cast<ParUint8*>(pars[MQTTUP1+i])->doaction(1); //fast
			}
			break;
		}
#else
		if(bitRead(regA, 0)){
			static_cast<ParUint8*>(pars[MQTTUP1])->load((uint8_t)255); 
			static_cast<ParUint8*>(pars[MQTTUP1])->doaction(1); //fast
		}
#endif
		au16data[WRBTN] = 0; //reset cmd register
	}
}
	
void leggiTastiModbus16(){
	uint8_t inmask = au16data[WRUP1] || au16data[WRDW1] || au16data[WRUP2] || au16data[WRDW2];
	if(inmask != 0){ //pulldown!
		
#if (!ONEBTN)
		for(int i=0; i<BTNDIM*NBTN; i++){
			if(au16data[WRUP1+i]>0 && au16data[WRUP1+i]<256){
				static_cast<ParUint8*>(pars[MQTTUP1+i])->load((uint8_t)au16data[WRUP1+i]); 
				static_cast<ParUint8*>(pars[MQTTUP1+i])->doaction(1); //fast
				au16data[WRUP1] =  au16data[WRDW1] = au16data[WRUP2] = au16data[WRDW2] = 0; //reset cmd register
				break;
				
			}
		}
		/*if(au16data[WRPR1]>0){
			static_cast<ParUint8*>(pars[MQTTUP1])->load((uint8_t)au16data[WRPR1]); 
			static_cast<ParUint8*>(pars[MQTTUP1])->doaction(1); //fast
			au16data[WRPR1] = 0; //reset cmd register
		}
		if(au16data[WRPR2]>0){
			static_cast<ParUint8*>(pars[MQTTUP2])->load((uint8_t)au16data[WRPR2]); 
			static_cast<ParUint8*>(pars[MQTTUP2])->doaction(1); //fast
			au16data[WRPR2] = 0; //reset cmd register
		}*/
#else
		if(au16data[WRUP1]==255){
			static_cast<ParUint8*>(pars[MQTTUP1])->load((uint8_t)255); 
			static_cast<ParUint8*>(pars[MQTTUP1])->doaction(1); //fast
			au16data[WRUP1] = 0; //reset cmd register
		}
#endif
	}
#if (ROLLER)
	inmask = au16data[WRPR1] || au16data[WRPR2];
	if(inmask != 0){ //pulldown!

		if(au16data[RDPR1]==1){
			au16data[RDPR1] = percfdbck(0);
			//au16data[WRPR1] = 0; //reset cmd register
		}else if(au16data[WRPR2]==1){
			au16data[RDPR2] = percfdbck(1);
			//au16data[WRPR2] = 0; //reset cmd register
		}
		au16data[WRPR1] = au16data[WRPR2] = 0; //reset cmd register
	}
#endif	
}
#endif	

void leggiTastiLocali2(){
#if (MCP2317) 
	char s[18];
    uint8_t regA = mcp.readInputs();
#if (RS485)
	au16data[RDGPA] = regA;
#endif
	uint8_t inmask = regA & FILTERMASK;	//00001111 (15)
	uint8_t inmask2;
	
	//uint8_t inmask = regA & 0xF0;	//00001111 (15)
	if(inmask != FILTERMASK){ //pullup!
	//if(inmask != 240){ //pullup!
		inmask2 = FILTERMASK;
		DEBUG2_PRINT(F("Inmask: "));
		DEBUG2_PRINTLN(inmask2);
		sprintf(s,"MASK  : "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(inmask2));
		DEBUG2_PRINT(s);
		sprintf(s,"GPIOIN: "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(regA));
		DEBUG2_PRINT(s);
		
#if (!ONEBTN)
		for(int i=0; i<BTNDIM*NBTN; i++){
			if(!bitRead(regA, inPorts[i])){
				static_cast<ParUint8*>(pars[MQTTUP1+i])->load((uint8_t)255); 
				static_cast<ParUint8*>(pars[MQTTUP1+i])->doaction(0); //delayed
				break; //no pressioni contemporanee
			}
		}
#else
		if(!bitRead(regA, inPorts[0])){
			static_cast<ParUint8*>(pars[MQTTUP1])->load((uint8_t)255); 
			static_cast<ParUint8*>(pars[MQTTUP1])->doaction(0); //delayed
		}
#endif
		//printMcpRealOut();
	}else if(inflag){
		DEBUG2_PRINTLN(F("Fronte di discesa "));
		inflag = false;
		initdfnUL(LOW,BTNDIM*NBTN);
		resetTimer(RESETTIMER);
		//rilascio interblocco gruppo 1
#if (ROLLER)
		if(roll[0] == true){
			resetOutlogic(0);
		}
		//rilascio interblocco gruppo 2
		if(roll[1] == true){
			resetOutlogic(1);
		}
#endif
	}
#else	
	uint16_t gpioread = (uint16_t)GPI;
	uint16_t inmask = FILTERMASK; 						//01000110 00000001 (17921)
	inmask = gpioread & inmask; 
	uint16_t inmask2;									 //10111001 1
	char s[26];
	
	if(inmask != FILTERMASK){ //pullup!
		inmask2 = FILTERMASK;
		DEBUG2_PRINT(F("Inmask: "));
		DEBUG2_PRINTLN(inmask2);
		sprintf(s,"MASK  : "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(inmask2>>8), BYTE_TO_BINARY(inmask2));
		DEBUG2_PRINT(s);
		sprintf(s,"GPIOIN: "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n", BYTE_TO_BINARY(inmask>>8), BYTE_TO_BINARY(inmask));
		DEBUG2_PRINT(s);
		
#if (!ONEBTN)
		for(int i=0; i<BTNDIM*NBTN; i++){
			if(!GPIP(inPorts[i])){
				static_cast<ParUint8*>(pars[MQTTUP1+i])->load((uint8_t)255); 
				static_cast<ParUint8*>(pars[MQTTUP1+i])->doaction(0); //delayed
			}
		}
#else
		if(!GPIP(inPorts[0])){
			DEBUG2_PRINTLN(F("inport "));
			DEBUG2_PRINTLN(inPorts[0]);
			static_cast<ParUint8*>(pars[MQTTUP1])->load((uint8_t)255); 
			static_cast<ParUint8*>(pars[MQTTUP1])->doaction(0);
		}
#endif
	}else if(inflag){
		DEBUG2_PRINTLN(F("Fronte di discesa "));
		inflag = false;
		initdfnUL(LOW,BTNDIM*NBTN);
		resetTimer(RESETTIMER);
#if (ROLLER)
		//rilascio interblocco gruppo 1
		if(roll[0] == true){
			resetOutlogic(0);
		}
		//rilascio interblocco gruppo 2
		if(roll[1] == true){
			resetOutlogic(1);
		}
#endif
	}
#endif	
}


void loop(){
	if(boot == false){
		//busy loop
		loop2();
	}else{
		//Dummy loop! Finestra solo wifi!
		//workaround for the DHCP offer problem ERROR: send_offer (error -13)
		//no busy loop! For difficult (intensive time consuming) wifi operation
		delay(3000);
		boot = false;
//#if (MCP2317) 
		setColor(0); // all color off
//#endif
	}
}
	
inline void loop2() {
  //PRE SCHEDULERS ACTIONS ----------------------------------------------
  //ArduinoOTA.handle();
  //funzioni eseguite ad ogni loop (istante di esecuzione dipendente dal clock della CPU)
  aggiornaTimer(TMRHALT);
  aggiornaTimer(TMRHALT+TIMERDIM); 
  aggiornaTimer(TMRHALT2);
  aggiornaTimer(TMRHALT2+TIMERDIM); 
  server.handleClient();  // Listen for HTTP requests from clients
  webSocket.loop();
#if(RS485)
  modbusState = slave.poll(au16data,REGDIM);
  if(modbusState > 4){
	leggiTastiModbus(au16data[WRBTN]);
	leggiTastiModbus16();
  }
#endif  
#if(LARGEFW)
  //MDNS.update();
#endif  
  //FINE PRE SCHEDULERS ACTIONS -----------------------------------------  
  
  //SCHEDULATED LOOPS------------------------------------------------------------------------------
  //Linee guida:
  //- azioni pesanti si dovrebbero eseguire molto raramente (o per pochi loop)
  //- azioni leggere possono essere eseguite molto frequentemente (o per molti loop)
  //- vie di mezzo di complessità da eseguire con tempi intermedi
  //- evitare il più possibile la contemporaneità di azioni pesanti 
  //-----------------------------------------------------------------------------------------------
  
  //-----------------------------------------------------------------------------------------------
  // 2 msec scheduler (main scheduler)
  //-----------------------------------------------------------------------
  if((millis()-prec) > TBASE) //schedulatore per tempo base 
  {	
	prec = millis();
	//calcolo dei multipli interi del tempo base
	step = (step + 1) % nsteps;
	
#if (AUTOCAL) 
#if (AUTOCAL_ACS712) 
	if(dosmpl){//solo a motore in moto
		currentPeakDetector();
	}
#endif
	// 10-20 msec scheduler
	//---------------------------------------------------------------------
#if (ROLLER && AUTOCAL)
	if(!(step % STOP_STEP)){
		automaticStopManager();
	}//END 20ms scheduler--------------------------------------------------
#endif
#endif

#if (MCP2317) 
	if(!(step % LED_STEP)){		
		 //printOut();
	}//END LED_STEP scheduler----------------------------------------------
#endif

	//---------------------------------------------------------------------
	// 1 sec scheduler
	//---------------------------------------------------------------------
	if(!(step % ONESEC_STEP)){
		updateCounters();
		
		//sempre vero se si è in modalit� switch!	
		if(!mov){//solo a motore fermo! Per evitare contemporaneità col currentPeakDetector
			aggiornaTimer(RESETTIMER);
			aggiornaTimer(APOFFTIMER);
			pushCnt++;
			wifiConn = (WiFi.status() == WL_CONNECTED);	
			
			DEBUG2_PRINT(F("wifiConn: "));
			DEBUG2_PRINT(WiFi.status());
			DEBUG2_PRINT(F(", wificnt: "));
			DEBUG2_PRINTLN(wificnt);
		    
			if(!mov && pageLoad == false)
				wifiFailoverManager();

#if (AUTOCAL_ACS712) 
			if(pageLoad == false && !doZeroSampl && !dosmpl)
				MQTTReconnectManager();
#else
			if(pageLoad == false)
				MQTTReconnectManager();
#endif
			
			pwrSampler();
			//yield();
		}
		if(sampleCurrTime()){//1min	
            itoa(millis()/1000,gbuf2,10);		
			readParamAndPub(MQTTTIME,gbuf2,true);
			//readStatesAndPub();
			//readTempAndPub();
		}
		//leggiTastiLocaliDaExp();
		//if(pageLoad == false)
		sensorStatePoll();
		yield();
		//ipcount = (ipcount + 1) % 30;
	}//END 1 sec scheduler-----------------------------------------------------
	
	//---------------------------------------------------------------------
	// 50-60 msec scheduler
	//---------------------------------------------------------------------
	if(!(step % MAINPROCSTEP)){	
		//leggi ingressi locali e mette il loro valore sull'array val[]
		leggiTastiLocali2();
		//leggiTastiLocaliRemoto();
		//se uno dei tasti delle tapparelle è stato premuto
		//o se è arrivato un comando dalla mqttCallback
		//provenienti dalla mqttCallback
		//remote pressed button event
		//leggiTastiRemoti();
	}//END 50-60ms scheduler------------------------------------------------------------------------------------
  }//END Time base (2-20 msec) main scheduler------------------------------------------------------------------------  
  //POST SCHEDULERS ACTIONS-----------------
#if(LARGEFW)
  if(dbg1->isTelnet() || dbg2->isTelnet()){
	telnet.handle();
  }
#endif  
  yield();	// Give a time for ESP8266
}//END loop

inline void updateCounters(){
	 incCnt(CNTIME1);
	 incCnt(CNTIME2);
	 incCnt(CNTIME3);
	 incCnt(CNTIME4);
	 //incCnt(TIMECNT);
}

inline void testSwitch(byte nn){
	//nn: 0,1,2,3 buttons
	//mm: 0,1 switches
	byte mm = nn / 2;
	bool pub = false;
	int app;
	
	if(incAndtestUpCntEvnt(0,true,SMPLCNT1+n)){
		app = eval( (static_cast<ParVarStr*>(pars[p(ONCOND1+n)])->getStrVal()).c_str() );
		if(app != -1){
			if(app){
				if(incAndtestUpCntEvnt(0, false, CONDCNT1+mm)){
					pub = setActionLogic(app, nn);
					//legge lo stato finale e lo scrive sulle uscite
					scriviOutDaStato(mm*2);
				}
			}else{
				startCnt(0, 1, CONDCNT1+n);
				pub = setActionLogic(app, nn);
				//legge lo stato finale e lo scrive sulle uscite
				scriviOutDaStato(mm*2);
			}
			readStatesAndPub(nn);
		}
	}
}

inline void testRoller(byte nn){
	//nn: 0,1,2,3 buttons
	int app;
	//modalità tapparella
	//simula pressione di un tasto locale
	app = eval( (static_cast<ParVarStr*>(pars[p(ONCOND1+nn)])->getStrVal()).c_str() );
	if(app > 0 && !isOnTarget(app, 0) && !isrun[0] && !isCalibr()){
		static_cast<ParUint8*>(pars[MQTTUP1+nn])->load((uint8_t) 255);			
		static_cast<ParUint8*>(pars[MQTTUP1+nn])->doaction(0);
	}
}

inline void testValues(byte n){
	//n: number of switches
	if(roll[n] == false){
		//modalità switch generico
		if(incAndtestUpCntEvnt(0,true,SMPLCNT1+n*2)){
			testSwitch(n);
		}
		if(incAndtestUpCntEvnt(0,true,SMPLCNT1+n*2+1)){
			testSwitch(n+1);
		}
		//legge lo stato finale e lo pubblica su MQTT
		//readStatesAndPub();
	}else{
		//modalità tapparella
		//simula pressione di un tasto locale
		testRoller(n*2);
		testRoller(n*2+1);
	}
}

//legge PERIODICAMENTE il parser delle condizioni sui sensori
inline void leggiTastiLocaliDaExp(){
	//imposta le configurazioni dinamiche in base ad eventi locali valutati periodicamente
	DEBUG1_PRINT(F("Periodic local cmds: "));	
	DEBUG1_PRINTLN( eval( (static_cast<ParVarStr*>(pars[p(ONCOND5)])->getStrVal()).c_str() ) );
	
	testValues(0);
	testValues(1);
}

inline void sensorStatePoll(){
	//sensor variation polling management
	//on events basis push of reports
	
#if (AUTOCAL_HLW8012)
#if (RS485) 
	if(mov){
		if(gatedfn(overallSwPower,GTIPWR, IPWRRND,false)){
			au16data[RDWATH] = l_2uint_int1(asyncBuf[GTIPWR]);
			au16data[RDWATL] = l_2uint_int2(asyncBuf[GTIPWR]);
		}
#else
 	if(mov){//
		gatedfn(overallSwPower,GTIPWR, IPWRRND,false);//only memorization!
#endif		
		//gatedfn(getTemperature(),GTTEMP, TEMPRND);
		//no power feedback to grant more precision into time calculation
		//no voltage measurement to grant more precision into time calculation	
	}else{
		if(incAndtestUpCntEvnt(0,true,ACTPWRCNT) && gatedfn(hlw8012.getActivePower(),GTIPWR, IPWRRND)){
#if (RS485) 
			au16data[RDWATH] = l_2uint_int1(asyncBuf[GTIPWR]);
			au16data[RDWATL] = l_2uint_int2(asyncBuf[GTIPWR]);
#endif
			readIpwrAndPub();
			DEBUG1_PRINT(F("\nPotenza cambiata "));
			DEBUG1_PRINT(au16data[RDWATH]);
			DEBUG1_PRINT(F(", "));
			DEBUG1_PRINT(au16data[RDWATH]);
		}
		if(incAndtestUpCntEvnt(0,true,ACVOLTCNT) && gatedfn(hlw8012.getVoltage(),GTIVAC, IVACRND)){
#if (RS485) 
			au16data[RDVLT] = asyncBuf[GTIVAC];
#endif
			readVacAndPub();
		}
		if(incAndtestUpCntEvnt(0,true,TEMPCNT) && gatedfn(getTemperature(),GTTEMP, TEMPRND)){
#if (RS485) 
			au16data[RDTMPH] = f_2uint_int1(asyncBuf[GTTEMP]);
			au16data[RDTMPL] = f_2uint_int2(asyncBuf[GTTEMP]);
#endif
			readTempAndPub();
			DEBUG1_PRINT(F("\nTemperatura cambiata "));
			DEBUG1_PRINT(au16data[RDTMPH]);
			DEBUG1_PRINT(F(", "));
			DEBUG1_PRINT(au16data[RDTMPL]);
		}
	}
#else
	if(mov){//
		//gatedfn(overallSwPower2,GTIPWR, IPWRRND);//only memorization!
		//gatedfn(getTemperature(),GTTEMP, TEMPRND);
		//no power feedback to grant more precision into time calculation
		//no voltage measurement to grant more precision into time calculation	
	}else{
		if(incAndtestUpCntEvnt(0,true,TEMPCNT) && gatedfn(getTemperature(),GTTEMP, TEMPRND)){
#if (RS485) 
			au16data[RDTMPH] = f_2uint_int1(asyncBuf[GTTEMP]);
			au16data[RDTMPL] = f_2uint_int2(asyncBuf[GTTEMP]);
#endif
			readTempAndPub();
			DEBUG1_PRINT(F("\nTemperatura cambiata"));
		}
	}
#endif	
}

#if (AUTOCAL_ACS712) 
inline void currentPeakDetector(){
	//AC current peak detector
        system_soft_wdt_stop();
        ets_intr_lock( ); //close interrupt
        noInterrupts();
		x = system_adc_read() - m;
		//samples[indx] = system_adc_read();
		//x = samples[indx] - m;	
		(x > maxx) && (maxx = x);
		(x < minx) && (minx = x);
		/*if(x > maxx) 					
		{    							
			maxx = x; 					
		}
		if(x < minx) 					
		{       						
			minx = x;					
		}*/
		//indx++;
		interrupts();
        ets_intr_unlock(); //open interrupt
        system_soft_wdt_restart();
}

void inline zeroAndPwrSampler(){
	//zero detection manager and scheduler
	if(doZeroSampl > 0){
		system_soft_wdt_stop();
		ets_intr_lock( ); //close interrupt
		noInterrupts();
		x = system_adc_read();
		interrupts();
		ets_intr_unlock(); //open interrupt
		system_soft_wdt_restart();
		//running mean calculation
		smplcnt++;
		smplcnt && (m += (float) (x - m) / smplcnt);  //protected against overflow by a logic short circuit
		DEBUG2_PRINTLN(F("Zero detection manager"));	
		DEBUG2_PRINT(F("doZeroSampl: "));	
		DEBUG2_PRINT(doZeroSampl);	
		DEBUG2_PRINT(F(", dosmpl: "));							
		DEBUG2_PRINT(dosmpl);	
		DEBUG2_PRINT(F(", x: "));	
		DEBUG2_PRINT(x);	
		DEBUG2_PRINT(F(", smplcnt: "));	
		DEBUG2_PRINTLN(smplcnt);
		--doZeroSampl;
	}
	if(doPwrSampl > 0){
		dd = maxx - minx;
		DEBUG2_PRINT(F("doPwrSampl: "));	
		DEBUG2_PRINT(doPwrSampl);	
		DEBUG2_PRINT(F(", dosmpl: "));	
		DEBUG2_PRINT(dosmpl);	
		DEBUG2_PRINT(F(", dd: "));	
		DEBUG2_PRINT(dd);
		DEBUG2_PRINT(F(", m2: "));	
		DEBUG2_PRINT(m2);
		DEBUG2_PRINT(F(", smplcnt2: "));	
		DEBUG2_PRINTLN(smplcnt2);
		
		smplcnt2++;
		smplcnt2 && (m2 += (float) (dd- m2) / smplcnt2);
		minx = 1024;
		maxx = 0;
		--doPwrSampl;
		overallSwPower = m2;
		sampleOne(m2);
	}else if(doPwrSampl == 0){
		dosmpl = false;
		overallSwPower = m2;
		sampleOne(m2);
		m2 = 0;
		smplcnt2 = 0;
		--doPwrSampl;
		DEBUG2_PRINT(F(", final dd: "));	
		DEBUG2_PRINT(dd);
		DEBUG2_PRINT(F(", overallSwPower: "));	
		DEBUG2_PRINT(overallSwPower);
		DEBUG2_PRINT(F(", final smplcnt2: "));	
		DEBUG2_PRINTLN(smplcnt2);
	}
}	
#endif

inline void sampleOne(float pwr){
	if(roll[1] && roll[0] == 0){
		//if(out[0] && (out[1] + out[2] + out[3]) == 0)
		(out[0] && out[1] == 0) && (outPwr[0] = pwr);
		//if(out[1] && (out[0] + out[2] + out[3]) == 0)
		(out[1] && out[0] == 0) && (outPwr[1] = pwr);
		//(out[0] == 0 && out[1] == 0) && (overallSwPower = 0);
	}else if(roll[0] && roll[1] == 0){
		//if(out[2] && (out[0] + out[1] + out[3]) == 0)
		(out[2] && out[3] == 0) && (outPwr[2] = pwr);
		//if(out[3] && (out[0] + out[1] + out[2]) == 0) 
		(out[3] && out[2] == 0) && (outPwr[3] = pwr);
		//(out[2] == 0 && out[3] == 0) && (overallSwPower = 0);
	}
}

inline float getMotorPower(){
#if (AUTOCAL_ACS712) 
	//potenza netta motore
	dd = maxx - minx - overallSwPower;
	minx = 1024;
	maxx = 0;
#elif (AUTOCAL_HLW8012) 
	//potenza lorda motore + luci
	overallSwPower2 = hlw8012.getExtimActivePower();
	//potenza netta motore
	dd = overallSwPower2 - overallSwPower;
#endif
return dd;
}

inline void pwrSampler(){//only if !mov
#if (AUTOCAL_HLW8012) 
	if(out[0] + out[1] + out[2] + out[3] != 0){
		overallSwPower = hlw8012.getExtimActivePower();
	}else{
		overallSwPower = 0;
	}
	
	sampleOne(overallSwPower);
#elif (AUTOCAL_ACS712) 
	if(out[0] + out[1] + out[2] + out[3] != 0){
		dosmpl = true;
		doZeroSampl = -1;
		doPwrSampl = NPWRSMPL;
	}else{
		dosmpl = false;
		doPwrSampl = -1;
		doZeroSampl = NZEROSMPL;
		overallSwPower = 0;
	}
	DEBUG2_PRINT(F("getMotorPower: doPwrSampl: "));	
	DEBUG2_PRINT(doPwrSampl);
	DEBUG2_PRINT(F(", getMotorPower: doZeroSampl: "));	
	DEBUG2_PRINT(doZeroSampl);
	DEBUG2_PRINT(F(", dosmpl: "));	
	DEBUG2_PRINT(dosmpl);
#endif
	DEBUG2_PRINT(F(", OverallSwPower: "));	
	DEBUG2_PRINT(overallSwPower);	
	DEBUG2_PRINT(F(", pwr1: "));	
	DEBUG2_PRINT(outPwr[0]);
	DEBUG2_PRINT(F(", pwr2: "));	
	DEBUG2_PRINT(outPwr[1]);
	DEBUG2_PRINT(F(", pwr3: "));	
	DEBUG2_PRINT(outPwr[2]);
	DEBUG2_PRINT(F(", pwr4: "));	
	DEBUG2_PRINTLN(outPwr[3]);
}

void onSWStateChange(uint8_t nn){
	//nn: 0,1,2,3
	int n = nn / TIMERDIM;
	//int sw = nn % TIMERDIM;
	DEBUG2_PRINT(F("onSWStateChange nn: "));
	DEBUG2_PRINT(nn);
	DEBUG2_PRINT(F(", n: "));
	DEBUG2_PRINT(n);

	if(roll[n] == 0){
		if(out[nn] == HIGH){
			DEBUG2_PRINT(F(", out: "));
			DEBUG2_PRINT(out[nn]);
			//OFF --> ON
			if(mov){
				//modifica stima potenza tapparella solo se questa è in movimento
				overallSwPower += outPwr[nn]; //add previously sampled value
			}
			DEBUG2_PRINT(F(", out HIGH corrected overallSwPower: "));
			DEBUG2_PRINTLN(overallSwPower);
		}else if(mov){
			//modifica stima potenza tapparella solo se questa è in movimento
			//ON --> OFF
			overallSwPower -= outPwr[nn]; //subtract previously sampled value
			DEBUG2_PRINT(F(", mov corrected overallSwPower: "));
			DEBUG2_PRINTLN(overallSwPower);
		}
		//unsigned short p = (packetBuffer[1] << 8) | packetBuffer[2];
	}
	
}

#if (ROLLER)
inline void checkStop(byte mm){
//mm: 0,1 switches
byte kk = mm * 2; //0, 2

#if (AUTOCAL_ACS712) 
	//doZeroSampl = -1;
	//doPwrSampl = -1;
	if(isrun[mm] && dosmpl){
#elif (AUTOCAL_HLW8012) 
	if(isrun[mm]){
#endif
		DEBUG2_PRINT(mm);
		if(isrundelay[mm] == 0){
			ex[mm] = dd*EMA + (1.0 - EMA)*ex[mm];
			DEBUG2_PRINT(F("\n("));
			DEBUG2_PRINT(mm);
#if (AUTOCAL_ACS712) 
			DEBUG2_PRINT(F(") minx sensor: "));
			DEBUG2_PRINT(minx);
			DEBUG2_PRINT(F(" - maxx sensor: "));
			DEBUG2_PRINT(maxx);
			DEBUG2_PRINT(F(" - Mean sensor: "));
			DEBUG2_PRINT(m);
#endif
			DEBUG2_PRINT(F(" -rundelay: "));
			DEBUG2_PRINT(isrundelay[mm]);
			DEBUG2_PRINT(F(" - EMA: "));
			DEBUG2_PRINT(ex[mm]);
			DEBUG2_PRINT(F(" - Inst: "));
			DEBUG2_PRINT(dd);
			DEBUG2_PRINT(F(" - weight[1]: "));
			DEBUG2_PRINT(weight[1]);
			chk[mm] = checkRange((double) ex[mm]*(1 - weight[(mm+1)%2]*isMoving((mm+1)%2)),mm);
			//chk[0] = checkRange((double) ex[0],0);
			if(chk[mm] != 0){
				DEBUG2_PRINT(F("\n("));
				DEBUG2_PRINT(mm);
				if(chk[mm] == -1){
					DEBUG2_PRINTLN(F(") Stop: sottosoglia"));
					//fine corsa raggiunto
					blocked[mm] = secondPress(mm,halfStop,true);
					//scriviOutDaStato(mm);
				}else if(chk[mm] == 2){
					DEBUG2_PRINTLN(F(") Stop: soprasoglia"));
					blocked[mm] = secondPress(mm,halfStop);
					//scriviOutDaStato(mm);
					blocked[mm] = 1;
				}else if(chk[mm] == 1){
					ex[mm] = getAVG(mm);
					DEBUG2_PRINTLN(F(") Start: fronte di salita"));					
					//inizio conteggio timer di posizionamento
					startEndOfRunTimer(mm);
					DEBUG2_PRINT(F("Timer lapse: "));		
					DEBUG2_PRINTLN(getTimerLapse(kk));
					readStatesAndPub(kk); //partenza con segnalazione completa
				}
				
				//ex[0] = getAVG(0);
				yield();
			}
		}else{
			DEBUG2_PRINT(F("\n("));
			DEBUG2_PRINT(mm);
			DEBUG2_PRINT(F(") aspetto: "));
			DEBUG2_PRINT(isrundelay[mm]);
#if (AUTOCAL_ACS712) 
			DEBUG2_PRINT(F(" - minx sensor: "));
			DEBUG2_PRINT(minx);
			DEBUG2_PRINT(F(" - maxx sensor: "));
			DEBUG2_PRINT(maxx);
#endif
			DEBUG2_PRINT(F(" - Peak: "));
			DEBUG2_PRINT(ex[mm]);
			isrundelay[mm]--;
			ex[mm] = dd;
			DEBUG2_PRINT(F(" - dd: "));
			DEBUG2_PRINT(dd);
		}
	}else{
		isrundelay[mm] = RUNDELAY;
		//reset dei fronti su blocco marcia (sia manuale che automatica) 
		resetEdges(mm);
	}
}

inline void automaticStopManager(){
	if(mov){ //Solo modalità tapparella! Sempre falso se si è in modalità switch!	
			getMotorPower();
			DEBUG2_PRINT(F(" \nOverallSwPower: "));
			DEBUG2_PRINT(overallSwPower);
			//EMA calculation
			//ACSVolt = (double) ex/2.0;
			//peak = (double) ex/2.0;
			//reset of peak sample value
			DEBUG2_PRINT(" Isrun: ");
			DEBUG2_PRINT(isrun[0]);
			DEBUG2_PRINT(", dosmpl: ");
			DEBUG2_PRINT(dosmpl);
			DEBUG2_PRINT(", dd: ");
			DEBUG2_PRINTLN(dd);
			
			checkStop(0); //switch 1
			checkStop(1); //switch 2		

			//AC peak measure init
			//indx = 0;
#if (AUTOCAL_ACS712) 
			dosmpl = true;
#endif
			//DEBUG2_PRINT(F("\n------------------------------------------------------------------------------------------"));
		}else{
			isrundelay[0] = isrundelay[1] = RUNDELAY;
			//reset dei fronti su blocco marcia (sia manuale che automatica)
			resetEdges(0); //switch 1
			resetEdges(1); //switch 2	
			//overallSwPower = hlw8012.getExtimActivePower();
#if (AUTOCAL_ACS712) 
			if(pageLoad == false)
				zeroAndPwrSampler();
#endif
		}
}
#endif

inline void wifiFailoverManager(){
	//wifi failover management
	//DEBUG1_PRINTLN(F("WifiFailoverManager"));
	//DEBUG1_PRINTLN(wl_status_to_string(wfs));
	if(WiFi.getMode() == WIFI_OFF || WiFi.getMode() == WIFI_STA || WiFi.getMode() == WIFI_AP_STA){
		if(wifiConn == false){
			if(wificnt > WIFISTEP){
				if(lastWifiConn){
					lastWifiConn = false;
					DEBUG1_PRINTLN(F("Stop websocket"));
					webSocket.close();
				}
				//lampeggia led di connessione
				ledState[0] = !ledState[0];
				#if (MCP2317) 
					//mcp.digitalWrite(BLUE, !mcp.digitalRead(BLUE));
					mcp.digitalWrite(BLUE, ledState[0]); 
					DEBUG1_PRINT(F("r: ")); DEBUG1_PRINTLN(mcp.digitalRead(RED));
					DEBUG1_PRINT(F("g: ")); DEBUG1_PRINTLN(mcp.digitalRead(GREEN));
					DEBUG1_PRINT(F("b: ")); DEBUG1_PRINTLN(mcp.digitalRead(BLUE));
				#else
					//digitalWrite(OUTSLED, !digitalRead(OUTSLED));
					digitalWrite(OUTSLED, ledState[0]); 
				#endif			//yield();
				DEBUG1_PRINT(F("\nSwcount roll: "));
				DEBUG1_PRINTLN(swcount);
				if((swcount == 0)){
					DEBUG1_PRINTLN(F("Connection timed out"));
					WiFi.persistent(false);
					WiFi.disconnect(true);
					WiFi.mode(WIFI_OFF);    
					DEBUG1_PRINTLN(F("Do new connection"));
					WiFi.setAutoReconnect(true);	
					WiFi.mode(WIFI_STA);
					WiFi.setAutoReconnect(true);
					WiFi.config(0U, 0U, 0U);
					setup_wifi(wifindx);	//tetativo di connessione
					//wifi_station_dhcpc_start();
					//WiFi.waitForConnectResult();	//non necessaria se il loop ha sufficienti slot di idle!
					stat = WiFi.status();
					wifiConn = (stat == WL_CONNECTED);
					wifindx = (wifindx +1) % 2; //0 or 1 are the index of the two alternative SSID
					//if(wifiConn)
					//	mqttReconnect();
				}
				swcount = (swcount + 1) % TCOUNT;
			}else{
				DEBUG1_PRINTLN(F("\nGiving time to ESP stack... "));
				delay(30);//give 30ms to the ESP stack for wifi connect
				//------------------------------------------------------------------------------------------------------------
				//Finestra idle di riconnessione (necessaria se il loop � molto denso di eventi e il wifi non si aggancia!!!)
				//------------------------------------------------------------------------------------------------------------
				//sostituisce la bloccante WiFi.waitForConnectResult();	
				//------------------------------------------------------------------------------------------------------------
			}
			wificnt++;
		}else{
			wificnt = 0;
			if(lastWifiConn == false){
				lastWifiConn = true;
				int i = 0;
				IPAddress ip = WiFi.localIP();
				ip.toString().toCharArray(IP, 16);
				ipSet = strcmp(IP, "(IP unset)") != 0;
				while(!ipSet && i<3){
					delay(10);
					IPAddress ip = WiFi.localIP();
					ip.toString().toCharArray(IP, 16);
					ipSet = strcmp(IP, "(IP unset)") != 0;
					DEBUG1_PRINT(F("Candidate IP address = "));
					DEBUG1_PRINTLN(IP);
					++i;
				};
				pars[p(LOCALIP)]->load(IP);
				DEBUG1_PRINT(F("AP client IP address = "));
				DEBUG1_PRINTLN(((ParStr32*)pars[p(LOCALIP)])->val);
				DEBUG1_PRINTLN(F("Start websocket"));
				startWebSocket();
				wificnt = 0;
				setColor(1); //BLUE ON
#if (MCP2317) 
				//mcp.digitalWrite(BLUE, LOW); //pullup!
#else
				digitalWrite(OUTSLED, LOW); 
#endif
			}	
		}
	}
}

inline void MQTTReconnectManager(){
	//MQTT reconnect management
			//a seguito di disconnessioni accidentali tenta una nuova procedura di riconnessione		
			
			if((wifiConn == true)&&(!mqttConnected) && WiFi.status()==WL_CONNECTED && WiFi.getMode()==WIFI_STA) {
				mqttcnt++; //messo a zero da evento onConnected
				
				ledState[1] = !ledState[1];
#if (MCP2317) 
				mcp.digitalWrite(GREEN, ledState[1]);
			    DEBUG1_PRINT(F("r: ")); DEBUG1_PRINTLN(mcp.digitalRead(RED));
			    DEBUG1_PRINT(F("g: ")); DEBUG1_PRINTLN(mcp.digitalRead(GREEN));
				DEBUG1_PRINT(F("b: ")); DEBUG1_PRINTLN(mcp.digitalRead(BLUE));
#endif
				DEBUG1_PRINT(F("ERROR! MQTT client is not connected: "));
				//DEBUG1_PRINT(F("mqttConnected: "));
				DEBUG1_PRINTLN(mqttConnected);
				
				if(mqttClient==NULL){
					DEBUG1_PRINTLN(F("ERROR! MQTT client is not allocated: doing reconnect..."));
					mqttReconnect();
					mqttcnt = 0;
					mqttofst = 4;
				}else{ 
					
					if(mqttcnt <= 8){
						DEBUG2_PRINTLN(F("ERROR! MQTT client is not connected: 4."));
						mqttofst = 4;
					}if(mqttcnt <= 16){
						DEBUG2_PRINTLN(F("ERROR! MQTT client is not connected: 8."));
						mqttofst = 8;
					}else if(mqttcnt <= 32){
						DEBUG2_PRINTLN(F("ERROR! MQTT client is not connected: 16."));
						mqttofst = 16;
					}else if(mqttcnt <= 128){
						DEBUG2_PRINTLN(F("ERROR! MQTT client is not connected: 32."));
						mqttofst = 32;
					}else if(mqttcnt <= 256){
						DEBUG2_PRINTLN(F("ERROR! MQTT client is not connected: 64."));
						mqttofst = 64;
					}else if(mqttcnt <= 512){
						DEBUG2_PRINTLN(F("ERROR! MQTT client is not connected: 128."));
						mqttofst = 128;
					}else{ 
						DEBUG2_PRINTLN(F("ERROR! MQTT client is not connected: 256."));
						mqttofst = 256;
					}
					
					if(!(mqttcnt % mqttofst)){
						//non si può fare perchè dopo pochi loop crasha
						if(dscnct){
							dscnct=false;
							DEBUG1_PRINT(F("eseguo la MQTT connect()...Cnt: "));
							ESP.wdtFeed();
							//ESP.wdtDisable(); 
							//ets_intr_lock(); 
							//noInterrupts ();
							mqttClient->connect();
							//interrupts ();
							//ets_intr_unlock();
							//ESP.wdtEnable(0); 
							//ESP.wdtFeed();
							delay(0);
						}
						else
						{
							dscnct=true;
							DEBUG1_PRINT(F("eseguo la MQTT disconnect()...Cnt: "));
							//noInterrupts ();
							//delay(30);
							ESP.wdtFeed();
							//ESP.wdtDisable(); 
							//ets_intr_lock(); 
							//noInterrupts ();
							mqttClient->disconnect();
							//yield();
							//interrupts ();
							//ets_intr_unlock();
							//ESP.wdtEnable(0); 
							//ESP.wdtFeed();
							//interrupts ();
							delay(0);
						}
						DEBUG1_PRINT(mqttcnt);
						DEBUG1_PRINT(F(" - Passo: "));
						DEBUG1_PRINTLN(mqttofst);
					}
					//non si può fare senza disconnect perchè dopo pochi loop crasha
					//mqttClient->setUserPwd((confcmd[MQTTUSR]).c_str(), (confcmd[MQTTPSW]).c_str());
					
				}
			}
}

//-----------------------------------------------INIZIO TIMER----------------------------------------------------------------------
//azione da compiere allo scadere di uno dei timer dell'array	
void onElapse(uint8_t nn, unsigned long tm){
	//nn: 0,1,2,3
	int n = nn / TIMERDIM;
	int sw = nn % TIMERDIM;
	
	DEBUG2_PRINT(F("\nElapse timer n: "));
	DEBUG2_PRINT(nn);
	DEBUG2_PRINT(F("  al tempo del timer: "));
	DEBUG2_PRINT(tm);
	DEBUG2_PRINT(F("  al tempo del cronometro: "));
	DEBUG2_PRINT(getCronoCount(n));
	DEBUG2_PRINT(F("  con stato: "));
	DEBUG2_PRINT(getGroupState(nn));
	DEBUG2_PRINT(F("  con n: "));
	DEBUG2_PRINT(n);
	DEBUG2_PRINT(F("  con count value: "));
	DEBUG2_PRINTLN(getCntValue(nn));
	DEBUG2_PRINTLN(F("-----------------"));
	DEBUG2_PRINTLN(getCntValue(0));
	DEBUG2_PRINTLN(getCntValue(1));
	DEBUG2_PRINTLN(getCntValue(2));
	DEBUG2_PRINTLN(getCntValue(3));
	DEBUG2_PRINTLN(F("-----------------"));
	
	if(nn != RESETTIMER || nn != APOFFTIMER) //se è scaduto il timer di attesa o di blocco  (0,1) --> state n
	{   
		DEBUG2_PRINT(F("\nCount value: "));
		DEBUG2_PRINTLN(getCntValue(nn));
		if(getCntValue(nn) == 1){ 
			if(roll[n]){//se è in modalità tapparella!
#if (ROLLER)
				//partenza tapparella (no sensore) o blocco tapparella (slider)
				uint8_t rngg = doSwitchTimerTransition(nn);
				if(rngg==1)
					readStatesAndPub(nn,false,true); //partenza motore ma non partenza tapparella
				else if(rngg==1)
					readStatesAndPub(nn); //partenza tapparella
#endif				
			}else{//se è in modalità switch
				doToggleTimerTransition(nn);
				//pubblica lo stato finale su MQTT (non lo fa il loop stavolta!)
				readStatesAndPub(nn);
			}
			//comanda gli attuatori per fermare (non lo fa il loop stavolta!)
			scriviOutDaStato(n);
			
			///////Reset count///////
			resetCnt(nn); //20/10/19 Deve essere alla fine casomai qualcuno prima lo modificasse...
		}else if(getCntValue(nn) > 1){ //in tutte le modalit�
			if(n == 0){
				DEBUG2_PRINTLN(F("onElapse:  timer 1 dei servizi a conteggio scaduto"));
				if(getCntValue(nn)==3){
					//DEBUG2_PRINT(F("Resettato contatore dei servizi: "));
					resetCnt(nn);
					wifiState = WIFIAP;
					//wifi_station_dhcpc_stop();
					//WiFi.enableAP(true);
					//wifi_softap_dhcps_start();
					DEBUG2_PRINTLN(F("-----------------------------"));
					DEBUG2_PRINTLN(F("Attivato AP mode"));
					DEBUG2_PRINTLN(F("-----------------------------"));
					startTimer(APOFFTIMER);
					//WiFi.enableSTA(false);
					DEBUG2_PRINTLN(F("AP mode on"));
					//WiFi.setAutoConnect(false);
					//WiFi.setAutoReconnect(false);	
					//ETS_UART_INTR_DISABLE();
					if(!WiFi.isConnected()){
						// disconnect sta, start ap
						WiFi.persistent(false);      
						WiFi.disconnect();  //cancella la connessione corrente memorizzata
						WiFi.mode(WIFI_AP);
						WiFi.persistent(true);
						DEBUG1_PRINTLN(F("SET AP"));
						wifiConn = false;
					}else{
						WiFi.mode(WIFI_AP_STA);
						DEBUG1_PRINTLN(F("SET AP STA"));
					}
					setup_AP(true);
					delay(500);
					yield();
					//WiFi.persistent(true);
					//setup_AP(true);
					//WiFi.printDiag(Serial);
					//wifi_station_dhcpc_stop();
					//interrupts ();
					//WiFi.enableAP(true); //is STA + AP
					//setup_AP();
					//confcmd[MQTTIP] = WiFi.softAPIP().toString();
					//MDNS.notifyAPChange()();
					//wifi_softap_dhcps_start();
					//delay(100);
					//WiFi.printDiag(Serial);
					//MDNS.update();
					setGroupState(0,n%2*2);
				}else if(getCntValue(nn)==7){
					//DEBUG2_PRINT(F("Resettato contatore dei servizi: "));
					resetCnt(nn);
					DEBUG2_PRINTLN(F("-----------------------------"));
					DEBUG1_PRINTLN(F("Rebooting ESP without reset of configuration"));
					DEBUG2_PRINTLN(F("-----------------------------"));
					ESP.eraseConfig(); //do the erasing of wifi credentials
					ESP.restart();
				}else if(getCntValue(nn)==5 && roll[n]){ //solo in modalit� tapparella!
#if (ROLLER)					
					//DEBUG2_PRINT(F("Resettato contatore dei servizi: "));
					resetCnt(nn);
					DEBUG2_PRINTLN(F("-----------------------------"));
					DEBUG1_PRINTLN(F("ATTIVATA CALIBRAZIONE MANUALE BTN 1"));
					DEBUG2_PRINTLN(F("-----------------------------"));
					manualCalibration(0); //BTN1
#endif
				}else if(getCntValue(nn)==9){
					//DEBUG2_PRINT(F("Resettato contatore dei servizi: "));
					resetCnt(nn);
					DEBUG2_PRINTLN(F("-----------------------------"));
					DEBUG1_PRINTLN(F("Reboot ESP with reset of configuration"));
					DEBUG2_PRINTLN(F("-----------------------------"));
					rebootSystem();
				}
#if (AUTOCAL_HLW8012) 
				else if(getCntValue(nn)==11){
					//DEBUG2_PRINT(F("Resettato contatore dei servizi: "));
					resetCnt(nn);
					DEBUG2_PRINTLN(F("-----------------------------"));
					DEBUG1_PRINTLN(F("Do power calibration"));
					DEBUG2_PRINTLN(F("-----------------------------"));
					calibrate_pwr();
					saveSingleConf(PWRMULT);
					saveSingleConf(CURRMULT);
					saveSingleConf(VACMULT);
					readPwrCalAndPub();
				}
#endif				
				else{
					//DEBUG2_PRINT(F("Resettato contatore dei servizi: "));
					resetCnt(nn);
					setGroupState(0,nn);
				}
			}else if(roll[n]){ //solo in modalità tapparella!
#if (ROLLER)
				DEBUG1_PRINTLN(F("onElapse:  timer 2 dei servizi a conteggio scaduto"));
				if(getCntValue(CNTSERV3)==5){
					//DEBUG2_PRINT(F("Resettato contatore dei servizi: "));
					resetCnt(nn);
					DEBUG2_PRINTLN(F("-----------------------------"));
					DEBUG1_PRINTLN(F("ATTIVATA CALIBRAZIONE MANUALE BTN 2"));
					DEBUG2_PRINTLN(F("-----------------------------"));
					manualCalibration(1); //BTN2
				}else{
					setGroupState(0,nn);
				}
#endif
			}
		}
		///////Reset count///////
		//25/10/2019
		resetCnt(nn);
	}else if(nn == RESETTIMER)
		{
			rebootSystem();
	}else if(nn == APOFFTIMER){
		if(WiFi.softAPgetStationNum() == 0){
			DEBUG1_PRINTLN(F("WIFI: reconnecting to AP"));
			uint8_t stat = WiFi.status();
			wifiConn = (stat == WL_CONNECTED);	
			WiFi.mode(WIFI_STA);	
			wifiState = WIFISTA;
			setup_wifi(wifindx);
			//mqttReconnect();
			//wifi_station_dhcpc_start();
			DEBUG2_PRINTLN(F("-----------------------------"));
			DEBUG1_PRINTLN(F("Nussun client si è ancora connesso, disatttivato AP mode"));
			DEBUG2_PRINTLN(F("-----------------------------"));
		}
		//setGroupState(0,n%2);				 								//stato 0: il motore va in stato fermo
		DEBUG1_PRINTLN(F("stato 0: il motore va in stato fermo da stato configurazione"));
	}
	//DEBUG2_PRINTLN(F("Fine timer"));
}

void onTapStop(uint8_t n){
	uint8_t toffset=n*TIMERDIM;
	//comanda gli attuatori per fermare (non lo fa il loop stavolta!)
	scriviOutDaStato(n);
	//pubblica lo stato di UP o DOWN attivo su MQTT (non lo fa il loop stavolta!)
	readStatesAndPub(toffset);
#if (AUTOCAL)
	resetStatDelayCounter(n);
#endif
}



#if (ROLLER)		
void onCalibrEnd(unsigned long app, uint8_t n){
	//n: 0,1
	static_cast<ParLong*>(pars[p(THALT1 + n)])->load(app);
	//initTapparellaLogic(in,inr,outLogic,(confcmd[THALT1]).toInt(),(confcmd[THALT2]).toInt(),(confcmd[STDEL1]).toInt(),(confcmd[STDEL2]).toInt(),BTNDEL1,BTNDEL2);
	setTapThalt(app, n);
#if (RS485) 
	au16data[RDSP1L + n/2] = l_2uint_int1(app);
    au16data[RDSP1H + n/2] = l_2uint_int2(app);
#endif
	DEBUG2_PRINTLN(F("-----------------------------"));
#if (AUTOCAL)
	calAvg[n] = getAVG(n);
	weight[0] = (double) calAvg[0] / (calAvg[0] +  calAvg[1]);
	weight[1] = (double) calAvg[1] / (calAvg[0] +  calAvg[1]);
	static_cast<ParFloat*>(pars[p(VALWEIGHT)])->load(weight[0]);
	updateUpThreshold(n);
	//confcmd[TRSHOLD1 + n] = String(getThresholdUp(n));
	//setThresholdUp((confcmd[TRSHOLD1 + n]).toFloat(), n);
	saveSingleConf(VALWEIGHT);
	DEBUG2_PRINT(F("Modified current weight "));
	DEBUG2_PRINTLN(static_cast<ParFloat*>(pars[p(VALWEIGHT)])->getStrVal());
#if (AUTOCAL_HLW8012) 	
	/*if(n == 0){
		calibrate_pwr(getAVG(n));
		saveSingleConf(PWRMULT);
		saveSingleConf(CURRMULT);
		saveSingleConf(VACMULT);
		readPwrCalAndPub();
	}*/
#endif
#endif
	saveSingleConf(THALT1 + n);
	DEBUG2_PRINT(F("Modified THALT "));
	DEBUG2_PRINTLN(THALT1 + n);
	DEBUG2_PRINT(F(": "));
	DEBUG2_PRINTLN(static_cast<ParLong*>(pars[p(THALT1 + n)])->getStrVal());
}

void manualCalibration(uint8_t btn){
	setGroupState(0,btn);	
	//activate the learning of the running statistics
	//setStatsLearnMode();
#if (AUTOCAL)
	//resetStatDelayCounter(btn);
	disableUpThreshold(btn);
#endif
	
    //printIn();
	//printDfn();
	//printOutlogic();
	
	//DEBUG1_PRINTLN(getCntValue(0));
	//DEBUG1_PRINTLN(getCntValue(1));
	//DEBUG1_PRINTLN(getCntValue(2));
	//DEBUG1_PRINTLN(getCntValue(3));
		
	DEBUG2_PRINTLN(F("-----------------------------"));
	DEBUG1_PRINT(F("FASE 1 CALIBRAZIONE MANUALE BTN "));
	DEBUG1_PRINTLN(btn+1);
	DEBUG2_PRINTLN(F("-----------------------------"));
	//-------------------------------------------------------------
	DEBUG2_PRINTLN(F("LA TAPPARELLA STA SCENDENDO......"));
	DEBUG1_PRINT(F("PREMERE UN PULSANTE QUALSIASI DEL GRUPPO "));
	DEBUG1_PRINTLN(btn+1);
	DEBUG2_PRINTLN(F("-----------------------------"));
	
	//set initial power balancement extimation
	setValweight(0.5);
	static_cast<ParUint8*>(pars[BTN2IN + btn*BTNDIM])->load((uint8_t) 101);			//codice comando attiva calibrazione
	static_cast<ParUint8*>(pars[BTN2IN + btn*BTNDIM])->doaction(0);
}
#endif

void rebootSystem(){
	EEPROM.begin(FIXEDPARAMSLEN);
	alterEEPROM();
	EEPROM.end();
	DEBUG1_PRINTLN(F("Resetting ESP"));
	ESP.eraseConfig(); //do the erasing of wifi credentials
	ESP.restart();
}
//----------------------------------------------------FINE TIMER----------------------------------------------------------------------
void onStationConnected(const WiFiEventSoftAPModeStationConnected& evt) {
	DEBUG1_PRINT(F("\nAP mode: Station connected: "));
	DEBUG1_PRINTLN(macToString(evt.mac));
	if(WiFi.softAPgetStationNum() == 1){
		boot = true;
		wifiState = WIFIAP;
	}
}

void onStationDisconnected(const WiFiEventSoftAPModeStationDisconnected& evt) {
	DEBUG1_PRINTLN(F("\nStation disconnected: "));
	DEBUG1_PRINTLN(macToString(evt.mac));
	if(WiFi.softAPgetStationNum() == 0){
		resetTimer(APOFFTIMER);
		DEBUG1_PRINTLN(F("WIFI: reconnecting to AP"));
		uint8_t stat = WiFi.status();
		wifiConn = (stat == WL_CONNECTED);	
		WiFi.mode(WIFI_STA);	
		wifiState = WIFISTA;
		setup_wifi(wifindx);
		//mqttReconnect();
	}
}

String macToString(const unsigned char* mac) {
  char buf[20];
  snprintf(buf, sizeof(buf), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  return String(buf);
}

#if(LARGEFW)
void processCmdRemoteDebug() {
	String lastCmd = telnet.getLastCommand();
	
	if(lastCmd == "showconf"){
		// overall config print
		printConfig();
	}else if(lastCmd == "reboot"){
		// Rebooting ESP without reset of configuration
		DEBUG1_PRINT(F("\nRebooting ESP without reset of configuration"));
		ESP.restart();
	}else if(lastCmd == "reset"){
		//Reboot ESP with reset of configuration
		DEBUG1_PRINT(F("\nReboot ESP with reset of configuration"));
		rebootSystem();
	}else if(lastCmd == "calibrate1"){
#if (ROLLER)
		//ATTIVATA CALIBRAZIONE MANUALE BTN 1
		manualCalibration(0);
#endif
	}else if(lastCmd == "calibrate2"){
#if (ROLLER)
		//ATTIVATA CALIBRAZIONE MANUALE BTN 1
		manualCalibration(1);
#endif
	}else if(lastCmd == "apmodeon"){
		DEBUG1_PRINT(F("\nAtttivato AP mode"));
		startTimer(APOFFTIMER);
	}else if(lastCmd == "scanwifi"){
		//scansione reti wifi disponibili
		scan_wifi();
	}else if(lastCmd == "getip"){
		DEBUG1_PRINT("\nLocal IP: ");
		DEBUG1_PRINTLN((WiFi.localIP()).toString());
	}else if(lastCmd == "getmqttstat"){
		if(!(mqttClient->isConnected())){
			DEBUG1_PRINTLN(F("\nMQTT non connesso."));
		}
		else
		{
			DEBUG1_PRINTLN(F("\nMQTT connesso"));
		}
	}else if(lastCmd == "gettemp"){
		DEBUG1_PRINT(F("\nTemperature: "));
		DEBUG1_PRINT(asyncBuf[GTTEMP]);
	}else if(lastCmd == "getadczero"){
		DEBUG1_PRINT(F("\nMean sensor: "));
		DEBUG1_PRINTLN(m);
	}else if(lastCmd == "getMotorPower"){
		DEBUG1_PRINT(F("\nAvg power: ["));
		DEBUG1_PRINT(asyncBuf[GTMEANPWR1]);
		DEBUG1_PRINT(F(","));
		DEBUG1_PRINT(asyncBuf[GTMEANPWR2]);
		DEBUG1_PRINTLN(F("]"));
		DEBUG1_PRINT(F("\nPeak power: ["));
		DEBUG1_PRINT(asyncBuf[GTPEAKPWR1]);
		DEBUG1_PRINT(F(","));
		DEBUG1_PRINT(asyncBuf[GTPEAKPWR2]);
		DEBUG1_PRINTLN(F("]")); 
	}else if(lastCmd == "getmac"){
		DEBUG1_PRINT(F("\nMAC: "));
		DEBUG2_PRINTLN(WiFi.macAddress());
	}else if(lastCmd == "gettime"){
		DEBUG1_PRINT(F("\nTime (ms): "));
		DEBUG2_PRINTLN(millis());
	}else if(lastCmd == "getmqttid"){
		DEBUG1_PRINT(F("\nMQTT ID: "));
		DEBUG1_PRINTLN(pars[p(MQTTID)]->getStrVal());
	}else if(lastCmd == "testflash"){
		testFlash();
	}else{
		DEBUG1_PRINT(F("\nComandi disponibili: "));
		DEBUG1_PRINT(F("\nshowconf, reboot, reset, calibrate1, calibrate2, apmodeon, scanwifi, getip, getmqttstat, getadczero, gettemp, getMotorPower, getmac, gettime, getmqttid, testflash\n"));
	}
	//telnet.flush();
}
#endif


#if defined (_DEBUG) || defined (_DEBUGR)	
const char* wl_status_to_string(wl_status_t status) {
  switch (status) {
    case WL_NO_SHIELD: return "WL_NO_SHIELD";
    case WL_IDLE_STATUS: return "WL_IDLE_STATUS";
    case WL_NO_SSID_AVAIL: return "WL_NO_SSID_AVAIL";
    case WL_SCAN_COMPLETED: return "WL_SCAN_COMPLETED";
    case WL_CONNECTED: return "WL_CONNECTED";
    case WL_CONNECT_FAILED: return "WL_CONNECT_FAILED";
    case WL_CONNECTION_LOST: return "WL_CONNECTION_LOST";
    case WL_DISCONNECTED: return "WL_DISCONNECTED";
  }
}
#endif

#if defined (_DEBUG) || defined (_DEBUGR)	
void testFlash(){
  uint32_t realSize = ESP.getFlashChipRealSize();
  uint32_t ideSize = ESP.getFlashChipSize();
  FlashMode_t ideMode = ESP.getFlashChipMode();
  char s[100];
  
  sprintf(s,"\nFlash real id:   %08X\n", ESP.getFlashChipId());
  DEBUG1_PRINT(s);
  sprintf(s,"Flash real size: %u uint8_ts\n", realSize);
  DEBUG1_PRINT(s);
  sprintf(s,"Flash ide  size: %u uint8_ts\n", ideSize);
  DEBUG1_PRINT(s);
  sprintf(s,"Flash ide speed: %u Hz\n", ESP.getFlashChipSpeed());
  DEBUG1_PRINT(s);
  sprintf(s,"Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));
  DEBUG1_PRINT(s);

  if (ideSize != realSize) {
    DEBUG1_PRINT("\nFlash Chip configuration wrong!\n");
  } else {
    DEBUG1_PRINT("\nFlash Chip configuration ok.\n");
  }
}
#endif
//-----------------------------------------------------------------------------------------------------------------------------
//richieste da remoto di valori locali
//--------------------------------------------------------------------------------------------------
//gestione eventi MQTT sui sensori (richieste e configurazioni) all'arrivo memorizzate su confcmd[]
//--------------------------------------------------------------------------------------------------
//configurazioni provenienti da remoto
//---------------------------------------------------------------
void MQTTMAC_Evnt::doaction(uint8_t save){
	readMacAndPub();
}
void MQTTIP_Evnt::doaction(uint8_t save){
	readIpAndPub();
}
void MQTTTIME_Evnt::doaction(uint8_t save){
	readTimeAndPub();
}
void MQTTTEMP_Evnt::doaction(uint8_t save){
	readTempAndPub();
}
void MQTTMEANPWR_Evnt::doaction(uint8_t save){
	readAvgPowerAndPub();
}
void MQTTPEAKPWR_Evnt::doaction(uint8_t save){
	readPeakPowerAndPub();
}

#if (AUTOCAL_HLW8012) 
void DOPWRCAL_Evnt::doaction(uint8_t save){
	calibrate_pwr();
	saveSingleConf(PWRMULT);
	saveSingleConf(CURRMULT);
	saveSingleConf(VACMULT);
	readPwrCalAndPub();
}
void INSTPWR_Evnt::doaction(uint8_t save){
	void readIpwrAndPub();
}
void INSTACV_Evnt::doaction(uint8_t save){
	void readVacAndPub();
}
void CALPWR_Evnt::doaction(uint8_t save){
	if(save==1)   
		saveSingleConf(CALPWR);
}
void PWRMULT_Evnt::doaction(uint8_t save){
	if(save==1)   
		saveSingleConf(PWRMULT);
	hlw8012.setPowerMultiplier(static_cast<ParFloat*>(pars[p(PWRMULT)])->val);
		// Show corrected factors
	DEBUG1_PRINT(F("[HLW] New power multiplier   : ")); DEBUG1_PRINTLN(hlw8012.getPowerMultiplier());
}
void CURRMULT_Evnt::doaction(uint8_t save){
	if(save==1)   
		saveSingleConf(CURRMULT);
	hlw8012.setCurrentMultiplier(static_cast<ParFloat*>(pars[p(CURRMULT)])->val);
		// Show corrected factors
	DEBUG1_PRINT(F("[HLW] New current multiplier : ")); DEBUG1_PRINTLN(hlw8012.getCurrentMultiplier());
}
void VACMULT_Evnt::doaction(uint8_t save){
	if(save==1)   
		saveSingleConf(VACMULT);
	hlw8012.setVoltageMultiplier(static_cast<ParFloat*>(pars[p(VACMULT)])->val);
		// Show corrected factors
	DEBUG1_PRINT(F("[HLW] New voltage multiplier : ")); DEBUG1_PRINTLN(hlw8012.getVoltageMultiplier());
}
void ACVOLT_Evnt::doaction(uint8_t save){
	if(save==1)   
		saveSingleConf(ACVOLT);
}
#endif
//----------------------------------------------------------------------------
//richieste da remoto di valori locali
//----------------------------------------------------------------------------
//void BaseEvnt::loadPid(uint8_t id){
//	this->pid = id;
//}
void MQTTBTN_Evnt::doaction(uint8_t save){
	unsigned i = this->pid;
	uint8_t v = static_cast<ParUint8*>(pars[i])->val;
	int n = i / TIMERDIM;
	int sw = i % TIMERDIM;
	
	DEBUG1_PRINT("pid: ");
	DEBUG1_PRINT(i);
	DEBUG1_PRINT(" val: ");
	DEBUG1_PRINT(v);
	in[i] = v;
	static_cast<ParUint8*>(pars[i])->load((uint8_t) 0); //flag reset
	inflag = true;
	DEBUG1_PRINT(" inval: ");
	DEBUG1_PRINTLN(in[i]);
	DEBUG1_PRINTLN("Prima");
	printDfn();
	printOutlogic();
	
	if(roll[n]){
#if (ROLLER)
		if(switchLogic(sw, n, save)){
			//comanda gli attuatori per fermare (non lo fa il loop stavolta!)
			scriviOutDaStato(n);
			//pubblica lo stato finale su MQTT (non lo fa il loop stavolta!)
			readStatesAndPub(i,false,true); //partenza motore ma non partenza tapparella
		}
#endif
	}else{
		if(toggleLogic(sw, n, save)){
			//comanda gli attuatori per fermare (non lo fa il loop stavolta!)
			scriviOutDaStato(n);
			//pubblica lo stato finale su MQTT (non lo fa il loop stavolta!)
			readStatesAndPub(i);
		}
	}
	DEBUG2_PRINTLN("Dopo");
	printDfn();
	printOutlogic();
}

void WIFICHANGED_Evnt::doaction(uint8_t save){	
	wifindx=0;
	DEBUG1_PRINTLN(F("Doing WiFi disconnection"));
	WiFi.persistent(false);
	WiFi.disconnect(false);
	WiFi.mode(WIFI_OFF);    
	//WiFi.mode(WIFI_STA);
	wifindx = 0;
}

//cambio di indirizzo o porta
void MQTTADDR_Evnt::doaction(uint8_t save){
	DEBUG1_PRINT(F("save: "));
	DEBUG1_PRINTLN(save);
	if(save == 2){
		acc = acc || active;
		count++;
		DEBUG1_PRINT(F("count: "));
		DEBUG1_PRINTLN(count);
		if(count >= 2 && acc){
			count = 0;
			if(wifiConn == true){
				DEBUG2_PRINTLN(F("confcmd[MQTTADDRMODFIED] eseguo la reconnect()"));
				mqttReconnect();
			}
		}
	}
}

//cambio di username o psw
void MQTTCONNCHANGED_Evnt::doaction(uint8_t save){
	if(wifiConn == true){
		if(mqttClient==NULL){
			DEBUG2_PRINTLN(F("ERROR confcmd[TOPICCHANGED]! MQTT client is not allocated."));
			mqttReconnect();
			delay(50);
		}else{
			if((wifiConn == true)&& WiFi.status()==WL_CONNECTED && WiFi.getMode()==WIFI_STA){
				if(mqttConnected){
					DEBUG1_PRINT(F("eseguo la MQTT disconnect()...Cnt: "));
					//noInterrupts ();
					mqttClient->disconnect();
					delay(50);
				}
				DEBUG1_PRINTLN(F("MQTTCONNCHANGED! Eseguo la setUserPwd() con usr "));
				DEBUG2_PRINTLN(pars[p(MQTTUSR)]->getStrVal());
				DEBUG2_PRINTLN(F(" e psw "));
				DEBUG2_PRINTLN(pars[p(MQTTPSW)]->getStrVal());
				mqttClient->setUserPwd((const char*)static_cast<ParStr32*>(pars[p(MQTTUSR)])->val, (const char*) static_cast<ParStr32*>(pars[p(MQTTPSW)])->val);
				//////noInterrupts ();
				delay(50);
				DEBUG1_PRINTLN(F("MQTT: Eseguo la re-connect."));
				mqttClient->connect();
				delay(50);
			}
		}
	}
		
}

//cambio di intopic (subscribe)
void MQTTINTOPIC_Evnt::doaction(uint8_t save){	
	if(wifiConn == true && !save){
		if(mqttClient==NULL){
			DEBUG2_PRINTLN(F("ERROR confcmd[INTOPICCHANGED]! MQTT client is not allocated."));
			mqttReconnect();
			delay(50);
		}else if(mqttConnected){
			DEBUG2_PRINTLN(F("MQTTCONNCHANGED! Eseguo la subscribe() con "));
			DEBUG2_PRINTLN(pars[p(MQTTINTOPIC)]->getStrVal());
			DEBUG2_PRINTLN(F("..."));
			mqttClient->subscribe(pars[p(MQTTINTOPIC)]->getStrVal());
			delay(50);
		}
	}
}
void STDELX_Evnt::doaction(uint8_t){
	setSTDelays(static_cast<ParLong*>(pars[p(STDEL1)])->val, static_cast<ParLong*>(pars[p(STDEL2)])->val);
}
#if (ROLLER)
void VALWEIGHT_Evnt::doaction(uint8_t){
	setValweight(static_cast<ParFloat*>(pars[p(VALWEIGHT)])->val);
}
void THICKNESS_Evnt::doaction(uint8_t){
	setThickness(static_cast<ParFloat*>(pars[p(THICKNESS)])->val);
}
void BARRELRAD_Evnt::doaction(uint8_t){
	setBarrRadius(static_cast<ParFloat*>(pars[p(BARRELRAD)])->val);
}
void SLATSRATIO_Evnt::doaction(uint8_t){
	setSLRatio(static_cast<ParFloat*>(pars[p(SLATSRATIO)])->val);
}
void TLENGTH_Evnt::doaction(uint8_t){
	setTapLen(static_cast<ParFloat*>(pars[p(TLENGTH)])->val);
}
#endif
void SWROLL1_Evnt::doaction(uint8_t save){
	if(save==1) 
		saveSingleConf(SWROLL1);	
	setSWMode((uint8_t) static_cast<ParUint8*>(pars[p(SWROLL1)])->val,0); 
}
void SWROLL2_Evnt::doaction(uint8_t save){
	if(save==1) 
		saveSingleConf(SWROLL2);	
	setSWMode((uint8_t) static_cast<ParUint8*>(pars[p(SWROLL2)])->val,0); 
}
void THALTX_Evnt::doaction(uint8_t){
	setTHalts(static_cast<ParLong*>(pars[p(THALT1)])->val, static_cast<ParLong*>(pars[p(THALT2)])->val, static_cast<ParLong*>(pars[p(THALT3)])->val, static_cast<ParLong*>(pars[p(THALT4)])->val);
}
void UTCVAL_Evnt::doaction(uint8_t){
	updateNTP(static_cast<ParLong*>(pars[p(UTCVAL)])->val);
}
void NTPADDR1_Evnt::doaction(uint8_t save){
	if(save==1)   
		saveSingleConf(NTPADDR1);
	setNtpServer(0,static_cast<ParStr64*>(pars[p(NTPADDR1)])->val);
}
void NTPADDR2_Evnt::doaction(uint8_t save){
	if(save==1)   
		saveSingleConf(NTPADDR2);
	setNtpServer(1,static_cast<ParStr64*>(pars[p(NTPADDR2)])->val);
}
void UTCSYNC_Evnt::doaction(uint8_t save){
	if(save==1) 
		setSyncInterval(saveLongConf(UTCSYNC));
	else
		setSyncInterval(static_cast<ParLong*>(pars[p(UTCSYNC)])->val);
}
void UTCADJ_Evnt::doaction(uint8_t save){
	if(save==1) 
		adjustTime(saveIntConf(UTCADJ));
}
void UTCSDT_Evnt::doaction(uint8_t save){
	if(save==1) 
		setSDT(saveByteConf(UTCSDT));
	else
		setSDT(static_cast<ParUint8*>(pars[p(UTCSDT)])->val);
}
void UTCZONE_Evnt::doaction(uint8_t save){
	if(save==1) 
		setTimeZone((int) saveIntConf(UTCZONE));
	else
		setTimeZone((int)static_cast<ParInt*>(pars[p(UTCZONE)])->val);
}
void ACTIONEVAL_Evnt::doaction(uint8_t save){
	if(save==1) 
		//save confs and actions on new action received event
		writeOnOffConditions();
	//run actions one time on new action received event and onload event
	readActionConfAndSet();
}
void ONCOND1_Evnt::doaction(uint8_t save){
	if(save==1) 
		//save confs and actions on new action received event
		writeOnOffConditions();
}
void ONCOND2_Evnt:: doaction(uint8_t save){
	if(save==1)
		//save confs and actions on new config received event	
		writeOnOffConditions();
}
void ONCOND3_Evnt::doaction(uint8_t save){
	if(save==1)
		//save confs and actions on new config received event	
		writeOnOffConditions();
}
void ONCOND4_Evnt::doaction(uint8_t save){
	if(save==1) 
		//save confs and actions on new config received event
		writeOnOffConditions();
}
void ONCOND5_Evnt::doaction(uint8_t save){
	if(save==1) 
		//save confs and actions on new config received event
		writeOnOffConditions();
}
void MQTTID_Evnt::doaction(uint8_t save){
	//feedback buffer init
	strcpy(s,"{\"");
	t = s;
	strcpy(t+=strlen(t),(char*)static_cast<ParStr32*>(pars[p(MQTTID)])->jsoname);
	strcpy(t+=strlen(t),twodot);
	strcpy(t+=strlen(t),(char*)static_cast<ParStr32*>(pars[p(MQTTID)])->val);
	strcpy(t+=strlen(t),"\",\"");
	t+=strlen(t);
	frst = t;
	DEBUG1_PRINTLN("MQTTID_Evnt ");
	DEBUG1_PRINTLN(s);
}
#if(RS485) 
void MBUSVEL_Evnt::doaction(uint8_t save){
	if(save==1)  
		//run actions one time on new action received event
		saveSingleConf(MBUSVEL);
	//run actions one time on new action received event and onload event
	slave.begin(static_cast<ParLong*>(pars[p(MBUSVEL)])->val);
	Serial.begin(static_cast<ParLong*>(pars[p(MBUSVEL)])->val);
	DEBUG1_PRINT(F("New modbus vel : ")); DEBUG1_PRINTLN(static_cast<ParLong*>(pars[p(MBUSVEL)])->val);
}	
void MBUSID_Evnt::doaction(uint8_t save){
	slave.setID(static_cast<ParUint8*>(pars[p(MBUSID)])->val);
	DEBUG1_PRINT(F("New modbus ID : ")); DEBUG1_PRINTLN(slave.getID());
}
#endif	

void LOGSLCT_Evnt::doaction(uint8_t save){
	uint8_t ser, tlnt, mqtt, num;
	
	DEBUG_PRINT("LOGSLCT_Evnt ");
	
	num = static_cast<ParUint8*>(pars[p(LOGSLCT)])->val;
	
	DEBUG_PRINT("saveByteConf ");
	if(save==1) saveByteConf(LOGSLCT);		////
	
	ser = (num >> 0) & 0x3;
	tlnt = (num >> 2) & 0x3;
	mqtt = (num >> 4) & 0x3;
	//1, 0, 2 = 33 (100001)
	//1, 0, 0 = 1 (000001)
	//1, 0, 1 = 17 (010001)
	//0, 0, 1 = 16 (010000)
	//0, 0, 2 = 32 (100000)
	//0, 0, 2 = 2 (00010)
	
	
	//DEBUG_PRINT("ser: ");
	//DEBUG_PRINT(ser);
	//DEBUG_PRINT(", tlnt: ");
	//DEBUG_PRINT(tlnt);
	//DEBUG_PRINT(", mqtt: ");
	//DEBUG_PRINTLN(mqtt);
	
	//DEBUG_PRINTLN("delete ");
	//if(dbg1 != NULL)
		//dbg1->destroy();
		delete dbg1;
	//if(dbg2 != NULL)
		//dbg2->destroy();
		delete dbg2;

	//DEBUG_PRINTLN(F("Oggetti log distrutti "));
	
	dbg1 = NULL;
	dbg2 = NULL;
	
	//DEBUG_PRINTLN(F("Oggetti log resi NULL "));
	
	if(ser > 0 && !tlnt & !mqtt){
		//DEBUG_PRINTLN(F("if(ser && !tlnt & !mqtt)"));
		//00 00 01
		//00 00 10
		if(ser == 1){
			//DEBUG_PRINTLN(F("if(ser == 1) "));
			dbg1 = new SerialLog(1);
			dbg2 = new BaseLog(2);
		}else if(ser == 2){
			//DEBUG_PRINTLN(F("if(ser == 2)"));
			dbg1 = new SerialLog(1);
			dbg2 = new SerialLog(2);
		}
	}else if(!ser && tlnt > 0 & !mqtt){
		//DEBUG_PRINTLN(F("if(!ser && tlnt & !mqtt)"));
		//00 01 00
		//00 10 00
		if(tlnt == 1){
			//DEBUG_PRINTLN(F("iif(tlnt == 1)"));
			dbg1 = new TelnetLog(1, &telnet);
			dbg2 = new BaseLog(2);
		}else if(tlnt == 2){
			//DEBUG_PRINTLN(F("if(tlnt == 2)"));
			dbg1 = new TelnetLog(1, &telnet);
			dbg2 = new TelnetLog(2, &telnet);
		}
	}else if(!ser && !tlnt && mqtt > 0){
		//DEBUG_PRINTLN(F("if(!ser && !tlnt && mqtt)"));
		//01 00 00
		//10 00 00
		if(mqtt == 1){
			//DEBUG_PRINTLN(F("if(mqtt == 1)"));
			dbg1 = new MQTTLog(1, mqttc);
			dbg2 = new BaseLog(2);
		}else if(mqtt == 2){
			//DEBUG_PRINTLN(F("iif(mqtt == 2)"));
			dbg1 = new MQTTLog(1, mqttc);
			dbg2 = new MQTTLog(2, mqttc);
		}
	}else if(ser > 0 && tlnt > 0 & !mqtt){
		//DEBUG_PRINTLN(F("if(ser && tlnt & !mqtt)"));
		serialTelnet(ser, tlnt);
	}else if(ser > 0 && mqtt > 0 & !tlnt){
		//DEBUG_PRINTLN(F("if(ser && mqtt & !tlnt)"));
		serialMQTT(ser, mqtt);
	}else if(tlnt > 0 && mqtt > 0 & !ser){
		//DEBUG_PRINTLN(F("f(tlnt && mqtt & !ser)"));
		telnetMQTT(tlnt, mqtt);
	}else if(tlnt == 2 && mqtt == 2 & ser == 2){
		//DEBUG_PRINTLN(F("if(tlnt == 2 && mqtt == 2 & ser == 2)"));
		dbg1 = new SerialTelnetMQTTLog(2, &telnet, mqttc);
		dbg2 = new SerialTelnetMQTTLog(2, &telnet, mqttc);
	}else if(tlnt == 2 && mqtt > 0 & ser > 0){
		//DEBUG_PRINTLN(F("if(tlnt == 2 && mqtt & ser)"));
		serialMQTT2(ser, mqtt);
	}else if(ser == 2 && tlnt > 0 & mqtt > 0){
		//DEBUG_PRINTLN(F("if(ser == 2 && tlnt & mqtt)"));
		telnetMQTT2(tlnt, mqtt);
	}else if(mqtt == 2 && tlnt > 0 & ser > 0){
		//DEBUG_PRINTLN(F("if(mqtt == 2 && tlnt & ser)"));
		serialTelnet2(ser, tlnt);
	}else{
		dbg1 = new BaseLog(1);
		dbg2 = new BaseLog(2);
	}
}

void serialTelnet(uint8_t ser, uint8_t tlnt){
	if(ser == 1 && tlnt == 1){
		//DEBUG_PRINTLN(F("if(ser == 1 && tlnt == 1)"));
		dbg1 = new SerialTelnetLog(1, &telnet);
		dbg2 = new BaseLog(2);
	}else if(ser == 2 && tlnt == 2){
		//DEBUG_PRINTLN(F("if(ser == 2 && tlnt == 2)"));
		dbg1 = new SerialTelnetLog(1, &telnet);
		dbg2 = new SerialTelnetLog(2, &telnet);
	}else if(ser == 2 && tlnt == 1){
		//DEBUG_PRINTLN(F("iif(ser == 2 && tlnt == 1)"));
		dbg1 = new SerialTelnetLog(1, &telnet);
		dbg2 = new SerialLog(2);
	}else if(ser == 1 && tlnt == 2){
		//DEBUG_PRINTLN(F("if(ser == 1 && tlnt == 2)"));
		dbg1 = new SerialTelnetLog(1, &telnet);
		dbg2 = new TelnetLog(2, &telnet);
	}
}

void telnetMQTT(uint8_t tlnt, uint8_t mqtt){
	if(tlnt == 1 && mqtt == 1){
		//DEBUG_PRINTLN(F("if(tlnt == 1 && mqtt == 1)"));
		dbg1 = new TelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new BaseLog(2);
	}else if(tlnt == 2 && mqtt == 2){
		//DEBUG_PRINTLN(F("if(tlnt == 2 && mqtt == 2)"));
		dbg1 = new TelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new TelnetMQTTLog(2, &telnet, mqttc);
	}else if(tlnt == 2 && mqtt == 1){
		//DEBUG_PRINTLN(F("if(tlnt == 2 && mqtt == 1)"));
		dbg1 = new TelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new TelnetLog(2, &telnet);
	}else if(tlnt == 1 && mqtt == 2){
		//DEBUG_PRINTLN(F("if(tlnt == 1 && mqtt == 2)"));
		dbg1 = new TelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new MQTTLog(2, mqttc);
	}
}

void serialMQTT(uint8_t ser, uint8_t mqtt){
	if(ser == 1 && mqtt == 1){
		//DEBUG_PRINTLN(F("\nif(ser == 1 && mqtt == 1)"));
		dbg1 = new SerialMQTTLog(1, mqttc);
		dbg2 = new BaseLog(2);
	}else if(ser == 2 && mqtt == 2){
		//DEBUG_PRINTLN(F("if(ser == 2 && mqtt == 2)"));
		dbg1 = new SerialMQTTLog(1, mqttc);
		dbg2 = new SerialMQTTLog(2, mqttc);
	}else if(ser == 2 && mqtt == 1){
		//DEBUG_PRINTLN(F("if(ser == 2 && mqtt == 1)"));
		dbg1 = new SerialMQTTLog(1, mqttc);
		dbg2 = new SerialLog(2);
	}else if(ser == 1 && mqtt == 2){
		//DEBUG_PRINTLN(F("if(ser == 1 && mqtt == 2)"));
		dbg1 = new SerialMQTTLog(1, mqttc);
		dbg2 = new MQTTLog(2, mqttc);
	}
}

void serialTelnet2(uint8_t ser, uint8_t tlnt){
	if(ser == 1 && tlnt == 1){
		//DEBUG_PRINTLN(F("if(ser == 1 && tlnt == 1)"));
		dbg1 = new SerialTelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new BaseLog(2);
	}else if(ser == 2 && tlnt == 2){
		//DEBUG_PRINTLN(F("if(ser == 2 && tlnt == 2)"));
		dbg1 = new SerialTelnetLog(1, &telnet);
		dbg2 = new SerialTelnetLog(2, &telnet);
	}else if(ser == 2 && tlnt == 1){
		//DEBUG_PRINTLN(F("if(ser == 2 && tlnt == 1)"));
		dbg1 = new SerialTelnetLog(1, &telnet);
		dbg2 = new SerialLog(2);
	}else if(ser == 1 && tlnt == 2){
		//DEBUG_PRINTLN(F("if(ser == 1 && tlnt == 2)"));
		dbg1 = new SerialTelnetLog(1, &telnet);
		dbg2 = new TelnetLog(2, &telnet);
	}
}

void telnetMQTT2(uint8_t tlnt, uint8_t mqtt){
	if(tlnt == 1 && mqtt == 1){
		//DEBUG_PRINT(F("if(tlnt == 1 && mqtt == 1)"));
		dbg1 = new SerialTelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new BaseLog(2);
	}else if(tlnt == 2 && mqtt == 2){
		//DEBUG_PRINT(F("if(tlnt == 2 && mqtt == 2)"));
		dbg1 = new TelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new TelnetMQTTLog(2, &telnet, mqttc);
	}else if(tlnt == 2 && mqtt == 1){
		//DEBUG_PRINT(F("if(tlnt == 2 && mqtt == 1)"));
		dbg1 = new TelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new TelnetLog(2, &telnet);
	}else if(tlnt == 1 && mqtt == 2){
		//DEBUG_PRINT(F("if(tlnt == 1 && mqtt == 2)"));
		dbg1 = new TelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new MQTTLog(2, mqttc);
	}
}

void serialMQTT2(uint8_t ser, uint8_t mqtt){
	if(ser == 1 && mqtt == 1){
		//DEBUG_PRINT(F("if(ser == 1 && mqtt == 1)"));
		dbg1 = new SerialTelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new BaseLog(2);
	}else if(ser == 2 && mqtt == 2){
		//DEBUG_PRINT(F("if(ser == 2 && mqtt == 2)"));
		dbg1 = new SerialTelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new SerialTelnetMQTTLog(2, &telnet, mqttc);
	}else if(ser == 2 && mqtt == 1){
		//DEBUG_PRINT(F("if(ser == 2 && mqtt == 1)"));
		dbg1 = new SerialTelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new SerialLog(2);
	}else if(ser == 1 && mqtt == 2){
		//DEBUG_PRINT(F("if(ser == 1 && mqtt == 2)"));	
		dbg1 = new SerialTelnetMQTTLog(1, &telnet, mqttc);
		dbg2 = new MQTTLog(2, mqttc);
	}
}

/**
 * Disconnect from the network
 * @param wifioff
 * @return  one value of wl_status_t enum
 */
/*
bool ESP8266WiFiSTAClass::disconnect(bool wifioff) {
    bool ret;
    struct station_config conf;
    *conf.ssid = 0;
    *conf.password = 0;

    ETS_UART_INTR_DISABLE();
    if(WiFi._persistent) {
        wifi_station_set_config(&conf);
    } else {
        wifi_station_set_config_current(&conf);
    }
    ret = wifi_station_disconnect();
    ETS_UART_INTR_ENABLE();

    if(wifioff) {
        WiFi.enableSTA(false);
    }

    return ret;
}*/
/*
  uint8_t ESP8266WiFiSTAClass::waitForConnectResult() {

    //1 and 3 have STA enabled

    if((wifi_get_opmode() & 1) == 0) {

        return WL_DISCONNECTED;

    }

    while(status() == WL_DISCONNECTED) {

        delay(100);

    }

    return status();

	}
	wl_status_t ESP8266WiFiSTAClass::status() {

    station_status_t status = wifi_station_get_connect_status();



    switch(status) {

        case STATION_GOT_IP:

            return WL_CONNECTED;

        case STATION_NO_AP_FOUND:

            return WL_NO_SSID_AVAIL;

        case STATION_CONNECT_FAIL:

        case STATION_WRONG_PASSWORD:

            return WL_CONNECT_FAILED;

        case STATION_IDLE:

            return WL_IDLE_STATUS;

        default:

            return WL_DISCONNECTED;

    }

}

typedef enum { WL_NO_SHIELD = 255, 
// for compatibility with WiFi Shield library WL_IDLE_STATUS = 0, 
WL_NO_SSID_AVAIL = 1, 
WL_SCAN_COMPLETED = 2, 
WL_CONNECTED = 3, 
WL_CONNECT_FAILED = 4,
 WL_CONNECTION_LOST = 5, 
 WL_DISCONNECTED = 6 } wl_status_t;
 
 typedef enum WiFiMode 
{
    WIFI_OFF = 0, WIFI_STA = 1, WIFI_AP = 2, WIFI_AP_STA = 3
} WiFiMode_t;
*/
