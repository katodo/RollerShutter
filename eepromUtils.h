#ifndef __EEPROM_UTILS__
#define __EEPROM_UTILS__
#include <EEPROM.h>

void initEEPROM(int);
bool validateEEPROM();
void alterEEPROM();
int EEPROMReadStr(int, char*,int len = 1000);
int EEPROMWriteStr(int,const char* ,int len = 0);
void EEPROMWriteInt(int, int); 
int EEPROMReadInt(int);
void EEPROMWriteLong(int, long); 
long EEPROMReadLong(int);
void EEPROMWriteFloat(int, float); //4 byte
float EEPROMReadFloat(int); //4 byte

unsigned int f_2uint_int1(float);
unsigned int f_2uint_int2(float);
float f_2uint_float(unsigned int, unsigned int);
unsigned int l_2uint_int1(long);
unsigned int l_2uint_int2(long);
long l_2uint_long(unsigned int, unsigned int);
#endif //EEPROM_UTILS


