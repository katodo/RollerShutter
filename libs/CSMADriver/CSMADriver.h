#ifndef __CSMADRIVER_H__
#define __CSMADRIVER_H__

#include "uart.h"
#include "esp8266_peri.h"
#include "user_interface.h"
#include "uart_register.h"

//states:
#define IDLE				0
#define NO_EMPTY_TXBUFFER	1
#define	NO_BACKOFF			2
#define DEFER				3
#define BACKOFF_STARTED		4
#define BACKOFF_STOPPED 	5
#define TRANSMIT_FRAME		6
#define NO_ACK				7
#define START_TRANSMIT		8
#define DATA_RECEIVED		9
#define ACK_TRANSMIT		10
//protocol parameters
#define RTT 				20 	//microseconds
#define SIFS 				10 	//microseconds
#define DIFS  				50 	//microseconds
#define TXTIMOUT 			500 //microseconds
#define RETRYLIMIT 			7 
#define CWMIN 				31 	//packets
#define CWMAX 				1023//packets
#define INTERPACKETINT 		2 	//msec

#endif //__CSMADRIVER_H__