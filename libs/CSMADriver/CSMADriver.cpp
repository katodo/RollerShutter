#include "CSMADriver.h"

byte txstate, rxstate;
bool mediumFree = true;
bool nav = false;
byte dprcval[2]; 

LOCAL os_timer_t IFS_timer;

bool uart_tx_fifo_empty(const int uart_nr)
{
    return ((USS(uart_nr) >> USTXC) & 0xff) == 0;
}

inline bool switchdfn(byte val, byte n){
	//n: numero di pulsanti
	val = (val>0);								//SEMPLICE!!!!
	bool changed = (val != dprcval[n]);
	dprcval[n] = val;            // valore di val campionato al loop precedente 
	return changed;
}

void timerInit(){
	os_timer_disarm(&IFS_timer);
	//impostazione callback function del timer
	os_timer_setfn(&IFS_timer,	(os_timer_func_t *)txDelayTimerEvent, NULL);
}

startTxDelayTimer(uint32_t delay_us){
	os_timer_disarm(&IFS_timer);
	os_timer_arm_us(&IFS_timer, delay_us, 0);// cannot be invoked repeatedly, os_timer_disarm should be invoked first. 
}

void sendEvent(){
	if(txstate == NO_EMPTY_TXBUFFER){
		if(isMediumFree()){
			txstate = NO_BACKOFF;
			startTxDelayTimer(DIFS);
		}else{
			txstate = DEFER;
		}
	}
}

void checkTxBufferEvent(){
	if(txstate == NO_EMPTY_TXBUFFER){
		if(last != first){//NO_EMPTY_TXBUFFER
			txstate == NO_EMPTY_TXBUFFER
			last++;
			sendEvent();
		}else{//IDLE
			txstate = IDLE;
			emptyTxBufferEvent();
		}
	}
}

//definizione callback function del timer
void ICACHE_FLASH_ATTR txDelayTimerEvent(){
	//os_timer_disarm(&IFS_timer);
	if(txstate == NO_BACKOFF || txstate == BACKOFF_STARTED){
		txstate = TRANSMIT_FRAME
		setTXMode();
		//mediumFree = false;
		startTxDelayTimer(SIFS);
	}else if(txstate == NO_ACK){//NO_ACK
		if(retry < RETRYLIMIT){
			txstate = NO_EMPTY_TXBUFFER;
			retry++;
			last--;		//il successivo incremento rimanda lo stesso pachetto
		else{
			txstate = IDLE;
			print("error");
			retry = 0;
		}
	}else if(txstate == TRANSMIT_FRAME){//START_TRANSMIT
		txstate = START_TRANSMIT;
		transmitFrame(txpackets[last]);
		//mediumFree = true;
		setRXMode();
		startTxDelayTimer(TXTIMOUT);
	}else if(txstate == DATA_RECEIVED){//DATA_RECEIVED
		txstate == ACK_TRANSMIT;
		transmitFrame(makeAck(rxpackets[last]));
		setRXMode();
		mediumFree = false;
	}
}

void mediumBusyEvent(){
	if(txstate == START_BACKOFF){
		pauseTxDelayTimer();
		txstate = BACKOFF_STOPPED;
	}
}

void ackReceivedEvent(){
	if(txstate == NO_ACK){
		txstate = NO_EMPTY_TXBUFFER;
		stopTxDelayTimer();
	}
}
//---------------------------------------------------------------------------
void dataReceivedEvent(){
		if(isAck(txpackets[last])){
			ackReceivedEvent();
		}else if(txstate == IDLE){
			txstate = DATA_RECEIVED;
			mediumBusyEvent();
			startTxDelayTimer(SIFS);
			setTXMode();
		}
	}
}

void frameSentEvent(){
	if(txstate == START_TRANSMIT){
		txstate = NO_ACK;
	}else if(txstate == ACK_TRANSMIT){
		txstate = IDLE;
	}
}

void mediumFreeEvent(){
	if(txstate == DEFER){
		txstate = BACKOFF_STARTED;
		startTxDelayTimer(BACKOFF);
	}else if(txstate == BACKOFF_STOPPED){
		resumeTxDelayTimer();
		txstate = BACKOFF_STARTED;
	}
}

void CSMApoll(){
	int last = 0;
	while(Serial.available()){
		nav = false;
		CSMABuffer[last] = Serial.read();
		last++;
	}
	
	if (last > 0) {
		mediumBusyEvent();
		// Updates registers information 
		CSMA_msg_length = CSMA_Pack(CSMABuffer, MBHoldingRegister);

		if (MB_msg_length > 0){
			dataReceivedEvent();
			// Returns the Modbus response
			//Serial.write((const uint8_t *)CSMABuffer, MB_msg_length);
			Serial.flush();
		}
	}else{
		if(uart_tx_fifo_empty(0)){
			frameSentEvent();
			mediumFreeEvent();
			checkTxBufferEvent();
		}	
	}
}

/*
void UARTInit(){
  ETS_UART_INTR_DISABLE();
  ETS_UART_INTR_ATTACH(uart_intr_handler,  NULL);
  ETS_UART_INTR_ENABLE();
}
static void ICACHE_RAM_ATTR uart_intr_handler(void *para){
  uint32_t int_status = U0IS;
  uint8_t space = 0;
  uint8_t waiting = 0;

  if(int_status & (1 << UIFR)) U0IC = (1 << UIFR);//clear any frame error

  if(int_status & (1 << UIFF) || int_status & (1 << UITO)){
    ETS_UART_INTR_DISABLE();
    while(((U0S >> USRXC) & 0x7F) != 0){
      WDT_RESET();
      RingBuffer_Insert(&uart0_rx_buffer, U0F);
    }
    int_status = U0IS;
    if(int_status & (1 << UIFF)) U0IC = (1 << UIFF);//clear any FIFO FULL error
    if(int_status & (1 << UITO)) U0IC = (1 << UITO);//clear any TimeOut error
    ETS_UART_INTR_ENABLE();
  }

  if(U0IS & (1 << UIFE)){
    U0IC = (1 << UIFE);
    if(RingBuffer_IsEmpty(&uart0_tx_buffer))return uart_txint(0, 0);
    space = 0x7F - ((U0S >> USTXC) & 0x7F);
    waiting = RingBuffer_GetCount(&uart0_tx_buffer);
    while(space-- && waiting--){
      U0F = RingBuffer_Remove(&uart0_tx_buffer);
    }
  }

  if(U1IS & (1 << UIFE)){
    U1IC = (1 << UIFE);
    if(RingBuffer_IsEmpty(&uart1_tx_buffer))return uart_txint(1, 0);
    space = 0x7F - ((U1S >> USTXC) & 0x7F);
    waiting = RingBuffer_GetCount(&uart1_tx_buffer);
    while(space-- && waiting--){
      U1F = RingBuffer_Remove(&uart1_tx_buffer);
    }
  }
}
*/



