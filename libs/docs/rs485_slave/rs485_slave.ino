
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define BAUDRATE            19200       //rata de baud a comunicatiei RS485
#define RS485_ENABLE_PIN    0           //pinul GPIO0

//parametrii master pentru Access Point -> acesti parametri trebuie sa fie cunoscuti si de catre sclavi
char ssid[] = "myNetwork";             // your network SSID (name) 
char pass[] = "myPassword";            // your network password -> min 8 caractere                 

//comunicatia se desfasoara folosind protocolul UDP.
WiFiUDP Udp;
unsigned int localUdpPort = 59999;     
IPAddress masterIp(192, 168, 4, 1);    //adresa modulului master e folosita doar de modulele slave
 

void setup() {
 //defineste portul serial in configuratie rs485
  Serial.begin(BAUDRATE);                    
  //pinul GPIO0 asigura functia Enable rs485   
  pinMode(RS485_ENABLE_PIN, OUTPUT);       //pinul Enable rs485 este iesire digitala
  digitalWrite(RS485_ENABLE_PIN, LOW);     //stare inactiva -> pentru receptie   

  Udp.begin(localUdpPort);    

  //modulele slave se conecteaza la Access Point asigurat de modulul master
  WiFi.begin(ssid, pass);  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //declara modulele slave ca WiFi statie internet
  WiFi.mode(WIFI_STA);
}


void loop() {    
  uint8_t Answer_len = 0;      //lungimea raspunsului receptionat serial
   
  while (Serial.available() > Answer_len) {
    Answer_len = Serial.available();   
    delay(1); 
  } 

  if (Answer_len > 0) { 
    if (WiFi.status() == WL_CONNECTED) {
      //transmite raspunsul catre modulul master doar daca e conectat la Access Point
      Udp.beginPacket(masterIp, localUdpPort);        
      for (uint8_t i = 0; i < Answer_len; i++){
        Udp.write(Serial.read());
      }      
      Udp.endPacket(); 
    }
  }

  //conectarea la Access Point al modulului master e absolut necesara
  if (WiFi.status() == WL_CONNECTED) {
    //receptionarea mesajelor udp -> procedura identica pentru modulul master si modulele slave    
    int Cmd_len = Udp.parsePacket();      //lungimea pachetului de date receptionat    
   
    if (Cmd_len) {      
      byte Cmd[Cmd_len];                  //aria bytes in care e salvat pachetul receptionat

      //salveaza mesajul UDP receptionat
      Udp.read(Cmd, Cmd_len);
     
      //modulul ESP-01 slave activeaza interfata rs485 a senzorului AQ-01 pt receptionarea comenzii
      digitalWrite(RS485_ENABLE_PIN, HIGH);     //stare activa pt Enable pin
      delay(1);                       
      //transmite mesajul receptionat fara nicio modificare, pe interfata rs485     
      for (uint8_t i = 0; i < Cmd_len; i++){ 
        Serial.write(Cmd[i]);                   
      }   
      Serial.flush ();    //asteapta pana cand a fost transmis ultimul byte
      delay(1);                               
      digitalWrite(RS485_ENABLE_PIN, LOW);      //stare inactiva pt Enable pin     
    }
  }

  //asteapta pana la conectarea la Access Point al modulului master
  else if (WiFi.status() != WL_CONNECTED) {
     delay(500);
     Serial.print(".");
  }
}








