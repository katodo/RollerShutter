
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define BAUDRATE            19200   //rata de baud a comunicatiei RS485
#define RS485_ENABLE_PIN    0       //pinul GPIO0

//parametrii pentru Access Point -> acesti parametri trebuie sa fie cunoscuti si de sclavi
char ssid[] = "myNetwork";          // your network SSID (name) 
char pass[] = "myPassword";         // your network password -> min 8 caractere


//comunicatia se desfasoara folosind protocolul UDP.
WiFiUDP Udp;
unsigned int localUdpPort = 59999;
IPAddress broadcastIp(192, 168, 4, 255);    //adresa de broadcast e folosita doar de modulul master


void setup() {
   //defineste portul serial in configuratie rs485
  Serial.begin(BAUDRATE);                    
  //pinul GPIO0 asigura functia Enable rs485   
  pinMode(RS485_ENABLE_PIN, OUTPUT);       //pinul Enable rs485 este iesire digitala
  digitalWrite(RS485_ENABLE_PIN, LOW);     //stare inactiva  

  Udp.begin(localUdpPort);
  
  //modulul master asigura un Access Point pentru modulele slave    
  WiFi.mode(WIFI_AP);      
  WiFi.softAP(ssid, pass);  
}


void loop() {        
  uint8_t Cmd_len = 0;      //lungimea comenzii receptionata serial
        
  while (Serial.available() > Cmd_len) {
    Cmd_len = Serial.available();
    delay(1); 
  }

  if (Cmd_len > 0) {    
     //difuzeaza mesajul tuturor adreselor IP din reteaua proprie
     Udp.beginPacket(broadcastIp,localUdpPort);         
     for (uint8_t i = 0; i < Cmd_len; i++){ 
       Udp.write(Serial.read());
     }     
     Udp.endPacket();      
   } 

  //receptionarea mesajelor udp -> procedura identica pentru modulul master si modulele slave
  int Answer_len = Udp.parsePacket();
 
  if (Answer_len) {
    //e ff important ca bufferul in care e salvat raspunsul receptionat sa aiba lungime precizata
    byte Answer[Answer_len];    
  
    //salveaza mesajul UDP receptionat
    Udp.read(Answer, Answer_len);   

    //transmite mesajul receptionat fara nicio modificare, pe interfata rs485
    digitalWrite(RS485_ENABLE_PIN, HIGH);     //stare activa
    delay(1);
    for (uint8_t i = 0; i < Answer_len; i++){ 
      Serial.write(Answer[i]);           
    }   
    Serial.flush ();    //asteapta pana cand a fost transmis ultimul byte 
    delay(1);       
    digitalWrite(RS485_ENABLE_PIN, LOW);      //stare inactiva     
  } 
}








